import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

String selectedUrl = 'https://www.teleforce.in/';

class AboutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: new Text('About us'),
        ),
        body: Column(
          children: [
            Expanded(
                child: WebView(
                    javascriptMode: JavascriptMode.unrestricted,
                    initialUrl: selectedUrl))
          ],
        ));
  }
}
