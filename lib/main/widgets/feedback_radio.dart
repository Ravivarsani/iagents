import 'dart:convert';
import 'dart:io';
import 'package:TeleVoice/main/model/feedback_model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FeedbackRadio extends StatefulWidget {
  String id;
  bool forUpdate;
  String feedback;
  String note;
  String date;

  FeedbackRadio(
      String Id, bool isForUpdate, String feedback, String note, String date) {
    this.id = Id;
    this.forUpdate = isForUpdate;
    this.feedback = feedback;
    this.note = note;
    this.date = date;
  }

  @override
  _FeedbackRadioState createState() =>
      _FeedbackRadioState(id, forUpdate, feedback, note, date);
}

class _FeedbackRadioState extends State<FeedbackRadio> {
  int _radioValue1 = 0;
  DateTime pickedDate;
  TimeOfDay time;
  String cdrId;
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  String mFeedback = 'interested';
  String note = ' -';
  String mdate = ' -';
  String mtime = ' -';
  TextEditingController _NoteController = TextEditingController();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  bool visibilityTag = false;
  bool isforUpdate = false;

  _FeedbackRadioState(
      String id, bool forUpdate, String feedback, String note, String date) {
    this.cdrId = id;
    this.isforUpdate = forUpdate;
    if (isforUpdate) {
      note = note;
      if (feedback == 'interested') {
        _radioValue1 = 0;
        mFeedback = 'interested';
      } else if (feedback == 'not interested') {
        _radioValue1 = 1;
        mFeedback = 'not interested';
      } else if (feedback == 'schedule') {
        _radioValue1 = 2;
        mFeedback = 'schedule';
        visibilityTag = true;
        var datetime =
            new DateFormat("yyyy/MM/dd hh:mm:ss").parse(date).toLocal();
        mdate = "${datetime.year}-${datetime.month}-${datetime.day}";
        mtime = "${datetime.hour}-${datetime.minute}-${datetime.second}";
//        _changed(true);
      }
    }
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          mFeedback = "interested";
          _changed(false);
          break;
        case 1:
          mFeedback = "not interested";
          _changed(false);
          break;
        case 2:
          mFeedback = "schedule";
          _changed(true);
//          _pickDate();
          break;
      }
    });
  }

  void _changed(bool visibility) {
    setState(() {
      visibilityTag = visibility;
    });
  }

//using firstDate and lastDate parameter, we are allowing users to pick a date 5 years in past and 5 years in future.
  _pickDate() async {
    DateTime date = await showDatePicker(
      context: context,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
      initialDate: pickedDate,
    );
    if (date != null)
      setState(() {
        pickedDate = date;
      });
  }

  _pickTime() async {
    TimeOfDay t = await showTimePicker(context: context, initialTime: time);
    if (t != null)
      setState(() {
        time = t;
      });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    pickedDate = DateTime.now();
    time = TimeOfDay.now();
    _loadSettings();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();
    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Feedback'),
      ),
      body: new SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Padding(
                padding: new EdgeInsets.only(
                    left: 2.0, top: 20.0, right: 4.0, bottom: 4.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new Radio(
                      value: 0,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    new Text(
                      'Interested',
                      style: new TextStyle(fontSize: 14.0),
                    ),
                    new Radio(
                      value: 1,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    new Text(
                      'Not Interested',
                      style: new TextStyle(
                        fontSize: 14.0,
                      ),
                    ),
                    new Radio(
                      value: 2,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    new Text(
                      'Schedule',
                      style: new TextStyle(fontSize: 14.0),
                    ),
                  ],
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(
                    left: 2.0, top: 10.0, right: 4.0, bottom: 4.0),
                child: visibilityTag
                    ? new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text('Schedule Date : '),
                          InkWell(
                            child: Text(
                                '${pickedDate.year}-${pickedDate.month}-${pickedDate.day}'),
                            onTap: () {
                              _pickDate();
                            },
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          new Text('Time : '),
                          InkWell(
                            child: Text('${time.hour}:${time.minute}'),
                            onTap: () {
                              _pickTime();
                            },
                          )
                        ],
                      )
                    : new Container(),
              ),
              new Padding(
                padding: new EdgeInsets.all(20),
                child: new TextFormField(
                  decoration: new InputDecoration(
                    labelText: "Enter Note",
                    fillColor: Colors.white,
                    border: new OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(25.0),
                      borderSide: new BorderSide(),
                    ),
                    //fillColor: Colors.green
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Note cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  textInputAction: TextInputAction.newline,
                  controller: _NoteController,
                  style: new TextStyle(
                    fontFamily: "Poppins",
                  ),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(
                    left: 24.0, top: 12.0, right: 8.0, bottom: 4.0),
                child: new RaisedButton(
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.white,
                  color: Colors.blue,
                  splashColor: Colors.grey,
                  onPressed: _saveFeedback,
                  child: new Text("Save Feedback"),
                ),
              ),
            ]),
      ),
    );
  }

  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: SimpleDialog(
                  key: key,
                  backgroundColor: Colors.black54,
                  children: <Widget>[
                    Center(
                      child: Column(children: [
                        CircularProgressIndicator(),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Updating Feedback",
                          style: TextStyle(color: Colors.blueAccent),
                        )
                      ]),
                    )
                  ]));
        });
  }

  _saveFeedback() {
    String date = '';
    String time1 = '';

    showLoadingDialog(context, _keyLoader);

    if (mFeedback == 'schedule') {
      date = '${pickedDate.year}-${pickedDate.month}-${pickedDate.day} ${time.hour}:${time.minute}';
      time1 = '${time.hour}:${time.minute}';
    }

    Logger().d("=========>>> " + date);
    Logger().d("=========>>> " + time1);

    Future<feedback_model> _futureModelFeedback = PostFeedbackData(
        context,
        userID,
        accessToken,
        cdrId,
        mFeedback,
        _NoteController.text,
        date,
        time1);

    Future.delayed(Duration(seconds: 1)).then((value) {
      setState(() {
        Navigator.pop(_keyLoader.currentContext);

        _futureModelFeedback.then((value) {
          Fluttertoast.showToast(
              msg: value.msg,
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.CENTER,
              timeInSecForIosWeb: 1,
              backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
              textColor: Colors.white,
              fontSize: 17.0);
        });
      });
    });
  }
}

Future<feedback_model> PostFeedbackData(
    BuildContext ctx,
    int id,
    String token,
    String cdrID,
    String feedback,
    String note,
    String date,
    String time) async {
  final http.Response response = await http.post(
    Constant.POST_FEEDBACK,
    headers: <String, String>{
      HttpHeaders.authorizationHeader: "Bearer " + token,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: {
      'id': cdrID,
      'feedback': feedback,
      'note': note,
      'scheduledate': date,
      'starttime': time,
    },
  );

  Logger().d("=======>>> ongoing " + response.body.toString());

  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);
    return feedback_model.fromJson(json);
  } else {
    throw Exception('=====>>> Failed to fetch data');
  }
}
