import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'feedback_radio.dart';

class ShowFeedbackPage extends StatefulWidget {
  int id;
  String feedback;
  String note;
  String date;
  bool show;

  ShowFeedbackPage(int s, String feedback, String note, String date, bool bool) {
    this.id = s;
    this.show = bool;
    this.feedback = feedback.toString();
    print('=============>>>>>  $date');
    if (note.toString() == 'null') {
      this.note = ' -';
    } else {
      this.note = note;
    }
    if (date.toString() == 'null') {
      this.date = ' -';
    } else {
      this.date = DateFormat('dd/MM/yyyy HH:mm:ss')
          .format(DateTime.parse(date.toString()).toLocal());
    }
  }

  @override
  _ShowFeedbackPageState createState() =>
      _ShowFeedbackPageState(id, feedback, note, date, show);
}

class _ShowFeedbackPageState extends State<ShowFeedbackPage> {
  int mId;
  String feedback;
  String note;
  String date;
  bool isShow;

  _ShowFeedbackPageState(
      int id, String feedback, String note, String date, bool isshow) {
    this.mId = id;
    this.feedback = feedback;
    this.note = note;
    this.date = date;
    this.isShow = isshow;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: new Center(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
            child: Text(
              'Call id : ' + mId.toString(),
              style: new TextStyle(
                fontSize: 18.0,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 2.0),
            child: Text(
              'Feedback : ' + feedback ?? '',
              style: new TextStyle(fontSize: 18.0, color: Colors.black),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 2.0),
            child: Text(
              'Note : ' + note ?? '  - ',
              style: new TextStyle(fontSize: 18.0, color: Colors.black),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, bottom: 2.0),
            child: Text('Schedule date : ' + date ?? '',
                style: TextStyle(fontSize: 18, color: Colors.black)),
          ),
          isShow ?  new Padding(
            padding: new EdgeInsets.only(
                left: 24.0, top: 12.0, right: 8.0, bottom: 4.0),
            child: new RaisedButton(
              padding: const EdgeInsets.all(8.0),
              textColor: Colors.white,
              color: Colors.blue,
              splashColor: Colors.grey,
              onPressed: () => _updateFeedback(mId),
              child: new Text("Update Feedback"),
            ),
          ) : Container()
        ],
      ),
    ));
  }

  _updateFeedback(int id) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              FeedbackRadio(id.toString(), true, feedback, note, date),
        ));
  }
}
