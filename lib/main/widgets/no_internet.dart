import 'package:flutter/material.dart';
import 'package:TeleVoice/main/utils/constant.dart';

class NoInternetConnectionWidget extends StatefulWidget {
  @override
  _MyConnectionPageState createState() {
    return _MyConnectionPageState();
  }
}

class _MyConnectionPageState extends State<NoInternetConnectionWidget>{

  bool isInternetConnection = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Container(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Icon(
                    Icons.signal_wifi_off,
                    color: Color.fromRGBO(49, 87, 110, 1.0),
                    size: 65.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  new Text('Oops!',
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.title),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0),
                  ),
                  new Text('It seems like you are not connected to network.',
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.body1),
                  Padding(
                    padding: EdgeInsets.only(top: 27.0),
                  ),
                  new MaterialButton(
                    color: Colors.blue[700],
                    textColor: Colors.white,
                    child: new Text('RETRY'),
                    onPressed: () {
                      if (this.mounted) {
                        setState(() {
                          _internetConnection();
                        });
                      }
                    },
                  )
                ],
              ),
            ),
          ],
        ));
  }

  _internetConnection() {
    Constant.isInternetAvailable().then((value) {
      print("=====>> internet connection ==>> $value");
      if (value) {
        isInternetConnection = true;
        if (this.mounted) {
          setState(() {});
        }
      } else {
        isInternetConnection = false;
        if (this.mounted) {
          setState(() {});
        }
      }
    });
  }

}
