import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:TeleVoice/src/sip_ua_helper.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/location_model.dart';
import 'package:TeleVoice/main/ui/dashboard/dashboard.dart';

//import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:logger/logger.dart';
import 'package:ringtone/ringtone.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:workmanager/workmanager.dart';
import 'package:TeleVoice/main/notification.dart' as notif;
import 'package:http/http.dart' as http;

const fetchBackground = "fetchBackground";

void callbackDispatcher() {
  Workmanager.executeTask((task, inputData) async {
    switch (task) {
      case fetchBackground:
        Position userLocation = await Geolocator.getCurrentPosition(
            desiredAccuracy: LocationAccuracy.high);
        final coordinates =
            new Coordinates(userLocation.latitude, userLocation.longitude);
        var addresses =
            await Geocoder.local.findAddressesFromCoordinates(coordinates);
        var first = addresses.first;
        String userID = '', accessToken = '';
        if (inputData.isNotEmpty) {
          inputData.forEach((key, value) {
            if (key == 'agentID') {
              userID = value;
            } else if (key == 'token') {
              accessToken = value;
            }
          });

          print('=========>>>  $userID');
          print('=========>>>  $accessToken');

          sendLocationToServer(
                  userID,
                  accessToken,
                  userLocation.latitude,
                  userLocation.longitude,
                  first.featureName,
                  first.addressLine,
                  first.adminArea,
                  first.subAdminArea,
                  first.subLocality,
                  first.countryName,
                  first.postalCode)
              .then((value) {
            print('========================>>>');
            print(value.msg);
            print('========================>>>');
          });
        } else {
          print('=========>>> Fuckkk bc');
        }
        break;
    }
    return Future.value(true);
  });
}

class Home extends StatefulWidget {
  final SIPUAHelper _helper;

  Home(this._helper, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> implements SipUaHelperListener {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool isInternetConnection = true;
  bool isError = false;
  SharedPreferences _preferences;
  String userID;
  String accessToken;

  SIPUAHelper get helper => widget._helper;

  /* static const platform =
      const MethodChannel('teleforce.flutter.dev/incoming_phone_number');
  String _number = '';*/

  @override
  void initState() {
    _internetConnection();

    _loadSettings();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        Logger().d("========>> Notification onMessage ");
        Logger().d("========>>$message ");
        //_navigateToItemDetail();
      },
      onLaunch: (Map<String, dynamic> message) async {
        Logger().d("========>> Notification onLaunch ");
        _navigateToItemDetail();
      },
      onResume: (Map<String, dynamic> message) async {
        Logger().d("========>> Notification onResume ");
        _navigateToItemDetail();
      },
    );

    //_getIncomingNumber();

    helper.addSipUaHelperListener(this);

    super.initState();
  }

  @override
  deactivate() {
    super.deactivate();
    helper.removeSipUaHelperListener(this);
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id').toString();
      print('=========>>> in Home $userID');
      accessToken = _preferences.getString('access_token');
    });

    Map<String, String> createData = new HashMap();
    createData['agentID'] = userID.toString();
    createData['token'] = accessToken.toString();

    Workmanager.initialize(
      callbackDispatcher,
      isInDebugMode: false,
    );

    Workmanager.registerPeriodicTask("agentlocation", fetchBackground,
        frequency: Duration(minutes: 15), inputData: createData);

    checkforLocationPermission();
  }

  void checkforLocationPermission() async {
    LocationPermission permission = await Geolocator.checkPermission();

    LocationPermission permission1 = await Geolocator.requestPermission();

    bool isLocationServiceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!isLocationServiceEnabled) {
      await Geolocator.openAppSettings();
      await Geolocator.openLocationSettings();
    }

    Position userLocation = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    final coordinates =
        new Coordinates(userLocation.latitude, userLocation.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;

    sendLocationToServer(
            userID,
            accessToken,
            userLocation.latitude,
            userLocation.longitude,
            first.featureName,
            first.addressLine,
            first.adminArea,
            first.subAdminArea,
            first.subLocality,
            first.countryName,
            first.postalCode)
        .then((value) {
      print('========================>>> innnn');
      print(value.msg);
      print('========================>>> innnn');
    });
  }

//  Future<void> _getIncomingNumber() async {
//    String number = '';
//    try {
//      final String result = await platform.invokeMethod('getIncomingNumber');
//      number = result;
//    } on PlatformException catch (e) {
//      number = "Failed to get number: '${e.message}'.";
//    }
//
//    setState(() {
//      _number = number;
//
//      Fluttertoast.showToast(
//          msg: ' Incoming Number ' + _number,
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.CENTER,
//          timeInSecForIosWeb: 1,
//          backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
//          textColor: Colors.white,
//          fontSize: 16.0);
//    });
//  }

  void _navigateToItemDetail() {
    Navigator.pushNamedAndRemoveUntil(
        context, "/currentcall", (Route<dynamic> route) => true);
  }

  @override
  Widget build(BuildContext context) {
    if (isInternetConnection != null && isInternetConnection) {
      return Scaffold(
        body: Dashboard(),
      );
    } else if (isInternetConnection != null && !isInternetConnection) {
      return new Scaffold(
          body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(
                  Icons.signal_wifi_off,
                  color: Color.fromRGBO(49, 87, 110, 1.0),
                  size: 65.0,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                new Text('Oops!',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.title),
                Padding(
                  padding: EdgeInsets.only(top: 16.0),
                ),
                new Text('It seems like you are not connected to network.',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.body1),
                Padding(
                  padding: EdgeInsets.only(top: 27.0),
                ),
                new MaterialButton(
                  color: Colors.blue[700],
                  textColor: Colors.white,
                  child: new Text('RETRY'),
                  onPressed: () {
                    if (this.mounted) {
                      setState(() {
                        _internetConnection();
                      });
                    }
                  },
                )
              ],
            ),
          ),
        ],
      ));
    }
  }

  _internetConnection() {
    Constant.isInternetAvailable().then((value) {
      print("=====>> internet connection ==>> $value");
      if (value) {
        isInternetConnection = true;
        if (this.mounted) {
          setState(() {});
        }
      } else {
        isInternetConnection = false;
        if (this.mounted) {
          setState(() {});
        }
      }
    });
  }

  @override
  void callStateChanged(Call call, CallState state) {
    // TODO: implement callStateChanged
    if (state.state == CallStateEnum.CALL_INITIATION) {
      if (call.direction == 'INCOMING') {
        Ringtone.play();
      }

      Navigator.pushNamed(context, '/callscreen', arguments: call);
    }
  }

  @override
  void onNewMessage(SIPMessageRequest msg) {
    // TODO: implement onNewMessage
  }

  @override
  void registrationStateChanged(RegistrationState state) {
    // TODO: implement registrationStateChanged
  }

  @override
  void transportStateChanged(TransportState state) {
    // TODO: implement transportStateChanged
  }
}

Future<Location_model> sendLocationToServer(
  String id,
  String token,
  double lat,
  double long,
  String number,
  String address,
  String state,
  String city,
  String areaname,
  String country,
  String pincode,
) async {

  if (areaname == null) {
    areaname = '';
  }

  final http.Response response = await http.post(
    Constant.POST_AGENT_LIVE_LOCATION,
    headers: <String, String>{
      HttpHeaders.authorizationHeader: "Bearer " + token,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: {
      'agent_id': id,
      'lat': lat.toString(),
      'long': long.toString(),
      'number': number,
      'address': address,
      'state': state,
      'city': city,
      'areaname': areaname,
      'country': country,
      'pincode': pincode,
    },
  );

  //Logger().d("=======>>> Location " + response.body.toString());
  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);
    return Location_model.fromJson(json);
  }
}
