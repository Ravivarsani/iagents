import 'dart:developer';
import 'dart:io';
import 'package:TeleVoice/sip_ua.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:TeleVoice/main/model/role_model.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:TeleVoice/main/widgets/no_internet.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:universal_platform/universal_platform.dart';
import 'model/login_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyLogin extends StatefulWidget {
  final SIPUAHelper _helper;

  MyLogin(this._helper, {Key key}) : super(key: key);

  @override
  _MyLoginPageState createState() {
    return _MyLoginPageState();
  }
}

class _MyLoginPageState extends State<MyLogin> implements SipUaHelperListener {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  TextEditingController _usernameController = TextEditingController();

  TextEditingController _passwordController = TextEditingController();

  Future<login_model> _futureAlbum;
  Future<RoleModel> _futureRole;
  final _formKey = GlobalKey<FormState>();

  Map<String, String> _wsExtraHeaders = {
    'Origin': 'https://tryit.jssip.net',
    'Host': 'tryit.jssip.net:10443'
  };

  final String _password = 'garudavoip@dv';
  final String _wsUri = 'ws://192.168.1.26:8088/ws';
  final String _sipUri = '@192.168.1.26'; //gaesip.teleforce.in
  String _displayName = '';
  String _authorizationUser = '';
  SharedPreferences _preferences;

  RegistrationState _registerState;

  SIPUAHelper get helper => widget._helper;

  Future<uuid_model> _futureUpdateUUID;
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
  int userID;
  String accessToken;
  String app_version;
  bool isInternetConnection = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _internetConnection();
    _registerState = helper.registerState;
    helper.addSipUaHelperListener(this);
    check_if_already_login();
    _loadSettings();
  }

  @override
  deactivate() {
    super.deactivate();
    _saveSettings();
  }

  void _loadSettings() async {
    if (UniversalPlatform.isIOS) {
      checkforIosPermission();
    }

    //_checkPermission();

    PackageInfo packageInfo = await PackageInfo.fromPlatform();

    app_version = packageInfo.version;
    String buildNumber = packageInfo.buildNumber;

    firebaseMessaging.getToken().then((token) {
      _preferences.setString("uuid", token);
    });
  }

  void checkforIosPermission() async {
    await firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    await firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  void _saveSettings() {
    _preferences.setString('sip_uri', _authorizationUser + _sipUri);
    _preferences.setString('display_name', _displayName);
    _preferences.setString('auth_user', _authorizationUser);
  }

  void _registerWithServer(BuildContext context, String username) {
    _displayName = username;
    _authorizationUser = username;

    UaSettings settings = UaSettings();

    settings.webSocketUrl = _wsUri;
    settings.uri = username + _sipUri;
    settings.authorizationUser = username;
    settings.password = _password;
    settings.displayName = username;

    settings.webSocketUrl = _wsUri;
    settings.webSocketSettings.extraHeaders = _wsExtraHeaders;
    settings.webSocketSettings.allowBadCertificate = true;
    settings.webSocketSettings.userAgent = 'Dart/2.8 (dart:io) for OpenSIPS.';
    settings.userAgent = 'Dart SIP Client v1.0.0';

    helper.start(settings);
  }

  @override
  Widget build(BuildContext context) {
    final emailField = TextFormField(
      keyboardType: TextInputType.emailAddress,
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter Username';
        }
        return null;
      },
      obscureText: false,
      style: style,
      textInputAction: TextInputAction.next,
      controller: _usernameController,
      decoration: new InputDecoration(
        labelText: "Username",
        fillColor: Colors.white,
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(25.0),
          borderSide: new BorderSide(),
        ),
        //fillColor: Colors.green
      ),
    );

    final passwordField = TextFormField(
      obscureText: true,
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter Password';
        }
        return null;
      },
      style: style,
      controller: _passwordController,
      decoration: new InputDecoration(
        labelText: "Password",
        fillColor: Colors.white,
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(25.0),
          borderSide: new BorderSide(),
        ),
        //fillColor: Colors.green
      ),
    );

    final loginButon = Padding(
      padding: EdgeInsets.only(left: 0.0, right: 0.0),
      child: (_futureAlbum == null)
          ? RaisedButton(
              textColor: Colors.white,
              color: Color.fromRGBO(49, 87, 110, 1.0),
              child: new Padding(
                  padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
                  child: new Center(
                    child: new Text(
                      'Login',
                      style: new TextStyle(fontSize: 18.0, color: Colors.white),
                    ),
                  )),
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  setState(() {
                    _futureAlbum = createAlbum(
                        _usernameController.text, _passwordController.text);
                  });
                } else {
//                  _showDialog();
                }
              },
              shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(30.0),
              ),
            )
          : FutureBuilder<login_model>(
              future: _futureAlbum,
              builder: (context, snapshot) {
                WidgetsBinding.instance.addPostFrameCallback((_) {
                  // Add Your Code here.
                  if (snapshot.hasData) {
                    if (snapshot.data.islogin) {
                      //_authorizationUser = snapshot.data.user.username;
                      String mobile = snapshot.data.user.mobile;
                      String isVerified = snapshot.data.user.mobileverify;

                      _futureUpdateUUID = sendTokenToServer(
                          snapshot.data.user.id,
                          _preferences.getString("uuid"));

                      _futureUpdateUUID.then((value) {});
                      _registerWithServer(context, snapshot.data.user.username);
                      getRole(
                          context,
                          snapshot.data.user.id,
                          snapshot.data.accessToken,
                          snapshot.data.user.username,
                          mobile,
                          isVerified,
                          snapshot.data.user.voip_status);
                    } else {
                      final snackBar =
                          SnackBar(content: Text(snapshot.data.msg));

                      Scaffold.of(context).showSnackBar(snackBar);

                      Timer timer = new Timer(const Duration(seconds: 1), () {
                        Navigator.popAndPushNamed(context, '/login');
                      });
                    }
                  } else if (snapshot.hasError) {
                    var logger = Logger();
                    logger.d("========>> errrorr " + snapshot.error);
                    Timer timer = new Timer(const Duration(seconds: 1), () {
                      Navigator.popAndPushNamed(context, '/login');
                    });
                  }
                });

                return CircularProgressIndicator();
              },
            ),
    );

    if (isInternetConnection != null && !isInternetConnection) {
      return new Scaffold(
          body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(
                  Icons.signal_wifi_off,
                  color: Color.fromRGBO(49, 87, 110, 1.0),
                  size: 65.0,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                new Text('Oops!',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.title),
                Padding(
                  padding: EdgeInsets.only(top: 16.0),
                ),
                new Text('It seems like you are not connected to network.',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.body1),
                Padding(
                  padding: EdgeInsets.only(top: 27.0),
                ),
                new MaterialButton(
                  color: Colors.blue[700],
                  textColor: Colors.white,
                  child: new Text('RETRY'),
                  onPressed: () {
                    if (this.mounted) {
                      setState(() {
                        _internetConnection();
                      });
                    }
                  },
                )
              ],
            ),
          ),
        ],
      ));
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: Center(
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Center(
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(36.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 155.0,
                          child: Image.asset(
                            "images/logo.png",
                            fit: BoxFit.contain,
                          ),
                        ),
                        SizedBox(height: 70.0),
                        emailField,
                        SizedBox(height: 30.0),
                        passwordField,
                        SizedBox(
                          height: 45.0,
                        ),
                        loginButon,
                        SizedBox(
                          height: 15.0,
                        ),
                        SizedBox(),
                        Text(
                          'App version : $app_version',
                          style: TextStyle(color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      );
    }
  }

  void getRole(BuildContext cxt, int id, String token, String username,
      String mobile, String isverify, int voipstatus) {
    _futureRole = fetchRole(cxt, id, token);
    _futureRole.then((value) =>
        saveRole(value, id, token, username, mobile, isverify, voipstatus));
  }

  void saveRole(RoleModel value, int id, String token, String username,
      String mobile, String isverify, int voipstatus) async {
    if (value.data.isNotEmpty) {
      String type = value.data.elementAt(0).accountType.toString();

      if (type == '3' || type == '4') {
        Fluttertoast.showToast(
            msg: 'You can not login, This app is not designed for you!',
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
            textColor: Colors.white,
            fontSize: 17.0);

        Timer timer = new Timer(const Duration(seconds: 1), () {
          Navigator.popAndPushNamed(context, '/login');
        });
      } else if (type == '1' || type == '2') {
        saveLoginSessionToSF(username, token, id, voipstatus,
            value.data.elementAt(0).accountType);

        /*if (isverify == 'no') {
          Map map = {
            'id': id,
            'accessToken': token,
            'username': username,
            'mobile': mobile,
            'voipstatus': voipstatus,
          };

          Navigator.popAndPushNamed(context, '/verification',
              arguments: {'mapdata': map});
        } else {
          saveLoginSessionToSF(username, token, id, voipstatus,
              value.data.elementAt(0).accountType);

          SharedPreferences _preference = await SharedPreferences.getInstance();
          _preference.setInt('role', value.data.elementAt(0).accountType);

          Timer timer = new Timer(const Duration(seconds: 1), () {
            Navigator.pushNamedAndRemoveUntil(
                context, "/home", (Route<dynamic> route) => false);
          });
        }*/
      }
    }
  }

  saveLoginSessionToSF(String username, String accessToken, int id,
      int voipstatus, int role) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('islogin', true);
    prefs.setString('auth_user', username);
    prefs.setString('access_token', accessToken);
    prefs.setInt('user_id', id);
    prefs.setInt('role', role);

    if (voipstatus == 0) {
      prefs.setBool('voip_status', true);
    } else {
      prefs.setBool('voip_status', false);
    }

    Navigator.pushNamedAndRemoveUntil(
        context, "/home", (Route<dynamic> route) => false);
  }

  void check_if_already_login() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      _displayName = _preferences.getString('display_name') ?? '';
      _authorizationUser = _preferences.getString('auth_user') ?? '';
    });

    //Return bool
    bool islogin = _preferences.getBool('islogin' ?? false);
    if (islogin == true) {
      UaSettings settings = UaSettings();

      settings.webSocketUrl = _wsUri;
      settings.webSocketSettings.extraHeaders = _wsExtraHeaders;
      settings.webSocketSettings.allowBadCertificate = true;
      settings.webSocketSettings.userAgent = 'Dart/2.8 (dart:io) for OpenSIPS.';

      settings.webSocketUrl = _wsUri;
      settings.uri = _authorizationUser + "@192.168.1.26";
      settings.authorizationUser = _authorizationUser;
      settings.password = _password;
      settings.displayName = _authorizationUser;

      helper.start(settings);

      Navigator.pushNamedAndRemoveUntil(
          context, "/home", (Route<dynamic> route) => false);
    }
  }

  _internetConnection() {
    Constant.isInternetAvailable().then((value) {
      print("=====>> internet connection ==>> $value");
      if (value) {
        isInternetConnection = true;
        if (this.mounted) {
          setState(() {});
        }
      } else {
        isInternetConnection = false;
        if (this.mounted) {
          setState(() {});
        }
      }
    });
  }

  _checkPermission() async {
    if (await Permission.phone.request().isGranted) {
      // Either the permission was already granted before or the user just granted it.
    } else {
      Map<Permission, PermissionStatus> statuses = await [
        Permission.phone,
      ].request();
    }
  }

  @override
  void callStateChanged(Call call, CallState state) {
    // TODO: implement callStateChanged
  }

  @override
  void onNewMessage(SIPMessageRequest msg) {
    // TODO: implement onNewMessage
  }

  @override
  void registrationStateChanged(RegistrationState state) {
    // TODO: implement registrationStateChanged
    this.setState(() {
      _registerState = state;
    });
  }

  @override
  void transportStateChanged(TransportState state) {
    // TODO: implement transportStateChanged
  }
}

Future<login_model> createAlbum(text, text2) async {
  final http.Response response = await http.post(
    Constant.API_URL + Constant.SIGNIN,
    headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: {
      'username': text,
      'password': text2,
    },
  );

  if (response.statusCode == 200) {
    final json = jsonDecode(response.body);
    Logger().d("=======>>> login " + response.body.toString());
    return login_model.fromJson(json);
  } else {
    throw Exception('Failed to login.');
  }
}

Future<RoleModel> fetchRole(BuildContext cnxt, int id, String token) async {
  final response = await http.get(
    Constant.API_URL + Constant.GET_ROLE + "/" + id.toString(),
    headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
  );

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,

    return RoleModel.fromJson(json.decode(response.body));
  } else if (response.statusCode == 401) {
    Fluttertoast.showToast(
        msg: json.decode(response.body)['message'].toString(),
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
        textColor: Colors.white,
        fontSize: 16.0);
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception(
        '============>> Failed to load album ' + response.body.toString());
  }
}

Future<uuid_model> sendTokenToServer(int id, String token) async {
  final http.Response response = await http.post(
    "https://app.teleforce.in/api/signin/updateuuid",
    headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: {
      'id': id.toString(),
      'uuid': token,
    },
  );

  if (response.statusCode == 200) {
    var json = jsonDecode(response.body);
    return uuid_model.fromJson(json);
  } else {
    throw Exception('=====>> Failed to post uuid. ' + response.body.toString());
  }
}