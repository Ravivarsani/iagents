import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:TeleVoice/main/utils/SocketUtils.dart';
import 'package:TeleVoice/main/utils/global.dart';
import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'widgets/action_button.dart';

class DialPadWidget extends StatefulWidget {
  DialPadWidget({Key key}) : super(key: key);

  @override
  _MyDialPadWidget createState() => _MyDialPadWidget();
}

class _MyDialPadWidget extends State<DialPadWidget> {
  String _dest;

  TextEditingController _textController;
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  Future<uuid_model> _futureSetCall;
  String URI = "https://auth.teleforce.in/";
  SocketIOManager manager;
  Map<String, SocketIO> sockets = {};
  Map<String, bool> _isProbablyConnected = {};

  int _radioValue1 = -1;

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 0:
          print(' yes Interested ');
          break;
        case 1:
          print(' No not Interested ');
          break;
        case 2:
          print(' yes Scheduled ');
          break;
      }
    });
  }

  void _showFeedbackDialog() {
    AwesomeDialog(
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.INFO,
      keyboardAware: false,
      body: Padding(
        padding: const EdgeInsets.all(4.0),
        child: Column(
          children: <Widget>[
            Text(
              'Feedback',
              style: Theme.of(context).textTheme.headline6,
            ),
            SizedBox(
              height: 20,
            ),
            Material(
              elevation: 0,
              color: Colors.blueGrey.withAlpha(40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Radio(
                    value: 0,
                    groupValue: _radioValue1,
                    onChanged: _handleRadioValueChange1,
                  ),
                  new Text(
                    'Interested',
                    style: new TextStyle(fontSize: 13.0),
                  ),
                  new Radio(
                    value: 1,
                    groupValue: _radioValue1,
                    onChanged: _handleRadioValueChange1,
                  ),
                  new Text(
                    'Not Interested',
                    style: new TextStyle(fontSize: 13.0),
                  ),
                  new Radio(
                    value: 2,
                    groupValue: _radioValue1,
                    onChanged: _handleRadioValueChange1,
                  ),
                  new Text(
                    'Schedule',
                    style: new TextStyle(fontSize: 13.0),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    ).show();
  }

  @override
  initState() {
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    manager = SocketIOManager();
    _preferences = await SharedPreferences.getInstance();
    userID = _preferences.getInt('user_id');
    accessToken = _preferences.getString('access_token');
    _dest = '';
    _textController = TextEditingController(text: _dest);
    _textController.text = _dest;

    Future.delayed(Duration(seconds: 1), () async {
      G.initSocket();
      await G.socketUtils.initSocket(userID.toString());
      G.socketUtils.connectToSocket();
      G.socketUtils.setConnectListener(onConnect);
      G.socketUtils.setOnAgentCallReceivedListener(onDataReceived);
    });
  }

  onConnect(data) {
    print('==============>>> in dialpad Connected $data');
  }

  onDataReceived(data) {
    print('==============>>> in dialpad data $data');
  }

  Widget _handleCall(BuildContext context, [bool voiceonly = false]) {
    var dest = _textController.text;
    if (dest == null || dest.isEmpty) {
      showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Target is empty.'),
            content: Text('Please enter a number'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
      return null;
    }

    _futureSetCall = setCallRequest(userID, dest, accessToken);
    _preferences.setString('dest', dest);
    _futureSetCall.then((value) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(value.msg),
        duration: Duration(seconds: 1),
      ));

      if (value.msg == 'Call originate succesfully.') {
        Timer timer = new Timer(const Duration(seconds: 1), () {
          Navigator.pushNamedAndRemoveUntil(
              context, "/currentcall", (Route<dynamic> route) => true);
        });
      }
    });

    return null;
  }

  void _handleBackSpace([bool deleteAll = false]) {
    var text = _textController.text;
    if (text.isNotEmpty) {
      this.setState(() {
        text = deleteAll ? '' : text.substring(0, text.length - 1);
        _textController.text = text;
      });
    }
  }

  void _handleNum(String number) {
    this.setState(() {
      _textController.text += number;
    });
  }

  List<Widget> _buildNumPad() {
    var lables = [
      [
        {'1': ''},
        {'2': 'abc'},
        {'3': 'def'}
      ],
      [
        {'4': 'ghi'},
        {'5': 'jkl'},
        {'6': 'mno'}
      ],
      [
        {'7': 'pqrs'},
        {'8': 'tuv'},
        {'9': 'wxyz'}
      ],
      [
        {'*': ''},
        {'0': '+'},
        {'#': ''}
      ],
    ];

    return lables
        .map((row) => Padding(
            padding: const EdgeInsets.all(12),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: row
                    .map((label) => ActionButton(
                          title: '${label.keys.first}',
                          subTitle: '${label.values.first}',
                          onPressed: () => _handleNum(label.keys.first),
                          number: true,
                        ))
                    .toList())))
        .toList();
  }

  List<Widget> _buildDialPad(BuildContext context1) {
    return [
      Container(
          width: 360,
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 360,
                  child: IgnorePointer(
                    child: TextField(
                      keyboardType: TextInputType.text,
                      onTap: () =>
                          FocusScope.of(context).requestFocus(new FocusNode()),
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 24, color: Colors.black54),
                      decoration: new InputDecoration.collapsed(
                        hintText: 'Enter number',
                        border: InputBorder.none,
                      ),
                      controller: _textController,
                    ),
                  ),
                ),
              ])),
      Container(
          width: 300,
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: _buildNumPad())),
      Container(
          width: 300,
          child: Padding(
              padding: const EdgeInsets.all(12),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Visibility(
                    visible: true,
                    child: ActionButton(
                      icon: Icons.close,
                      onPressed: () => _handleBackSpace(true),
                    ),
                  ),
                  ActionButton(
                    icon: Icons.call,
                    fillColor: Colors.green,
                    onPressed: () => _handleCall(context1, true),
                  ),
                  ActionButton(
                    icon: Icons.keyboard_arrow_left,
                    onPressed: () => _handleBackSpace(),
                    onLongPress: () => _handleBackSpace(true),
                  ),
                ],
              )))
    ];
  }

  Future<uuid_model> setCallRequest(int id, String number, String token) async {
    final http.Response response = await http.post(
      Constant.POST_AGENT_OUTGOING_CALL,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'agentid': id.toString(),
        'mobile': number,
      },
    );
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return uuid_model.fromJson(json);
    } else {
      throw Exception(
          '=====>> Failed to post uuid. ' + response.body.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Align(
      alignment: Alignment(0, 0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: _buildDialPad(context),
            )),
          ]),
    ));
  }
}
