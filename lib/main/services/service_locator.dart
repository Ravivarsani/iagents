import 'package:TeleVoice/main/services/calls_and_emails_service.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt();

void setupLocator() {
  locator.registerSingleton(CallsAndEmailsService());
}
