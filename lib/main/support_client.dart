import 'package:TeleVoice/main/services/calls_and_emails_service.dart';
import 'package:TeleVoice/main/services/service_locator.dart';
import 'package:flutter/material.dart';

class SupportClientPage extends StatefulWidget {
  @override
  _SupportClientPageState createState() => _SupportClientPageState();
}

class _SupportClientPageState extends State<SupportClientPage> {
  final CallsAndEmailsService _service = locator<CallsAndEmailsService>();

  final String number = "07968202222";
  final String emailID = "support@teleforce.in";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Support'),
      ),
      body: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            RaisedButton(
              child: Text(
                "Call    $number",
                style: TextStyle(color: Colors.white),
              ),
              color: Color.fromRGBO(49, 87, 110, 1.0),
              onPressed: () => _service.call(number),
            ),
            SizedBox(height: 20),
            RaisedButton(
              child: Text(
                "Email   $emailID",
                style: TextStyle(color: Colors.white),
              ),
              color: Color.fromRGBO(49, 87, 110, 1.0),
              onPressed: () => _service.sendEmail(emailID),
            ),
          ],
        ),
      ),
    );
  }
}
