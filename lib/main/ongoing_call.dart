import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:TeleVoice/main/utils/global.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:TeleVoice/main/widgets/action_button.dart';
import 'package:TeleVoice/main/widgets/feedback_radio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:logger/logger.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:TeleVoice/main/model/call_log_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:http/http.dart' as http;

import 'model/agent_group_extension_model.dart';
import 'model/did_model.dart';

class OngoingCallWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _OngoingCallWidgetState();
  }
}

class _OngoingCallWidgetState extends State<OngoingCallWidget> {
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  Future<List<CallLogModelData>> _futureCurrentCall;
  StreamController<List<CallLogModelData>> _userController;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  String _timeLabel = '00:00';
  Timer _timer;
  bool isDataAvail = false;
  bool isCallFound = false;
  Timer _apiTimer;
  int call_id = 0;

  @override
  void initState() {
    // TODO: implement initState
    _loadSettings();
    super.initState();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();
    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
    });

    Future.delayed(Duration(seconds: 1), () async {
      G.initSocket();
      await G.socketUtils.initSocket(userID.toString());
      G.socketUtils.connectToSocket();
      G.socketUtils.setConnectListener(onConnect);
      G.socketUtils.setOnAgentCallReceivedListener(onDataReceived);
    });

    _userController = new StreamController();
    _apiTimer = Timer.periodic(Duration(seconds: 5), (_) {
      if (!isDataAvail && !isCallFound) {
        loadDetails();
      } else if (!isDataAvail && isCallFound) {
        isCallFound = false;
        _saveFeedback(call_id);
      } else {
        loadDetails();
        _startTimer();
      }
    });
  }

  onConnect(data) {
    print('==============>>> in dialpad Connected $data');
  }

  onDataReceived(data) {
    print('==============>>> in dialpad data $data');
  }

  loadDetails() async {
    _futureCurrentCall = GetCurrentCallData(userID, accessToken);
    _futureCurrentCall.then((value) async {
      print('======>>> size of ${value.length}');
      if (value.length > 0) {
        isDataAvail = true;
        isCallFound = true;
      } else {
        isDataAvail = false;
      }
      _userController.add(value);

      return value;
    });
  }

  /*Future<Null> _handleRefresh() async {
    _futureCurrentCall = GetCurrentCallData(userID, accessToken);
    _futureCurrentCall.then((value) async {
      print('======>>> LoadDetails of ${value[0].agentCallStartTime}');
      _userController.add(value);
      return null;
    });
  }*/

  @override
  void dispose() {
    // TODO: implement dispose
    _apiTimer.cancel();
    _userController.close();
    super.dispose();
  }

  void _startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      Duration duration = Duration(seconds: timer.tick);
      if (mounted) {
        this.setState(() {
          _timeLabel = [duration.inMinutes, duration.inSeconds]
              .map((seg) => seg.remainder(60).toString().padLeft(2, '0'))
              .join(':');
        });
      } else {
        _timer.cancel();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Current Call"),
      ),
      body: Center(
        child: StreamBuilder(
          stream: _userController.stream,
          builder: (BuildContext context,
              AsyncSnapshot<List<CallLogModelData>> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Center(
                  child: Text(
                    'Getting Call...',
                    style: new TextStyle(
                        fontSize: 18.0,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold),
                  ),
                );
                break;
              case ConnectionState.waiting:
                return Center(
                  child: Text(
                    'Getting Call...',
                    style: new TextStyle(
                        fontSize: 20.0,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold),
                  ),
                );
                break;
              case ConnectionState.active:
                if (snapshot.hasData) {
                  List<CallLogModelData> data = snapshot.data;

                  if (data.length == 0) {
                    return Center(
                      child: Text(
                        'Getting Call...',
                        style: new TextStyle(fontSize: 20.0),
                      ),
                    );
                  } else {
                    call_id = data[0].id;
                    return new SafeArea(
                      child: Center(
                        child: Stack(
                          overflow: Overflow.visible,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 8.0, bottom: 6.0),
                              child: Align(
                                alignment: Alignment.topCenter,
                                child: Text(
                                  'Calling...',
                                  style: new TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            Align(
                              alignment: Alignment(0.0, -0.72),
                              child: Text(
                                data[0].callerName,
                                style: new TextStyle(
                                    fontSize: 22.0, color: Colors.black),
                              ),
                            ),
                            Align(
                              alignment: Alignment(0.0, -0.6),
                              child: Text(
                                data[0].callerNumber,
                                style: new TextStyle(
                                    fontSize: 18.0, color: Colors.black54),
                              ),
                            ),
                            Align(
                              alignment: Alignment(0.0, -0.5),
                              child: Text(_timeLabel,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.black54)),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Image.asset(
                                "images/calling1.gif",
                                height: 220.0,
                                width: 240.0,
                              ),
                            ),
                            Align(
                              alignment: Alignment(0.0, 0.72),
                              child: ActionButton(
                                title: "transfer",
                                icon: Icons.phone_forwarded,
                                onPressed: () async {
                                  showRoundedModalBottomSheet(
                                      color: Theme.of(context).canvasColor,
                                      dismissOnTap: false,
                                      context: context,
                                      builder: (builder) {
                                        return Container(
                                            height: 280.0,
                                            color: Colors.transparent,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: Scaffold(
                                                resizeToAvoidBottomPadding:
                                                    false,
                                                body: SingleChildScrollView(
                                                  child: Column(
                                                    children: <Widget>[
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 8.0),
                                                        child:
                                                            ModalDrawerHandle(),
                                                      ),
                                                      Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(16.0),
                                                          child: Text(
                                                              'Transfer',
                                                              style: new TextStyle(
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  fontSize:
                                                                      20.0))),
                                                      Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(16.0),
                                                          child: Text(
                                                              'To transfer this call Enter #(extension code) from dailpad,'
                                                              ' choose extension code from below list. for example if extension code is  101 then type #101.',
                                                              style:
                                                                  new TextStyle(
                                                                      fontSize:
                                                                          16.0))),
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(16.0),
                                                        child: FutureBuilder<
                                                            List<
                                                                AgentTransferModelData>>(
                                                          future:
                                                              fetchAgentGroupExtension(
                                                                  userID,
                                                                  accessToken),
                                                          builder: (BuildContextcontext,
                                                              AsyncSnapshot<
                                                                      List<
                                                                          AgentTransferModelData>>
                                                                  snapshot) {
                                                            if (snapshot
                                                                .hasData) {
                                                              List<AgentTransferModelData>
                                                                  data =
                                                                  snapshot.data;

                                                              if (data.length ==
                                                                  0) {
                                                                return Container(
                                                                    child: Text(
                                                                        'No extension available'));
                                                              } else {
                                                                return _listData(
                                                                    data);
                                                              }
                                                            } else if (snapshot
                                                                .hasError) {
                                                              return Text(
                                                                  "${snapshot.error}");
                                                            }
                                                            return CircularProgressIndicator();
                                                          },
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ));
                                      });
                                },
                              ),
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: RaisedButton(
                                child: Text(
                                  "Give Feedback",
                                  style: TextStyle(fontSize: 18),
                                ),
                                onPressed: () => _saveFeedback(data[0].id),
                                color: Color.fromRGBO(49, 87, 110, 1.0),
                                textColor: Colors.white,
                                splashColor: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                } else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      'No current call found!',
                      style: new TextStyle(fontSize: 20.0),
                    ),
                  );
                } else {
                  return Center(
                    child: Text(
                      'Getting Call...',
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold),
                    ),
                  );
                }
                break;
              case ConnectionState.done:
                Logger().d("======>> in Done state");
                break;
            }
            return Center(
              child: Text(
                'Getting Call...',
                style:
                    new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
            );
          },
        ),
      ),
    );
  }

  ListView _listData(data) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 2.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: GroovinExpansionTile(
              leading: CircleAvatar(
                child: Text((index + 1).toString()),
                backgroundColor: Theme.of(context).primaryColor,
              ),
              title: Text(
                data[index].groupname.toString(),
                style: TextStyle(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Colors.black
                      : Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle:
                  Text(' TransferText:- ' + data[index].transferext.toString()),
            ),
          ),
        );
      },
    );
  }

  _saveFeedback(int id) {
    Navigator.pop(context);
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => FeedbackRadio(id.toString(), false, '', '', ''),
        ));
  }

  Future<List<CallLogModelData>> GetCurrentCallData(
      int id, String token) async {
    final http.Response response = await http.post(
      Constant.GET_AGENT_CURRENT_CALL,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {'id': id.toString()},
    );

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return CallLogModel.fromJson(json).data;
    } else {
      throw Exception('=====>>> Failed to fetch data');
    }
  }

  Future<List<AgentTransferModelData>> fetchAgentGroupExtension(
      int id, String token) async {
    final response = await http.get(
      Constant.GET_AGENT_GROUP + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return AgentTransferModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  void _attendTransfer() {
    fetchAgentGroupExtension(userID, accessToken);
  }

  void _blindTransfer() {}
}
