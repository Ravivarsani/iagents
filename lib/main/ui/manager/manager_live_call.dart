import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';
import 'package:TeleVoice/main/model/live_call_model.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LiveCall extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LiveCallState();
  }
}

class _LiveCallState extends State<LiveCall> {
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  Future<List<LiveCallModelData>> _futureLiveCall;
  StreamController<List<LiveCallModelData>> _userController;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isDataAvail = false;
  Timer _apiTimer;
  bool isExpanded = false;
  String finalDate = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();
    var date = new DateTime.now().toString();

    var dateParse = DateTime.parse(date);

    var formattedDate = "${dateParse.year}-${dateParse.month}-${dateParse.day}";

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
      finalDate = formattedDate.toString();
    });

    _userController = new StreamController();
    _apiTimer = Timer.periodic(Duration(seconds: 2), (_) {
      if (!isDataAvail) {
        loadDetails();
      } else {
        loadDetails();
//        _userController.close();
      }
    });
  }

  loadDetails() async {
    _futureLiveCall = GetLiveCallData(userID, accessToken);
    _futureLiveCall.then((value) async {
      print('======>>> size of ${value.length}');
      if (value.length > 0) {
        isDataAvail = true;
      } else {
        isDataAvail = false;
      }
      _userController.add(value);
      return value;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _apiTimer.cancel();
    _userController.close();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Live Call"),
      ),
      body: Center(
        child: StreamBuilder(
          stream: _userController.stream,
          builder: (BuildContext context,
              AsyncSnapshot<List<LiveCallModelData>> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Center(
                  child: Text(
                    'Searching for Call...',
                    style: new TextStyle(
                        fontSize: 18.0,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold),
                  ),
                );
                break;
              case ConnectionState.waiting:
                return Center(
                  child: Text(
                    'Searching for Call...',
                    style: new TextStyle(
                        fontSize: 20.0,
                        color: Colors.black54,
                        fontWeight: FontWeight.bold),
                  ),
                );
                break;
              case ConnectionState.active:
                if (snapshot.hasData) {
                  List<LiveCallModelData> data = snapshot.data;

                  if (data.length == 0) {
                    return Center(
                      child: Text(
                        'No Live call found!',
                        style: new TextStyle(
                            fontSize: 20.0,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold),
                      ),
                    );
                  } else {
                    return _listData(data);
                  }
                } else if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      'No Live call found!',
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold),
                    ),
                  );
                } else {
                  return Center(
                    child: Text(
                      'Getting Call...',
                      style: new TextStyle(
                          fontSize: 20.0,
                          color: Colors.black54,
                          fontWeight: FontWeight.bold),
                    ),
                  );
                }
                break;
              case ConnectionState.done:
                Logger().d("======>> in Done state");
                break;
            }
            return Center(
              child: Text(
                'Getting Call...',
                style:
                    new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
            );
          },
        ),
      ),
    );
  }

  ListView _listData(data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 2.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: GroovinExpansionTile(
              leading: CircleAvatar(
                child: Text((index + 1).toString()),
                backgroundColor: Theme.of(context).primaryColor,
              ),
              title: Text(
                data[index].CallerName.toString(),
                style: TextStyle(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Colors.black
                      : Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(data[index].CallerNumber),
              onExpansionChanged: (value) {
                setState(() {
                  isExpanded = value;
                });
              },
              inkwellRadius: !isExpanded
                  ? BorderRadius.all(Radius.circular(5.0))
                  : BorderRadius.only(
                      topRight: Radius.circular(5.0),
                      topLeft: Radius.circular(5.0),
                    ),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0, bottom: 4.0),
                      child: Text(
                        DateFormat('dd/MM/yyyy HH:mm:ss').format(
                            DateTime.parse(data[index].CallStartTime)
                                .toLocal()),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4.0, right: 16),
                      child: Text(data[index].CustomerStatus.toString()),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.phone_in_talk),
                      onPressed: () async {
                        Map<String, String> data1 =
                            await showRoundedModalBottomSheet(
                          color: Theme.of(context).canvasColor,
                          dismissOnTap: false,
                          context: context,
                          builder: (builder) {
                            return Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: MyPageForMobile('Call Listen'),
                            );
                          },
                        );

                        if (data1.isNotEmpty) {
                          String mobilenumber;

                          data1.forEach((key, value) {
                            if (key == 'mobile') {
                              mobilenumber = value;
                            }
                          });

                          print('===========>>> $mobilenumber');

                          LiveCallMonitor(userID, data[index].uniqueid,
                                  accessToken, 'listen', mobilenumber)
                              .then((value) {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(value.msg),
                            ));
                          });
                        }
                      },
                      tooltip: "Listen Call",
                    ),
                    IconButton(
                      icon: Icon(Icons.call_split),
                      onPressed: () async {
                        Map<String, String> data1 =
                            await showRoundedModalBottomSheet(
                          color: Theme.of(context).canvasColor,
                          dismissOnTap: false,
                          context: context,
                          builder: (builder) {
                            return Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: MyPageForMobile('Call Whishper'),
                            );
                          },
                        );

                        if (data1.isNotEmpty) {
                          String mobilenumber;

                          data1.forEach((key, value) {
                            if (key == 'mobile') {
                              mobilenumber = value;
                            }
                          });

                          print('===========>>> $mobilenumber');

                          LiveCallMonitor(userID, data[index].uniqueid,
                                  accessToken, 'whisper', mobilenumber)
                              .then((value) {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(value.msg),
                            ));
                          });
                        }
                      },
                      tooltip: "Whishper Call",
                    ),
                    IconButton(
                      icon: Icon(Icons.call_merge),
                      onPressed: () async {
                        Map<String, String> data1 =
                            await showRoundedModalBottomSheet(
                          color: Theme.of(context).canvasColor,
                          dismissOnTap: false,
                          context: context,
                          builder: (builder) {
                            return Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: MyPageForMobile('Call Barge'),
                            );
                          },
                        );

                        if (data1 != null) {
                          String mobilenumber;

                          data1.forEach((key, value) {
                            if (key == 'mobile') {
                              mobilenumber = value;
                            }
                          });

                          print('===========>>> $mobilenumber');

                          LiveCallMonitor(userID, data[index].uniqueid,
                                  accessToken, 'barge', mobilenumber)
                              .then((value) {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text(value.msg),
                            ));
                          });
                        }
                      },
                      tooltip: "Barge Call",
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Future<List<LiveCallModelData>> GetLiveCallData(int id, String token) async {
    final http.Response response = await http.post(
      Constant.GET_LIVE_CALL,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'userid': id.toString(),
        'startdate': finalDate,
        'enddate': finalDate,
      },
    );

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return LiveCallModel.fromJson(json).data;
    } else {
      throw Exception('=====>>> Failed to fetch data');
    }
  }

  Future<uuid_model> LiveCallMonitor(
      int id, String uniqueid, String token, String type, String mobile) async {
    final http.Response response = await http.post(
      Constant.POST_LIVE_CALL_MONITOR,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'uniqueid': uniqueid,
        'userid': id.toString(),
        'mobile': mobile,
        'type': type,
      },
    );
    Logger().d("=======>>> response " + response.body.toString());
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return uuid_model.fromJson(json);
    } else {
      throw Exception(
          '=====>> Failed to post call monitor. ' + response.body.toString());
    }
  }
}

class MyPageForMobile extends StatefulWidget {
  String text;

  MyPageForMobile(String title) {
    this.text = title;
  }

  @override
  _MyPageWithAudioState createState() => _MyPageWithAudioState(text);
}

class _MyPageWithAudioState extends State<MyPageForMobile> {
  TextEditingController _usernameController = TextEditingController();
  String title;

  _MyPageWithAudioState(String text) {
    this.title = text;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: ModalDrawerHandle(),
              ),
              Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(title,
                      style: new TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18.0))),
              Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter Mobile';
                      }
                      return null;
                    },
                    obscureText: false,
                    controller: _usernameController,
                    decoration: new InputDecoration(
                      labelText: "Mobile Number",
                      fillColor: Colors.white,
                      border: new OutlineInputBorder(
                        borderRadius: new BorderRadius.circular(25.0),
                        borderSide: new BorderSide(),
                      ),
                    ),
                  )),
              Padding(
                padding: new EdgeInsets.all(12),
                child: new RaisedButton(
                  padding: const EdgeInsets.all(8.0),
                  textColor: Colors.white,
                  color: Colors.blue,
                  splashColor: Colors.grey,
                  onPressed: _Call,
                  child: new Text("Call"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _Call() {
    if (_usernameController.text.isEmpty) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Enter Mobile Number'),
      ));
    } else {
      Future.delayed(Duration(seconds: 1)).then((value) {
        setState(() {
          Map<String, String> createDoc = new HashMap();
          createDoc['mobile'] = _usernameController.text;
          Navigator.pop(context, createDoc);
        });
      });
    }
  }
}
