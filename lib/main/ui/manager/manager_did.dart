import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:TeleVoice/main/widgets/dropdown_search.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/activity_model.dart';
import 'package:TeleVoice/main/model/agent_model.dart';
import 'package:TeleVoice/main/model/agentgroup_model.dart';
import 'package:TeleVoice/main/model/conference_model.dart';
import 'package:TeleVoice/main/model/did_config_model.dart';
import 'package:TeleVoice/main/model/did_model.dart';
import 'package:TeleVoice/main/model/ivrlist_model.dart';
import 'package:TeleVoice/main/model/manager_call_log.dart';
import 'package:TeleVoice/main/model/sound_list_model.dart';
import 'package:TeleVoice/main/ui/manager/filter_widget.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:TeleVoice/main/widgets/show_feedback.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:http/http.dart' as http;
import 'package:TeleVoice/main/utils/string_duration.dart';

class ManagerDid extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ManagerDidState();
  }
}

class _ManagerDidState extends State<ManagerDid> {
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  bool isExpanded = false;
  Future<List<DidModelData>> mFuture;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');

      mFuture = fetchDID(userID, accessToken);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('DID Configuration'),
        ),
        body: Center(
          child: _ListData(),
        ));
  }

  FutureBuilder _ListData() {
    return FutureBuilder<List<DidModelData>>(
      future: mFuture,
      builder:
          (BuildContext context, AsyncSnapshot<List<DidModelData>> snapshot) {
        if (snapshot.hasData) {
          List<DidModelData> data = snapshot.data;

          if (data.length == 0) {
            return Container(child: Text('No logs available'));
          } else {
            // return _dialersData(data);
            return _listData(data);
          }
        } else if (snapshot.hasError) {
          // return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    );
  }

  ListView _listData(data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        String type = '';
        String action = '';
        if (data[index].Type == 0) {
          type = 'Terminate Call';
          action = 'Hang Up';
        } else if (data[index].Type == 1) {
          type = 'Announcement';
          action = '';
        } else if (data[index].Type == 2) {
          type = 'Agents Group';
          action = '';
        } else if (data[index].Type == 3) {
          type = 'IVR';
          action = '';
        } else if (data[index].Type == 4) {
          type = 'Voice Mail';
          action = '';
        } else if (data[index].Type == 6) {
          type = 'Miss Call';
          action = 'Hang Up';
        } else if (data[index].Type == 7) {
          type = 'Conference';
          action = '';
        } else if (data[index].Type == 8) {
          type = 'Click to Call';
          action = '';
        } else if (data[index].Type == 9) {
          type = 'Outgoing / Incoming';
          action = '';
        }

        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 2.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: GroovinExpansionTile(
              leading: CircleAvatar(
                child: Text((index + 1).toString()),
                backgroundColor: Theme.of(context).primaryColor,
              ),
              title: Text(
                type,
                style: TextStyle(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Colors.black
                      : Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(data[index].didNumber),
              onExpansionChanged: (value) {
                setState(() {
                  isExpanded = value;
                });
              },
              inkwellRadius: !isExpanded
                  ? BorderRadius.all(Radius.circular(5.0))
                  : BorderRadius.only(
                      topRight: Radius.circular(5.0),
                      topLeft: Radius.circular(5.0),
                    ),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0, bottom: 4.0),
                      child: Text(data[index].Description.toString()),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4.0, right: 16),
                      child: Text(
                        DateFormat('dd/MM/yyyy HH:mm:ss').format(
                            DateTime.parse(data[index].assginDate).toLocal()),
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.edit,
                        color: Colors.blueAccent,
                      ),
                      onPressed: () async {
                        bool b = await showRoundedModalBottomSheet(
                          color: Theme.of(context).canvasColor,
                          dismissOnTap: false,
                          context: context,
                          builder: (builder) {
                            return Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: EditDIDLayout(
                                  data[index].Type, data[index].id),
                            );
                          },
                        );

                        if (b) {
                          setState(() {
                            mFuture = fetchDID(userID, accessToken);
                          });
                        }
                      },
                      tooltip: "Edit did",
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<List<DidModelData>> fetchDID(int id, String token) async {
    final response = await http.get(
      Constant.GET_DID + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return DidModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }
}

class EditDIDLayout extends StatefulWidget {
  int type, id;

  EditDIDLayout(int type, id) {
    this.type = type;
    this.id = id;
  }

  @override
  _EditDIDLayoutState createState() => _EditDIDLayoutState(type, id);
}

class _EditDIDLayoutState extends State<EditDIDLayout> {
  String agentID = '';
  SharedPreferences _preferences;
  int userID, type, id, action;
  String accessToken;
  List<IVRListModelData> mListIvr;
  List<AgentGroupModelData> mListAgentGroup;
  List<ConferenceModelData> mListConference;
  List<SoundListModelData> mListSound;
  List<String> typeslist = [
    'Terminate Call',
    'Announcement',
    'Agents Group',
    'IVR',
    'Voice Mail',
    'Miss Call',
    'Conference',
    'Click to Call',
    'Outgoing / Incoming',
  ];
  List<String> hangup = [
    'Hang Up',
  ];

  _EditDIDLayoutState(int date, int id) {
    if (date > 5) {
      this.type = date - 1;
    } else {
      this.type = date;
    }
    this.id = id;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSetting();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: ModalDrawerHandle(),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 12.0),
              child: Text('Edit DID Configuration',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 18.0))),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: DropdownSearch<String>(
              mode: Mode.MENU,
              showSelectedItem: true,
              items: typeslist,
              itemAsString: (String s) => s,
              label: "Type",
              hint: "Select Type",
              selectedItem: typeslist.elementAt(type).toString(),
              onChanged: (String type) async {
                onClickType(type);
              },
            ),
          ),
          type < 10 ? makeActionView() : Container(),
          Padding(
            padding: new EdgeInsets.only(
                left: 24.0, top: 12.0, right: 8.0, bottom: 4.0),
            child: new RaisedButton(
              padding: const EdgeInsets.all(8.0),
              textColor: Colors.white,
              color: Colors.blue,
              splashColor: Colors.grey,
              onPressed: _SaveDID,
              child: new Text("Save"),
            ),
          ),
        ],
      ),
    );
  }

  _SaveDID() {
    postDIDConfig(action, id).then((value) => Fluttertoast.showToast(
        msg: value.msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
        textColor: Colors.white,
        fontSize: 16.0));

    Future.delayed(Duration(seconds: 1)).then((value) {
      setState(() {
        Navigator.pop(context, true);
      });
    });
  }

  Future<DidConfigModel> postDIDConfig(int action, int did_id) async {
    int t = type > 4 ? type + 1 : type;
    final http.Response response = await http.post(
      Constant.UPDATE_DID,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + accessToken,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'did_id': did_id.toString(),
        'action': action.toString(),
        'type': t.toString(),
      },
    );

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return DidConfigModel.fromJson(json);
    } else {
      throw Exception('=====>>> Failed to fetch data');
    }
  }

  Future<List<IVRListModelData>> fetchIVRLIST(int id, String token) async {
    final response = await http.get(
      Constant.GET_IVR_LIST + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return IVRListModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  Future<List<SoundListModelData>> fetchSoundLIST(int id, String token) async {
    final response = await http.get(
      Constant.GET_SOUND_LIST + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return SoundListModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else if (response.statusCode == 304) {
      print(('=========>> 304 code'));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  Future<List<AgentGroupModelData>> fetchAgentGroup(
      int id, String token) async {
    final response = await http.get(
      Constant.GET_HUNT_AGENT_GROUP + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return AgentGroupModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  Future<List<ConferenceModelData>> fetchConference(
      int id, String token) async {
    final response = await http.get(
      Constant.GET_CONFERENCE_CALL + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return ConferenceModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  void loadSetting() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
    });

    if (type == 0) {
    } else if (type == 1) {
      fetchSoundLIST(userID, accessToken).then((value) {
        setState(() {
          mListSound = value;
        });
      });
    } else if (type == 2) {
      fetchAgentGroup(userID, accessToken).then((value) {
        setState(() {
          mListAgentGroup = value;
        });
      });
    } else if (type == 3) {
    } else if (type == 4) {
      fetchSoundLIST(userID, accessToken).then((value) {
        setState(() {
          mListSound = value;
        });
      });
    } else if (type == 5) {
    } else if (type == 6) {
      fetchConference(userID, accessToken).then((value) {
        setState(() {
          mListConference = value;
        });
      });
    } else if (type == 7) {
    } else if (type == 8) {
      fetchIVRLIST(userID, accessToken).then((value) {
        setState(() {
          mListIvr = value;
        });
      });
    }
  }

  Widget makeActionView() {
    if (type == 0) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<String>(
          mode: Mode.MENU,
          items: hangup,
          label: "Action",
          hint: "Select Action",
          onChanged: (String s) => action = 0,
        ),
      );
    } else if (type == 1) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<SoundListModelData>(
          mode: Mode.MENU,
          items: mListSound,
          itemAsString: (SoundListModelData u) => u.originalfilename,
          label: "Action",
          hint: "Select Action",
          onChanged: (SoundListModelData data) => action = data.id,
        ),
      );
    } else if (type == 2) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<AgentGroupModelData>(
          mode: Mode.MENU,
          items: mListAgentGroup,
          itemAsString: (AgentGroupModelData u) => u.GroupName,
          label: "Action",
          hint: "Select Action",
          onChanged: (AgentGroupModelData data) => action = data.GroupID,
        ),
      );
    } else if (type == 3) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<IVRListModelData>(
          mode: Mode.MENU,
          items: mListIvr,
          itemAsString: (IVRListModelData u) => u.IvrName,
          label: "Action",
          hint: "Select Action",
          onChanged: (IVRListModelData data) => action = data.BuilderID,
        ),
      );
    } else if (type == 4) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<SoundListModelData>(
          mode: Mode.MENU,
          items: mListSound,
          itemAsString: (SoundListModelData u) => u.originalfilename,
          label: "Action",
          hint: "Select Action",
          onChanged: (SoundListModelData data) => action = data.id,
        ),
      );
    } else if (type == 5) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<String>(
          mode: Mode.MENU,
          items: hangup,
          label: "Action",
          hint: "Select Action",
          onChanged: (String s) => action = 0,
        ),
      );
    } else if (type == 6) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<ConferenceModelData>(
          mode: Mode.MENU,
          items: mListConference,
          itemAsString: (ConferenceModelData u) => u.confName,
          label: "Action",
          hint: "Select Action",
          onChanged: (ConferenceModelData data) => action = data.confId,
        ),
      );
    } else if (type == 7) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<IVRListModelData>(
          mode: Mode.MENU,
          items: mListIvr,
          itemAsString: (IVRListModelData u) => u.IvrName,
          label: "Action",
          hint: "Select Action",
          onChanged: (IVRListModelData data) =>
              agentID = data.AnnouncementID.toString(),
        ),
      );
    } else if (type == 8) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: DropdownSearch<IVRListModelData>(
          mode: Mode.MENU,
          items: mListIvr,
          itemAsString: (IVRListModelData u) => u.IvrName,
          label: "Action",
          hint: "Select Action",
          onChanged: (IVRListModelData data) => action = data.BuilderID,
        ),
      );
    }
  }

  onClickType(String type1) {
    if (type1 == 'Terminate Call') {
      type = 0;
      setState(() {
        hangup;
      });
    } else if (type1 == 'Announcement') {
      type = 1;
      fetchSoundLIST(userID, accessToken).then((value) {
        setState(() {
          mListSound = value;
        });
      });
    } else if (type1 == 'Agents Group') {
      type = 2;
      fetchAgentGroup(userID, accessToken).then((value) {
        setState(() {
          mListAgentGroup = value;
        });
      });
    } else if (type1 == 'IVR') {
      type = 3;
      fetchIVRLIST(userID, accessToken).then((value) {
        setState(() {
          mListIvr = value;
        });
      });
    } else if (type1 == 'Voice Mail') {
      type = 4;
      fetchSoundLIST(userID, accessToken).then((value) {
        setState(() {
          mListSound = value;
        });
      });
    } else if (type1 == 'Miss Call') {
      type = 5;
      setState(() {
        hangup;
      });
    } else if (type1 == 'Conference') {
      type = 6;
      fetchConference(userID, accessToken).then((value) {
        setState(() {
          mListConference = value;
        });
      });
    } else if (type1 == 'Click to Call') {
      type = 7;
    } else if (type1 == 'Outgoing / Incoming') {
      type = 8;
      fetchIVRLIST(userID, accessToken).then((value) {
        setState(() {
          mListIvr = value;
        });
      });
    }
  }
}
