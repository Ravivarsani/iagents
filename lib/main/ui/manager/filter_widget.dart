import 'package:flutter/material.dart';

class FilterCallChip extends StatefulWidget {

  @override
  _FilterCallChipState createState() => _FilterCallChipState();
}

class _FilterCallChipState extends State<FilterCallChip> {
  bool isSelected = false;
  List<String> selectedChoices = List();

  List<String> reportList = [
    "Agent",
    "Start Date",
    "End Date",
    "Status"
  ];

  _buildChoiceList() {
    List<Widget> choices = List();
    reportList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.all(2.0),
        child: ChoiceChip(
          label: Text(item),
          selected: selectedChoices.contains(item),
          onSelected: (selected) {
            setState(() {
              selectedChoices.contains(item)
                  ? selectedChoices.remove(item)
                  : selectedChoices.add(item);
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      children: _buildChoiceList(),
    );
  }
}
