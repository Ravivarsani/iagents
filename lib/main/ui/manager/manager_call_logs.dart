import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:TeleVoice/main/widgets/dropdown_search.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/activity_model.dart';
import 'package:TeleVoice/main/model/agent_model.dart';
import 'package:TeleVoice/main/model/manager_call_log.dart';
import 'package:TeleVoice/main/ui/manager/filter_widget.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:TeleVoice/main/widgets/show_feedback.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:http/http.dart' as http;
import 'package:TeleVoice/main/utils/string_duration.dart';

class ManagerCallLogs extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ManagerCallLogsState();
  }
}

class _ManagerCallLogsState extends State<ManagerCallLogs> {
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  bool isExpanded = false;
  String finalDate = '';
  Future<List<ManagerCallLogModelData>> mFuture;
  bool isForSearch = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();

    var date = new DateTime.now().toString();

    var dateParse = DateTime.parse(date);

    var formattedDate = "${dateParse.year}-${dateParse.month}-${dateParse.day}";

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
      finalDate = formattedDate.toString();

      mFuture = GetCallLogData(userID, accessToken, finalDate, finalDate, '');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Call Logs'),
          /*actions: <Widget>[
            IconButton(
              icon: const Icon(Icons.filter_list),
              tooltip: 'Filter',
              onPressed: () async {
                Map<String, String> data = await showRoundedModalBottomSheet(
                  color: Theme.of(context).canvasColor,
                  dismissOnTap: false,
                  context: context,
                  builder: (builder) {
                    return Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: FilterLayout(finalDate),
                    );
                  },
                );

                if (data.isNotEmpty) {
                  String sdate, edate, aid;

                  Map map = {
                    'agentid': aid,
                    'startdate': sdate,
                    'enddate': edate,
                  };

                  data.forEach((key, value) {
                    if (key == 'agentID') {
                      aid = value;
                    } else if (key == 'startdate') {
                      sdate = value;
                    } else if (key == 'enddate') {
                      edate = value;
                    }
                  });

                  setState(() {
                    Future<List<ManagerCallLogModelData>> mFutur2  = GetSearchedCallLogData(userID, accessToken, map, sdate, edate, aid);
//                    mFuture = GetSearchedCallLogData(userID, accessToken, map, sdate, edate, aid);
                  });
                }
              },
            ),
          ],*/
        ),
        body: Center(
          child: _ListData(),
        ));
  }

  FutureBuilder _ListData() {
    return FutureBuilder<List<ManagerCallLogModelData>>(
      future: mFuture,
      builder: (BuildContext context,
          AsyncSnapshot<List<ManagerCallLogModelData>> snapshot) {
        if (snapshot.hasData) {
          List<ManagerCallLogModelData> data = snapshot.data;

          if (data.length == 0) {
            return Container(
                child: Text(
              'No logs available',
              style: new TextStyle(fontSize: 20.0),
            ));
          } else {
            // return _dialersData(data);
            return _listData(data);
          }
        } else if (snapshot.hasError) {
          // return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    );
  }

  ListView _listData(data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 2.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: GroovinExpansionTile(
              leading: CircleAvatar(
                child: Text((index + 1).toString()),
                backgroundColor: Theme.of(context).primaryColor,
              ),
              title: Text(
                data[index].CallerName.toString(),
                style: TextStyle(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Colors.black
                      : Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(data[index].CallerNumber),
              onExpansionChanged: (value) {
                setState(() {
                  isExpanded = value;
                });
              },
              inkwellRadius: !isExpanded
                  ? BorderRadius.all(Radius.circular(5.0))
                  : BorderRadius.only(
                      topRight: Radius.circular(5.0),
                      topLeft: Radius.circular(5.0),
                    ),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0, bottom: 4.0),
                      child: Text(
                        DateFormat('dd/MM/yyyy HH:mm:ss').format(
                            DateTime.parse(data[index].CallStartTime)
                                .toLocal()),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4.0, right: 16),
                      child: Text(data[index].CallTalkTime.toString() + ' Sec'),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.feedback,
                        color: Colors.blueAccent,
                      ),
                      onPressed: () async {
                        showRoundedModalBottomSheet(
                          color: Theme.of(context).canvasColor,
                          dismissOnTap: false,
                          context: context,
                          builder: (builder) {
                            return Container(
                              height: 300.0,
                              color: Colors.transparent,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Scaffold(
                                  resizeToAvoidBottomPadding: false,
                                  body: SingleChildScrollView(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 8.0),
                                          child: ModalDrawerHandle(),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: Text('Feedback',
                                                style: new TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 20.0))),
                                        ShowFeedbackPage(
                                            data[index].id,
                                            data[index].feedback,
                                            data[index].note,
                                            data[index].scheduledate,
                                            false),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      },
                      tooltip: "View Feedback",
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.play_circle_filled,
                        color: Colors.green,
                      ),
                      onPressed: () async {
                        showRoundedModalBottomSheet(
                          color: Theme.of(context).canvasColor,
                          dismissOnTap: false,
                          context: context,
                          builder: (builder) {
                            return Container(
                              height: 280.0,
                              color: Colors.transparent,
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Scaffold(
                                  body: Container(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 8.0),
                                          child: ModalDrawerHandle(),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.all(16.0),
                                            child: Text('Recording',
                                                style: new TextStyle(
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18.0))),
                                        MyPageWithAudio(
                                            Constant.RECORDING_PATH +
                                                data[index].RecPath),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      },
                      tooltip: "Play Audio",
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<List<ManagerCallLogModelData>> GetCallLogData(int id, String token,
      String startdate, String enddate, String agentid) async {
    final http.Response response = await http.get(
      Constant.GET_MANAGER_CALL_LOGS + id.toString(),
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    );

    Logger().d("=======>>> Manager " + response.body.toString());

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return ManagerCallLogModel.fromJson(json).data;
    } else {
      throw Exception('=====>>> Failed to fetch data');
    }
  }

  Future<List<ManagerCallLogModelData>> GetSearchedCallLogData(
      int id,
      String token,
      Map data,
      String startdate,
      String enddate,
      String agentid) async {
    print(' =================>>> ');

    final http.Response response = await http.post(
      Constant.GET_MANAGER_SEARCH_CALL_LOGS,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'filter': data,
        'user_id': id.toString(),
        'pageNumber': '0',
        'pageSize': '50',
      },
    );

    Logger().d("=======>>> Manager111 " + response.body.toString());

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      Logger().d("=======>>> json Manager11 " + json);
      return ManagerCallLogModel.fromJson(json).data;
    } else {
      throw Exception('=====>>> Failed to fetch data');
    }
  }
}

class FilterLayout extends StatefulWidget {
  String date;

  FilterLayout(String finalDate) {
    this.date = finalDate;
  }

  @override
  _FilterLayoutState createState() => _FilterLayoutState(date);
}

class _FilterLayoutState extends State<FilterLayout> {
  TextEditingController _StartDateController = TextEditingController();
  TextEditingController _EndDateController = TextEditingController();
  DateTime pickedDate, endDate;
  String todayDate, agentID = '';
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  List<AgentModelData> mList;

  _FilterLayoutState(String date) {
    this.todayDate = date;
  }

  _pickDate() async {
    DateTime date = await showDatePicker(
      context: context,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
      initialDate: pickedDate,
    );

    if (date != null)
      setState(() {
        pickedDate = date;
        _StartDateController.text =
            '${pickedDate.year}-${pickedDate.month}-${pickedDate.day}';
      });
  }

  _pickEndDate() async {
    DateTime date = await showDatePicker(
      context: context,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
      initialDate: endDate,
    );

    if (date != null)
      setState(() {
        endDate = date;
        _EndDateController.text =
            '${endDate.year}-${endDate.month}-${endDate.day}';
      });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadSetting();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: ModalDrawerHandle(),
          ),
          Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 12.0),
              child: Text('Select Filter',
                  style: new TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 18.0))),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: DropdownSearch<AgentModelData>(
              mode: Mode.MENU,
              showSelectedItem: false,
              items: mList,
              itemAsString: (AgentModelData u) => u.accountName,
              label: "Agent",
              hint: "Select Agent",
              onChanged: (AgentModelData data) =>
                  agentID = data.accountId.toString(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: new TextFormField(
              decoration: new InputDecoration(
                labelText: "Start Date",
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                    borderSide:
                        new BorderSide(color: Theme.of(context).primaryColor)),
                suffixIcon: new Icon(Icons.date_range),
                //fillColor: Colors.green
              ),
              validator: (val) {
                if (val.length == 0) {
                  return "Date cannot be empty";
                } else {
                  return null;
                }
              },
              keyboardType: TextInputType.datetime,
              maxLines: null,
              controller: _StartDateController,
              onTap: () {
                _pickDate();
              },
              style: new TextStyle(
                fontFamily: "Poppins",
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: new TextFormField(
              decoration: new InputDecoration(
                labelText: "End Date",
                fillColor: Colors.white,
                border: new OutlineInputBorder(
                    borderSide:
                        new BorderSide(color: Theme.of(context).primaryColor)),
                suffixIcon: new Icon(Icons.date_range),
                //fillColor: Colors.green
              ),
              validator: (val) {
                if (val.length == 0) {
                  return "Date cannot be empty";
                } else {
                  return null;
                }
              },
              keyboardType: TextInputType.datetime,
              maxLines: null,
              controller: _EndDateController,
              onTap: () {
                _pickEndDate();
              },
              style: new TextStyle(
                fontFamily: "Poppins",
              ),
            ),
          ),
          Padding(
            padding: new EdgeInsets.only(
                left: 24.0, top: 12.0, right: 8.0, bottom: 4.0),
            child: new RaisedButton(
              padding: const EdgeInsets.all(8.0),
              textColor: Colors.white,
              color: Colors.blue,
              splashColor: Colors.grey,
              onPressed: _Search,
              child: new Text("Search"),
            ),
          ),
        ],
      ),
    );
  }

  _Search() {
    String startdate =
        '${pickedDate.year}-${pickedDate.month}-${pickedDate.day}';
    String enddate = '${endDate.year}-${endDate.month}-${endDate.day}';

    Future.delayed(Duration(seconds: 1)).then((value) {
      setState(() {
        Map<String, String> createDoc = new HashMap();
        createDoc['startdate'] = startdate;
        createDoc['enddate'] = enddate;
        createDoc['agentID'] = agentID;
        Navigator.pop(context, createDoc);
      });
    });
  }

  Future<List<AgentModelData>> fetchAgent(int id, String token) async {
    final response = await http.get(
      Constant.GET_AGENT_LIST + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return AgentModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  void loadSetting() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
    });

    pickedDate = DateTime.now();
    endDate = DateTime.now();
    _StartDateController.text = todayDate;
    _EndDateController.text = todayDate;

    fetchAgent(userID, accessToken).then((value) {
      setState(() {
        mList = value;
      });
    });
  }
}

class MyPageWithAudio extends StatefulWidget {
  String url;

  MyPageWithAudio(String s) {
    this.url = s;
  }

  @override
  _MyPageWithAudioState createState() => _MyPageWithAudioState(url);
}

class _MyPageWithAudioState extends State<MyPageWithAudio> {
  bool _play = true;
  String _currentPosition = "";
  String mUrl;

  _MyPageWithAudioState(String url) {
    this.mUrl = url;
  }

  @override
  Widget build(BuildContext context) {
    return AudioWidget.network(
      url: mUrl,
      play: _play,
      onReadyToPlay: (total) {
        setState(() {
          _currentPosition = "${Duration().mmSSFormat} / ${total.mmSSFormat}";
        });
      },
      onPositionChanged: (current, total) {
        setState(() {
          _currentPosition = "${current.mmSSFormat} / ${total.mmSSFormat}";
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: RaisedButton(
              shape: CircleBorder(),
              padding: EdgeInsets.all(14),
              color: Theme.of(context).primaryColor,
              child: Icon(
                _play ? Icons.pause : Icons.play_arrow,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  _play = !_play;
                });
              },
            ),
          ),
          Text(_currentPosition),
        ],
      ),
    );
  }
}
