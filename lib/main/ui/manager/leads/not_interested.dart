import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:TeleVoice/main/model/manager_lead_model.dart';
import 'package:TeleVoice/main/utils/string_duration.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/call_log_model.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:TeleVoice/main/widgets/feedback_radio.dart';
import 'package:TeleVoice/main/widgets/show_feedback.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class NotInterested extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _NotInterestedState();
  }
}

class _NotInterestedState extends State<NotInterested> {
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  bool isExpanded = false;
  bool isFeedbackAvail1 = false;

  final assetsAudioPlayer = AssetsAudioPlayer();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(child: _ListData()),
    );
  }

  FutureBuilder _ListData() {
    return FutureBuilder<List<ManagerLeadModelData>>(
      future: GetDialerData(userID, accessToken),
      builder: (BuildContext context,
          AsyncSnapshot<List<ManagerLeadModelData>> snapshot) {
        if (snapshot.hasData) {
          List<ManagerLeadModelData> data = snapshot.data;

          if (data.length == 0) {
            return Container(
                child: Text(
              'No logs available',
              style: new TextStyle(fontSize: 20.0),
            ));
          } else {
            // return _dialersData(data);
            return _listData(data);
          }
        } else if (snapshot.hasError) {
          // return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    );
  }

  ListView _listData(data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 2.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: GroovinExpansionTile(
              leading: CircleAvatar(
                child: Text((index + 1).toString()),
                backgroundColor: Theme.of(context).primaryColor,
              ),
              title: Text(
                data[index].CallerName.toString(),
                style: TextStyle(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Colors.black
                      : Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(data[index].CallerNumber),
              onExpansionChanged: (value) {
                setState(() {
                  isExpanded = value;
                });
              },
              inkwellRadius: !isExpanded
                  ? BorderRadius.all(Radius.circular(5.0))
                  : BorderRadius.only(
                      topRight: Radius.circular(5.0),
                      topLeft: Radius.circular(5.0),
                    ),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0, bottom: 4.0),
                      child: Text(
                        data[index].AgentName,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 4.0, right: 16),
                      child: Text(data[index].CallType.toString()),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 16.0, top: 4.0, bottom: 2.0),
                      child: Text(
                        DateFormat('dd/MM/yyyy HH:mm:ss').format(
                            DateTime.parse(data[index].CallStartTime)
                                .toLocal()),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 2.0, top: 4.0, right: 16),
                      child: Text(data[index].CallTalkTime.toString() + ' Sec'),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 24.0, right: 24),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.play_circle_filled,
                          color: Colors.green,
                        ),
                        onPressed: () async {
                          showRoundedModalBottomSheet(
                            color: Theme.of(context).canvasColor,
                            dismissOnTap: false,
                            context: context,
                            builder: (builder) {
                              return Container(
                                height: 280.0,
                                color: Colors.transparent,
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Scaffold(
                                    body: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8.0),
                                            child: ModalDrawerHandle(),
                                          ),
                                          Padding(
                                              padding:
                                                  const EdgeInsets.all(16.0),
                                              child: Text('Recording',
                                                  style: new TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18.0))),
                                          MyPageWithAudio(
                                              Constant.RECORDING_PATH +
                                                  data[index].RecPath),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        },
                        tooltip: "Play Audio",
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<List<ManagerLeadModelData>> GetDialerData(int id, String token) async {
    final http.Response response = await http.post(
      Constant.GET_MANAGER_LEAD,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {'id': id.toString(), 'feedback': "not interested"},
    );

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return ManagerLeadModel.fromJson(json).data;
    } else {
      throw Exception('=====>>> Failed to fetch data');
    }
  }
}

class MyPageWithAudio extends StatefulWidget {
  String url;

  MyPageWithAudio(String s) {
    this.url = s;
  }

  @override
  _MyPageWithAudioState createState() => _MyPageWithAudioState(url);
}

class _MyPageWithAudioState extends State<MyPageWithAudio> {
  bool _play = true;
  String _currentPosition = "";
  String mUrl;

  _MyPageWithAudioState(String url) {
    this.mUrl = url;
  }

  @override
  Widget build(BuildContext context) {
    return AudioWidget.network(
      url: mUrl,
      play: _play,
      onReadyToPlay: (total) {
        setState(() {
          _currentPosition = "${Duration().mmSSFormat} / ${total.mmSSFormat}";
        });
      },
      onPositionChanged: (current, total) {
        setState(() {
          _currentPosition = "${current.mmSSFormat} / ${total.mmSSFormat}";
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: RaisedButton(
              shape: CircleBorder(),
              padding: EdgeInsets.all(14),
              color: Theme.of(context).primaryColor,
              child: Icon(
                _play ? Icons.pause : Icons.play_arrow,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  _play = !_play;
                });
              },
            ),
          ),
          Text(_currentPosition),
        ],
      ),
    );
  }
}
