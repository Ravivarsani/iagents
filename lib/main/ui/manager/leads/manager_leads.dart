import 'package:flutter/material.dart';
import 'package:TeleVoice/main/ui/manager/leads/schedule.dart';
import 'interested.dart';
import 'not_interested.dart';

class ManagerLeads extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ManagerLeadsState();
  }
}

class _ManagerLeadsState extends State<ManagerLeads> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            title: Text('Lead Management'),
            bottom: TabBar(
              tabs: [
                Tab(
                  text: 'Interested',
                ),
                Tab(
                  text: 'Not Interested',
                ),
                Tab(
                  text: 'Schedule',
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Interested(),
              NotInterested(),
              Schedule(),
            ],
          )),
    );
  }
}
