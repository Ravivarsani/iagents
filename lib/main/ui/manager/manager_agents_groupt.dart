import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/activity_model.dart';
import 'package:TeleVoice/main/model/agent_model.dart';
import 'package:TeleVoice/main/model/agentgroup_model.dart';
import 'package:TeleVoice/main/model/did_config_model.dart';
import 'package:TeleVoice/main/model/manager_call_log.dart';
import 'package:TeleVoice/main/ui/manager/filter_widget.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:TeleVoice/main/widgets/show_feedback.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:http/http.dart' as http;
import 'package:TeleVoice/main/utils/string_duration.dart';

class ManagerAgentGroupPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ManagerAgentGroupPageState();
  }
}

class _ManagerAgentGroupPageState extends State<ManagerAgentGroupPage> {
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  bool isExpanded = false;
  Future<List<AgentGroupModelData>> mFuture;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');

      mFuture = fetchAgentGroup(userID, accessToken);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Agents Group'),
        ),
        body: Center(
          child: _ListData(),
        ));
  }

  FutureBuilder _ListData() {
    return FutureBuilder<List<AgentGroupModelData>>(
      future: mFuture,
      builder: (BuildContext context,
          AsyncSnapshot<List<AgentGroupModelData>> snapshot) {
        if (snapshot.hasData) {
          List<AgentGroupModelData> data = snapshot.data;

          if (data.length == 0) {
            return Container(
                child: Text(
              'No Agents found',
              style: new TextStyle(fontSize: 20.0),
            ));
          } else {
            // return _dialersData(data);
            return _listData(data);
          }
        } else if (snapshot.hasError) {
          // return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    );
  }

  ListView _listData(data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 2.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: GroovinExpansionTile(
              leading: CircleAvatar(
                child: Text((index + 1).toString()),
                backgroundColor: Theme.of(context).primaryColor,
              ),
              title: Text(
                data[index].GroupName.toString(),
                style: TextStyle(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Colors.black
                      : Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(data[index].GroupDescription),
              onExpansionChanged: (value) {
                setState(() {
                  isExpanded = value;
                });
              },
              inkwellRadius: !isExpanded
                  ? BorderRadius.all(Radius.circular(5.0))
                  : BorderRadius.only(
                      topRight: Radius.circular(5.0),
                      topLeft: Radius.circular(5.0),
                    ),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                      icon:Icon(
                        Icons.delete,
                        color: Colors.red,
                      ),
                      onPressed: () async {
                        String s = showAlertDialog(context);
                        if(s == 'dialog'){
                          setState(() {
                            mFuture = fetchAgentGroup(userID, accessToken);
                          });
                        }
                      },
                      tooltip: "Delete",
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  showAlertDialog(BuildContext context) {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        setState(() {
          Navigator.of(context, rootNavigator: true).pop('dialog');
        });
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Delete"),
      onPressed: () {
        _deleteAgent();
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Delete Agent Group"),
      content: Text("Are you sure to delete this Agent Group?"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  _deleteAgent() {
    deleteAgent(userID, accessToken).then((value) => Fluttertoast.showToast(
        msg: value.msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
        textColor: Colors.white,
        fontSize: 16.0));

    Future.delayed(Duration(seconds: 1)).then((value) {
      setState(() {
        Navigator.of(context, rootNavigator: true).pop('dialog');
      });
    });
  }

  Future<DidConfigModel> deleteAgent(int id, String token) async {
    final response = await http.get(
      Constant.DELETE_HUNT_GROUP + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return DidConfigModel.fromJson(json);
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  Future<List<AgentGroupModelData>> fetchAgentGroup(
      int id, String token) async {
    final response = await http.get(
      Constant.GET_HUNT_AGENT_GROUP + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return AgentGroupModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }
}
