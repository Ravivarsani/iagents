import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/activity_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class LoginTimeWidget extends StatefulWidget {
  @override
  _LoginTimeWidgetState createState() => _LoginTimeWidgetState();
}

class _LoginTimeWidgetState extends State<LoginTimeWidget> {
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  bool isExpanded = false;

  @override
  void initState() {
    // TODO: implement initState
    _loadSettings();
    super.initState();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(child: _ListData()),
    );
  }

  ListView _listData(data) {
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Material(
            elevation: 2.0,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0))),
            child: GroovinExpansionTile(
              leading: CircleAvatar(
                child: Text((index + 1).toString()),
                backgroundColor: Theme.of(context).primaryColor,
              ),
              title: Text(
                data[index].action.toString(),
                style: TextStyle(
                  color: Theme.of(context).brightness == Brightness.light
                      ? Colors.black
                      : Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(DateFormat('dd/MM/yyyy HH:mm:ss')
                  .format(DateTime.parse(data[index].date).toLocal())),
              onExpansionChanged: (value) {
                setState(() {
                  isExpanded = value;
                });
              },
              inkwellRadius: !isExpanded
                  ? BorderRadius.all(Radius.circular(5.0))
                  : BorderRadius.only(
                      topRight: Radius.circular(5.0),
                      topLeft: Radius.circular(5.0),
                    ),
            ),
          ),
        );
      },
    );
  }

  FutureBuilder _ListData() {
    return FutureBuilder<List<ActivityModelData>>(
      future: fetchLogin(userID, accessToken),
      builder: (BuildContext context,
          AsyncSnapshot<List<ActivityModelData>> snapshot) {
        if (snapshot.hasData) {
          List<ActivityModelData> data = snapshot.data;

          if (data.length == 0) {
            return Container(child: Text('No logs available'));
          } else {
            // return _dialersData(data);
            return _listData(data);
          }
        } else if (snapshot.hasError) {
          // return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      },
    );
  }

  Future<List<ActivityModelData>> fetchLogin(int id, String token) async {
    final response = await http.get(
      Constant.ACTIVITY_LOG + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return ActivityModel.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }
}
