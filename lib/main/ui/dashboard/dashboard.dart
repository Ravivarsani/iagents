import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/role_model.dart';
import 'package:TeleVoice/main/model/total_data_model.dart';
import 'package:TeleVoice/main/ui/drawer/side_drawer.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'dart:io';

class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DashboardState();
  }
}

class _DashboardState extends State<Dashboard> {
  SharedPreferences _preferences;
  int role;
  int userID, mTotalLiveCall, mTotalIVRCall, mTotalDIDCall, mTotalMISSEDCall;
  int mTotalTodayCall,
      mTotalIncomingCall,
      mTotalOutgoingCall,
      mTotalTodayMISSEDCall;
  String accessToken;

  List<String> titles = [
    "LIVE Calls",
    "IVR Calls",
    "DID Numbers",
    "MISSED Calls"
  ];

  List<MaterialColor> itemColor = [
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.red
  ];

  List<IconData> icon = [
    Icons.settings_phone,
    Icons.phone_forwarded,
    Icons.phone_in_talk,
    Icons.phone_missed,
  ];

  Timer _timer;
  List<int> count;
  bool isLoading = true;

  Map<String, double> dataMap = new Map();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      //Return bool
      bool islogin = _preferences.getBool('islogin' ?? false);
      if (islogin == true) {
        userID = _preferences.getInt('user_id');
        accessToken = _preferences.getString('access_token');
        role = _preferences.getInt('role');
        fetchRole(context, userID, accessToken);
      }
    });

    if (role == 2) {
      fetchTotalData(context, userID, accessToken, Constant.GET_TOTAL_LIVE_CALL)
          .then((value) {
        mTotalLiveCall = value.data.elementAt(0).total;
      });

      fetchTotalData(context, userID, accessToken, Constant.GET_TOTAL_IVR_CALL)
          .then((value) {
        mTotalIVRCall = value.data.elementAt(0).total;
      });

      fetchTotalData(
              context, userID, accessToken, Constant.GET_TOTAL_DID_NUMBER_CALL)
          .then((value) {
        mTotalDIDCall = value.data.elementAt(0).total;
      });

      fetchTotalData(
              context, userID, accessToken, Constant.GET_TOTAL_MISSED_CALL)
          .then((value) {
        mTotalMISSEDCall = value.data.elementAt(0).total;
      });

      fetchTotalData(
              context, userID, accessToken, Constant.GET_TOTAL_TODAY_CALL)
          .then((value) {
        mTotalTodayCall = value.data.elementAt(0).total;
      });

      fetchTotalData(context, userID, accessToken,
              Constant.GET_TOTAL_TODAY_INCOMING_CALL)
          .then((value) {
        mTotalIncomingCall = value.data.elementAt(0).total;
      });

      fetchTotalData(context, userID, accessToken,
              Constant.GET_TOTAL_TODAY_OUTGOING_CALL)
          .then((value) {
        mTotalOutgoingCall = value.data.elementAt(0).total;
      });

      fetchTotalData(context, userID, accessToken,
              Constant.GET_TOTAL_TODAY_MISSED_CALL)
          .then((value) {
        mTotalTodayMISSEDCall = value.data.elementAt(0).total;
      });

      _timer = Timer.periodic(Duration(seconds: 1), (_) {
        count = [
          mTotalLiveCall,
          mTotalIVRCall,
          mTotalDIDCall,
          mTotalMISSEDCall,
        ];

        dataMap.putIfAbsent(
            "Today's All Calls", () => mTotalTodayCall.roundToDouble());
        dataMap.putIfAbsent(
            "Incoming calls", () => mTotalIncomingCall.roundToDouble());
        dataMap.putIfAbsent(
            "Outgoing calls", () => mTotalOutgoingCall.roundToDouble());
        dataMap.putIfAbsent(
            "Missed calls", () => mTotalTodayMISSEDCall.roundToDouble());

        setState(() {
          isLoading = false;
        });
      });
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    if (_timer != null) {
      _timer.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: role == 2
          ? AppBar(
              title: Text("Dashboard"),
              elevation: 4,
              actions: <Widget>[
                Padding(
                    padding: EdgeInsets.only(right: 20.0),
                    child: GestureDetector(
                      onTap: () => Navigator.of(context).pushNamed("/support"),
                      child: Icon(
                        Icons.support_agent,
                        size: 26.0,
                      ),
                    )),
              ],
              backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
            )
          : AppBar(
              title: Text("Dashboard"),
              elevation: 4,
              backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
            ),
      drawer: SideDrawer(role),
      body: Container(
        padding: EdgeInsets.all(8.0),
        child: role == 2 ? _ManagerDash() : makeGrid(),
      ),
    );
  }

  Widget makeDashList() {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 4,
                itemBuilder: (context, i) {
                  return makeManagerDashboardItem(context, titles[i].toString(),
                      icon[i], count[i], itemColor[i]);
                }),
            PieChart(
              dataMap: dataMap,
              animationDuration: Duration(milliseconds: 800),
              showChartValuesInPercentage: false,
              showChartValues: true,
              colorList: [
                Colors.blue,
                Colors.green,
                Colors.orange,
                Colors.red,
              ],
            ),
          ],
        ),
      ),
    );
  }

  GridView makeGrid() {
    return GridView.count(
      crossAxisCount: 2,
      padding: EdgeInsets.all(3.0),
      children: <Widget>[
        makeAgentDashboardItem(context, "Dialer", Icons.dialpad),
        makeAgentDashboardItem(context, "Assigned Lead", Icons.assignment_ind),
        makeAgentDashboardItem(context, "Call Logs", Icons.call)
      ],
    );
  }

  Card makeAgentDashboardItem(
      BuildContext context1, String title, IconData icon) {
    return Card(
        clipBehavior: Clip.hardEdge,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        elevation: 6.0,
        margin: new EdgeInsets.all(8.0),
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(220, 220, 220, 1.0)),
          child: new InkWell(
            onTap: () {
              if (title.compareTo("Call Logs") == 0) {
                Navigator.of(context1).pushNamed("/calllogs");
              } else if (title.compareTo("Assigned Lead") == 0) {
                Navigator.of(context1).pushNamed("/assignedlead");
              } else {
                Navigator.of(context1).pushNamed("/phonedialer");
              }
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              verticalDirection: VerticalDirection.down,
              children: <Widget>[
                SizedBox(height: 50.0),
                Center(
                  child: Icon(
                    icon,
                    size: 40.0,
                    color: Color.fromRGBO(49, 87, 110, 1.0),
                  ),
                ),
                SizedBox(height: 20.0),
                new Center(
                  child: new Text(title,
                      style:
                          new TextStyle(fontSize: 18.0, color: Colors.black)),
                )
              ],
            ),
          ),
        ));
  }

  Card makeManagerDashboardItem(BuildContext context1, String title,
      IconData icon, int count, MaterialColor clr) {
    return Card(
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      elevation: 2.0,
      margin: new EdgeInsets.all(4.0),
      child: ClipPath(
        clipper: ShapeBorderClipper(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15))),
        child: Container(
            decoration: BoxDecoration(
              border: Border(left: BorderSide(color: clr, width: 10)),
              color: Colors.white,
            ),
            padding: EdgeInsets.all(16.0),
            alignment: Alignment.centerLeft,
            child: Container(
              child: new InkWell(
                onTap: () {
                  if (title.compareTo("LIVE Calls") == 0) {
                    Navigator.of(context).pushNamed("/livecall");
                  } else if (title.compareTo("DID Numbers") == 0) {
                    Navigator.of(context).pushNamed("/managerdid");
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      verticalDirection: VerticalDirection.down,
                      children: <Widget>[
                        SizedBox(height: 10.0),
                        Text(title,
                            style: new TextStyle(fontSize: 16.0, color: clr)),
                        SizedBox(height: 10.0),
                        Text(count.toString(),
                            style: new TextStyle(
                                fontSize: 17.0, color: Colors.black)),
                      ],
                    ),
                    Icon(
                      icon,
                      size: 32.0,
                      color: Colors.black45,
                    ),
                  ],
                ),
              ),
            )),
      ),
    );
  }

  Future<RoleModel> fetchRole(BuildContext cnxt, int id, String token) async {
    final response = await http.get(
      Constant.API_URL + Constant.GET_ROLE + "/" + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,

    } else if (response.statusCode == 401) {
      _preferences.clear();

      Fluttertoast.showToast(
          msg: json.decode(response.body)['message'].toString() +
              " Please re-login",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
          textColor: Colors.white,
          fontSize: 17.0);

      Timer timer = new Timer(const Duration(seconds: 1), () {
        // Navigator.popAndPushNamed(context, '/login');
        Navigator.pushNamedAndRemoveUntil(
            context, "/login", (Route<dynamic> route) => false);
      });
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  Future<TotalModel> fetchTotalData(
      BuildContext cnxt, int id, String token, String apiurl) async {
    final response = await http.get(
      apiurl + "/" + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      return TotalModel.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  _ManagerDash() {
    return isLoading
        ? new Scaffold(
            body: new Center(
              child: new CircularProgressIndicator(),
            ),
          )
        : makeDashList();
  }
}
