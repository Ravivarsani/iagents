import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:TeleVoice/main/model/agent_details.dart';
import 'package:TeleVoice/main/model/agent_model.dart';
import 'package:TeleVoice/main/model/caller_number.dart';
import 'package:TeleVoice/main/model/contact_segmnet.dart';
import 'package:TeleVoice/main/model/datamodel.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/widgets/dropdown_search.dart';
import 'package:TeleVoice/main/widgets/progress_dialog.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/scheduler.dart' show timeDilation;

class CreateContactPage extends StatefulWidget {
  String callNumber;
  int callID;

  CreateContactPage(String callerNumber, int id) {
    this.callNumber = callerNumber;
    this.callID = id;
  }

  @override
  createState() => new CreateContactPageState();
}

class CreateContactPageState extends State<CreateContactPage> {
  static final globalKey = new GlobalKey<ScaffoldState>();

  ProgressDialog progressDialog =
      ProgressDialog.getProgressDialog('Saving Contact', false);

  TextEditingController nameController = new TextEditingController(text: "");

  TextEditingController phoneController = new TextEditingController(text: "");

  TextEditingController emailController = new TextEditingController(text: "");

  TextEditingController addressController = new TextEditingController(text: "");

  TextEditingController cityController = new TextEditingController(text: "");

  TextEditingController stateController = new TextEditingController(text: "");

  Widget createContactWidget = new Container();

  int userID, managerid;
  String accessToken, contId = '';
  List<Data> dataSegment;
  String managerID;
  SharedPreferences _preferences;

  @override
  void initState() {
    super.initState();

    loadSetting();
  }

  void loadSetting() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');

      phoneController.text = widget.callNumber;

      fetchAgentDetails(userID, accessToken).then((value) {
        setState(() {
          managerID = value.createdBy;

          fetchContactSegment(managerID, accessToken).then((value) {
            setState(() {
              dataSegment = value;
            });
          });

        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 1.0;
    createContactWidget = ListView(
      children: <Widget>[
        new Center(
          child: new Container(
            margin: EdgeInsets.only(left: 30.0, right: 30.0, top: 30.0),
            child: new Column(
              children: <Widget>[
                _formContainer(),
              ],
            ),
          ),
        )
      ],
    );
    return new Scaffold(
      key: globalKey,
      appBar: new AppBar(
        centerTitle: true,
        leading: new GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: new Icon(
            Icons.arrow_back,
            size: 30.0,
          ),
        ),
        textTheme: new TextTheme(
            title: new TextStyle(
          color: Colors.white,
          fontSize: 22.0,
        )),
        iconTheme: new IconThemeData(color: Colors.white),
        title: new Text('Create Contact'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () {
              _validateCreateContactForm();
            },
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: new Icon(
                Icons.done,
                size: 30.0,
              ),
            ),
          )
        ],
      ),
      body: new Stack(
        children: <Widget>[createContactWidget, progressDialog],
      ),
      backgroundColor: Colors.grey[150],
    );
  }

  Widget _formContainer() {
    return new Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 20.0),
      child: new Form(
          child: new Theme(
              data: new ThemeData(primarySwatch: Colors.blue),
              child: new Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  _formField(
                      nameController, Icons.face, 'Name', TextInputType.text),
                  _formField(phoneController, Icons.phone, 'Mobile',
                      TextInputType.phone),
                  _formField(emailController, Icons.email, 'Email',
                      TextInputType.emailAddress),
                  _formField(addressController, Icons.location_on, 'Address',
                      TextInputType.text),
                  _formField(cityController, Icons.location_on, 'City',
                      TextInputType.text),
                  _formField(stateController, Icons.location_on, 'State',
                      TextInputType.text),
                  SizedBox(
                    height: 10.0,
                  ),
                  DropdownSearch<Data>(
                    mode: Mode.MENU,
                    showSelectedItem: false,
                    items: dataSegment,
                    itemAsString: (Data u) => u.name,
                    label: "Segment",
                    hint: "Select Segment",
                    onChanged: (Data data) => contId = data.contId.toString(),
                  ),
                ],
              ))),
    );
  }

  Widget _formField(TextEditingController textEditingController, IconData icon,
      String text, TextInputType textInputType) {
    return new Container(
        child: new TextFormField(
          controller: textEditingController,
          decoration: InputDecoration(
              suffixIcon: new Icon(
                icon,
                color: Colors.blue[400],
              ),
              labelText: text,
              labelStyle: TextStyle(fontSize: 18.0)),
          keyboardType: textInputType,
        ),
        margin: EdgeInsets.only(bottom: 10.0));
  }

  void _validateCreateContactForm() {
    String name = nameController.text;
    if (name.length < 1) {
      showSnackBar('Please enter name');
      return;
    } else if (name.length < 2) {
      showSnackBar('Please enter valid name');
      return;
    }

    String phone = phoneController.text;

    if (phone.length < 1) {
      showSnackBar('Please enter mobile');
      return;
    } else if (phone.length < 10 || phone.length > 12) {
      showSnackBar('Please enter valid mobile no');
      return;
    }

    String email = emailController.text;
    if (!isValidEmail(email)) {
      showSnackBar('Please enter valid email address');
      return;
    }

    String address = addressController.text;
    if (address.length < 3 || address.length > 1000) {
      showSnackBar('Please fill address within range of 4 to 1000 Characters');
      return;
    }

    String city = cityController.text;

    if (city.length < 1) {
      showSnackBar('Please enter City');
      return;
    } else if (city.length < 2) {
      showSnackBar('Please enter valid city');
      return;
    }

    String state = stateController.text;
    if (state.length < 1) {
      showSnackBar('Please enter State');
      return;
    } else if (state.length < 2) {
      showSnackBar('Please enter valid state');
      return;
    }

    progressDialog.show();

    updateCallLog(widget.callID, accessToken, name).then((value) => null);

    createContact(userID, accessToken, name, phone, email, address, city, state,
            contId)
        .then((value) {
      String dataSegment = value.msg;
      showSnackBar(dataSegment);

      progressDialog.hide();

      Timer timer = new Timer(const Duration(seconds: 2), () {
        Navigator.pop(context);
      });

    });
  }

  bool isValidEmail(String email) {
    return isValid(email, RegularExpressionsPatterns.EMAIL_VALIDATION);
  }

  bool isValid(String thingNeedToBeValidated, String validationPattern) {
    return new RegExp(validationPattern).hasMatch(thingNeedToBeValidated);
  }

  void showSnackBar(String textToBeShown) {
    globalKey.currentState.showSnackBar(new SnackBar(
      content: new Text(textToBeShown),
    ));
  }

  Future<List<Data>> fetchContactSegment(String id, String token) async {
    final response = await http.get(
      Constant.GET_CATEGORY_LIST + id,
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return contact_segmnet.fromJson(json).data;
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load data ' + response.body.toString());
    }
  }

  Future<DataAgent> fetchAgentDetails(int id, String token) async {
    final response = await http.get(
      Constant.GET_AGNET_DETAIL + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return AgentDetail.fromJson(json).data;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.

      throw Exception(
          '============>> Failed to load data ' + response.body.toString());
    }
  }

  Future<uuid_model> createContact(
    int id,
    String token,
    String name,
    String mobile,
    String email,
    String address,
    String city,
    String state,
    String category,
  ) async {
    final http.Response response = await http.post(
      Constant.CREATE_CONTACT,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'customer_id': id.toString(),
        'name': name,
        'mobile': mobile,
        'email': email,
        'address': address,
        'city': city,
        'state': state,
        'category': category,
        'is_mobile_dnd': 'no',
      },
    );

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      print(json);
      return uuid_model.fromJson(json);
    }
  }

  Future<Data_model> updateCallLog(
    int id,
    String token,
    String name,
  ) async {
    final http.Response response = await http.post(
      Constant.UPDATE_CALL_LOG,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'id': id.toString(),
        'CallerName': name,
      },
    );

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      print(json);
      return Data_model.fromJson(json);
    }
  }
}
