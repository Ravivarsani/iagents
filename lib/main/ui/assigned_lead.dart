import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:TeleVoice/main/model/assigned_lead.dart';
import 'package:TeleVoice/main/utils/string_duration.dart';
import 'package:TeleVoice/src/sip_ua_helper.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:logger/logger.dart';
import 'package:ringtone/ringtone.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class AssignedLead extends StatefulWidget {
  final SIPUAHelper _helper;

  AssignedLead(this._helper, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AssignedLeadState();
  }
}

class _AssignedLeadState extends State<AssignedLead>
    implements SipUaHelperListener {
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  bool isExpanded = false;
  bool isFeedbackAvail1 = false;
  int page_number = 0;
  bool loading = true;
  bool isLoading = false;
  List<AssignedleadModelData> dataList;
  final assetsAudioPlayer = AssetsAudioPlayer();
  bool isEnabled = true;

  SIPUAHelper get helper => widget._helper;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    helper.addSipUaHelperListener(this);
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
      GetDialerData(userID, accessToken);
    });
  }

  @override
  deactivate() {
    super.deactivate();
    helper.removeSipUaHelperListener(this);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Assigned Lead"),
      ),
      body: loading
          ? Center(
              child: new CircularProgressIndicator(),
            )
          : _buildPaginatedListView(),
    );
  }

  Widget _buildPaginatedListView() {
    return Column(
      children: <Widget>[
        Expanded(
            child: NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollInfo) {
            if (!isLoading &&
                scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {
              this.GetDialerData(userID, accessToken);
              // start loading data
              setState(() {
                isLoading = true;
              });
            }
          },
          child: _listData(dataList),
        )),
        Container(
          height: isLoading ? 70.0 : 0,
          child: Center(
            child: new CircularProgressIndicator(),
          ),
        ),
      ],
    );
  }

  void _handleCall(String dest, [bool voiceonly = false]) {
    if (dest != null || dest.isNotEmpty) {
      helper.call(dest, voiceonly);
      _preferences.setString('dest', dest);
      return null;
    }
    return null;
  }

  Widget _listData(data) {
    if (data.length == 0) {
      return Container(
          child: Text(
        'No assign leads available',
        style: new TextStyle(fontSize: 20.0),
      ));
    } else {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Material(
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0))),
              child: GroovinExpansionTile(
                leading: CircleAvatar(
                  child: Text((index + 1).toString()),
                  backgroundColor: Theme.of(context).primaryColor,
                ),
                title: Text(
                  data[index].name.toString(),
                  style: TextStyle(
                    color: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(data[index].mobile),
                onExpansionChanged: (value) {
                  setState(() {
                    isExpanded = value;
                  });
                },
                inkwellRadius: !isExpanded
                    ? BorderRadius.all(Radius.circular(5.0))
                    : BorderRadius.only(
                        topRight: Radius.circular(5.0),
                        topLeft: Radius.circular(5.0),
                      ),
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 24.0, right: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        IconButton(
                            icon: Icon(Icons.phone, color: Colors.green),
                            tooltip: "Call",
                            onPressed: isEnabled
                                ? () async {
                                    Future<uuid_model> _futureSetCall =
                                        setCallRequest(userID,
                                            data[index].numid, accessToken);

                                    _futureSetCall.then((value) {
                                      Scaffold.of(context)
                                          .showSnackBar(SnackBar(
                                        content: Text(value.msg),
                                      ));

                                      if (value.msg ==
                                          'Call originate succesfully.') {
                                        setState(() {
                                          isEnabled = true;
                                        });

                                        Timer timer = new Timer(
                                            const Duration(seconds: 1), () {
                                          Navigator.pushNamedAndRemoveUntil(
                                              context,
                                              "/currentcall",
                                              (Route<dynamic> route) => true);
                                        });
                                      } else {
                                        setState(() {
                                          isEnabled = true;
                                        });
                                      }
                                    });
                                  }
                                : null),
                        IconButton(
                            icon: Icon(Icons.dialer_sip_outlined),
                            tooltip: "Call",
                            onPressed: isEnabled
                                ? () async {
                                    _handleCall(data[index].callerNumber, true);
                                  }
                                : null),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<uuid_model> setCallRequest(int id, int numberid, String token) async {
    setState(() {
      isEnabled = false;
    });

    final http.Response response = await http.post(
      Constant.GET_AGENT_LEAD_CALL,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'agentid': id.toString(),
        'numid': numberid.toString(),
      },
    );
    Logger().d("=======>>> response " + response.body.toString());
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return uuid_model.fromJson(json);
    } else {
      setState(() {
        isEnabled = true;
      });
      throw Exception(
          '=====>> Failed to post uuid. ' + response.body.toString());
    }
  }

  Future<List<AssignedleadModelData>> GetDialerData(
      int id, String token) async {
    final http.Response response = await http.post(
      Constant.GET_AGENT_ASSIGN_LEAD,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'id': id.toString(),
        'pageNumber': page_number.toString(),
        'pageSize': "10",
      },
    );

    setState(() {
      isLoading = false;
      loading = false;

      Logger().d("=======>>> Dialer " + response.body.toString());

      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        List<AssignedleadModelData> newItems =
            AssignedleadModel.fromJson(json).data;
        if (dataList == null) {
          dataList = newItems;
        } else {
          dataList.addAll(newItems);
        }
        page_number = page_number + 1;
        return AssignedleadModel.fromJson(json).data;
      } else {
        throw Exception('=====>>> Failed to fetch data');
      }
    });
  }

  @override
  void callStateChanged(Call call, CallState state) {
    // TODO: implement callStateChanged
    if (state.state == CallStateEnum.CALL_INITIATION) {
      if (call.direction == 'INCOMING') {
        Ringtone.play();
      }

      Navigator.pushNamed(context, '/callscreen', arguments: call);
    }
  }

  @override
  void onNewMessage(SIPMessageRequest msg) {
    // TODO: implement onNewMessage
  }

  @override
  void registrationStateChanged(RegistrationState state) {
    // TODO: implement registrationStateChanged
  }

  @override
  void transportStateChanged(TransportState state) {
    // TODO: implement transportStateChanged
  }
}

class MyPageWithAudio extends StatefulWidget {
  String url;

  MyPageWithAudio(String s) {
    this.url = s;
  }

  @override
  _MyPageWithAudioState createState() => _MyPageWithAudioState(url);
}

class _MyPageWithAudioState extends State<MyPageWithAudio> {
  bool _play = true;
  String _currentPosition = "";
  String mUrl;

  _MyPageWithAudioState(String url) {
    this.mUrl = url;
  }

  @override
  Widget build(BuildContext context) {
    return AudioWidget.network(
      url: mUrl,
      play: _play,
      onReadyToPlay: (total) {
        setState(() {
          _currentPosition = "${Duration().mmSSFormat} / ${total.mmSSFormat}";
        });
      },
      onPositionChanged: (current, total) {
        setState(() {
          _currentPosition = "${current.mmSSFormat} / ${total.mmSSFormat}";
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: RaisedButton(
              shape: CircleBorder(),
              padding: EdgeInsets.all(14),
              color: Theme.of(context).primaryColor,
              child: Icon(
                _play ? Icons.pause : Icons.play_arrow,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  _play = !_play;
                });
              },
            ),
          ),
          Text(_currentPosition),
        ],
      ),
    );
  }
}
