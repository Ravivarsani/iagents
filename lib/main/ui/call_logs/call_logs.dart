import 'package:TeleVoice/src/sip_ua_helper.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/ui/tabs/dailer.dart';
import 'package:TeleVoice/main/ui/tabs/incoming.dart';
import 'package:TeleVoice/main/ui/tabs/outgoing.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ringtone/ringtone.dart';

class CallLogs extends StatefulWidget {
  final SIPUAHelper _helper;

  CallLogs(this._helper, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CallLogsState();
  }
}

class _CallLogsState extends State<CallLogs> implements SipUaHelperListener {
  SIPUAHelper get helper => widget._helper;

  @override
  void initState() {
    // TODO: implement initState
    helper.addSipUaHelperListener(this);
    super.initState();
  }


  @override
  deactivate() {
    super.deactivate();
    helper.removeSipUaHelperListener(this);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
            title: Text('Call Logs'),
            bottom: TabBar(
              tabs: [
                Tab(
                  text: 'Dialer',
                ),
                Tab(
                  text: 'Incoming',
                ),
                Tab(
                  text: 'Outgoing',
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Dialer(helper),
              Incoming(helper),
              Outgoing(helper),
            ],
          )),
    );
  }

  @override
  void callStateChanged(Call call, CallState state) {
    // TODO: implement callStateChanged
    /*if (state.state == CallStateEnum.CALL_INITIATION) {
      if (call.direction == 'INCOMING') {
        Ringtone.play();
      }

      Fluttertoast.showToast(
          msg: 'in Call Logs',
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
          textColor: Colors.white,
          fontSize: 17.0);

      Navigator.pushNamed(context, '/callscreen', arguments: call);
    }*/
  }

  @override
  void onNewMessage(SIPMessageRequest msg) {
    // TODO: implement onNewMessage
  }

  @override
  void registrationStateChanged(RegistrationState state) {
    // TODO: implement registrationStateChanged
  }

  @override
  void transportStateChanged(TransportState state) {
    // TODO: implement transportStateChanged
  }
}
