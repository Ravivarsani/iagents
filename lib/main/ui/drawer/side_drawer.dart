import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/breaktime_model.dart';
import 'package:TeleVoice/main/model/did_config_model.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:logger/logger.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SideDrawer extends StatefulWidget {
  int mRole;

  SideDrawer(int role) {
    this.mRole = role;
  }

  @override
  State<StatefulWidget> createState() {
    return _SideDrawerState(mRole);
  }
}

class _SideDrawerState extends State<SideDrawer> {
  SharedPreferences _preferences;
  String mUsername, accessToken;
  int userID;
  int role;
  bool _value = false;
  int breakid;

  _SideDrawerState(int mRole) {
    this.role = mRole;
  }

  @override
  void initState() {
    super.initState();
    _loadSettings();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();
    this.setState(() {
      mUsername = _preferences.getString('auth_user');
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
      Logger().d("=======>> " + accessToken);
    });

    getBreakTimeStatus(userID, accessToken).then((value) {
      setState(() {
        if (value.data != null) {
          _value = true;
          breakid = value.data.breakId;
        } else {
          _value = false;
        }
      });
    });
  }

  void _logOut(BuildContext context1) async {
    postLogout(userID, accessToken).then((value) {
      if (value == 'Activity log saved successfully') {
        _preferences.clear();
        setState(() {
          Navigator.pushNamedAndRemoveUntil(
              context1, "/login", (Route<dynamic> route) => false);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        child: Drawer(
          child: _buildList(),
        ),
      ),
    );
  }

  ListView _buildList() {
    if (role == 2) {
      return ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(mUsername),
            currentAccountPicture: CircleAvatar(
              child: Image.asset(
                'images/logo.png',
                height: 50.0,
                width: 50.0,
              ),
              backgroundColor: Colors.white,
            ),
            accountEmail: null,
          ),
          ListTile(
              title: Text('Home'),
              leading: Icon(Icons.home),
              onTap: () => Navigator.of(context).pushNamed("/home")),
          ListTile(
              title: Text('Lead Management'),
              leading: Icon(Icons.data_usage),
              onTap: () => Navigator.of(context).pushNamed("/managerleads")),
          ListTile(
              title: Text('Agents'),
              leading: Icon(Icons.group),
              onTap: () =>
                  Navigator.of(context).pushNamed("/manageragentslist")),
          ListTile(
              title: Text('Agents Group'),
              leading: Icon(Icons.group),
              onTap: () =>
                  Navigator.of(context).pushNamed("/manageragentsgroup")),
          ListTile(
              title: Text('Live Call'),
              leading: Icon(Icons.call),
              onTap: () => Navigator.of(context).pushNamed("/livecall")),
          ListTile(
              title: Text('Contacts'),
              leading: Icon(Icons.contacts),
              onTap: () => Navigator.of(context).pushNamed("/contacts")),
          ListTile(
              title: Text('Calls Log'),
              leading: Icon(Icons.history),
              onTap: () => Navigator.of(context).pushNamed("/managercalllogs")),
          ListTile(
              title: Text('Activity Log'),
              leading: Icon(Icons.timer),
              onTap: () => Navigator.of(context).pushNamed("/loginstate")),
          ListTile(
              title: Text('About'),
              leading: Icon(Icons.info),
              onTap: () => Navigator.of(context).pushNamed("/about")),
          /*  ListTile(
              title: Text('Support'),
              leading: Icon(Icons.support_agent),
              onTap: () => Navigator.of(context).pushNamed("/support")),*/
          ListTile(
              title: Text('Logout'),
              leading: Icon(Icons.exit_to_app),
              onTap: () => _logOut(context)),
        ],
      );
    } else {
      return ListView(
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(mUsername),
            currentAccountPicture: CircleAvatar(
              child: Image.asset(
                'images/logo.png',
                height: 50.0,
                width: 50.0,
              ),
              backgroundColor: Colors.white,
            ),
            accountEmail: null,
          ),
          ListTile(
              title: Text('Home'),
              leading: Icon(Icons.home),
              onTap: () => Navigator.of(context).pushNamed("/home")),
          ListTile(
              title: Text('Calls Log'),
              leading: Icon(Icons.history),
              onTap: () => Navigator.of(context).pushNamed("/calllogs")),
          /* ListTile(
                  title: Text('Lead Management'),
                  leading: Icon(Icons.data_usage),
                  onTap: () => Navigator.of(context).pushNamed("/leadmanage")),*/
          ListTile(
              title: Text('Contacts'),
              leading: Icon(Icons.contacts),
              onTap: () => Navigator.of(context).pushNamed("/contacts")),
          // ListTile(
          //     title: Text('SIP Dialer'),
          //     leading: Icon(Icons.dialer_sip),
          //     onTap: () => Navigator.of(context).pushNamed("/sipdailer")),
          ListTile(
              title: Text('Activity Log'),
              leading: Icon(Icons.timer),
              onTap: () => Navigator.of(context).pushNamed("/loginstate")),
          SwitchListTile(
              title: Text('Break Time'),
              secondary: Icon(Icons.free_breakfast),
              value: _value,
              onChanged: (bool value) {
                _onChanged(value);
              }),
          ListTile(
              title: Text('About'),
              leading: Icon(Icons.info),
              onTap: () => Navigator.of(context).pushNamed("/about")),
          ListTile(
              title: Text('Logout'),
              leading: Icon(Icons.exit_to_app),
              onTap: () => _logOut(context)),
        ],
      );
    }
  }

  Future<String> postLogout(int id, String token) async {
    final response = await http.get(
      Constant.POST_LOGOUT + "/" + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return json['data'].toString();
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  void _onChanged(bool value1) {
    if (value1) {
      startBreakTime(userID, accessToken).then((value) {
        Fluttertoast.showToast(
            msg: value.msg,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
            textColor: Colors.white,
            fontSize: 16.0);

        if (value.msg == 'Your Break Time start successfully') {
          setState(() {
            _value = value1;
          });
        }
      });
    } else if (!value1) {
      stopBreakTime(userID, breakid, accessToken).then((value) {
        Fluttertoast.showToast(
            msg: value.msg,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
            textColor: Colors.white,
            fontSize: 16.0);

        if (value.msg == 'Your Break Time stop successfully') {
          setState(() {
            _value = value1;
          });
        }
      });
    }
  }

  Future<DidConfigModel> startBreakTime(int id, String token) async {
    final response = await http.get(
      Constant.START_AGENT_BREAK_TIME + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return DidConfigModel.fromJson(json);
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  Future<DidConfigModel> stopBreakTime(
      int id, int break_id, String token) async {
    final http.Response response = await http.post(
      Constant.STOP_AGENT_BREAK_TIME,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'user_id': id.toString(),
        'breakid': break_id.toString(),
      },
    );
    Logger().d("=======>>> response " + response.body.toString());
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return DidConfigModel.fromJson(json);
    } else {
      throw Exception(
          '=====>> Failed to post uuid. ' + response.body.toString());
    }
  }

  Future<BreakTimeModel> getBreakTimeStatus(int id, String token) async {
    final response = await http.get(
      Constant.GET_AGENT_BREAK_TIME_STATUS + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      var json = jsonDecode(response.body);
      Logger().d("=========>> json " + json.toString());
      return BreakTimeModel.fromJson(json);
    } else if (response.statusCode == 401) {
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }
}
