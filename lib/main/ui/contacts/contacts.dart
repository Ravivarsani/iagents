import 'package:TeleVoice/src/sip_ua_helper.dart';
import 'package:flutter/material.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';
import 'package:path/path.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';

class ContactsPage extends StatefulWidget {
  final SIPUAHelper _helper;

  ContactsPage(this._helper, {Key key}) : super(key: key);

  @override
  _ContactsPageState createState() => _ContactsPageState();
}

class _ContactsPageState extends State<ContactsPage>
    implements SipUaHelperListener {
  Iterable<Contact> _contacts;
  bool isExpanded = false;
  SharedPreferences _preferences;
  int userID;
  String accessToken;
  List<Contact> contactsFiltered = [];
  TextEditingController searchController = new TextEditingController();

  SIPUAHelper get helper => widget._helper;

  @override
  void initState() {
    _getPermission();
    _loadSettings();
    super.initState();
  }

  void _loadSettings() async {
    helper.addSipUaHelperListener(this);

    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
    });
  }

  //Check contacts permission
  Future<PermissionStatus> _getPermission() async {
    final PermissionStatus permission = await Permission.contacts.status;
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.denied) {
      final Map<Permission, PermissionStatus> permissionStatus =
          await [Permission.contacts].request();
      if (permissionStatus[Permission.contacts] == PermissionStatus.granted) {
        getContacts();
        searchController.addListener(() {
          filterContacts();
        });
      }
    } else {
      getContacts();
      searchController.addListener(() {
        filterContacts();
      });
    }
  }

  @override
  deactivate() {
    super.deactivate();
    helper.removeSipUaHelperListener(this);
  }

  Future<void> getContacts() async {
    //We already have permissions for contact when we get to this page, so we
    // are now just retrieving it
    Iterable<Contact> contacts = await ContactsService.getContacts(
        withThumbnails: false, photoHighResolution: false);
    setState(() {
      _contacts = contacts;
    });
  }

  String flattenPhoneNumber(String phoneStr) {
    return phoneStr.replaceAllMapped(RegExp(r'^(\+91)|\D'), (Match m) {
      return m[0] == "+" ? "+" : "";
    });
  }

  filterContacts() {
    List<Contact> _contact = [];
    _contact.addAll(_contacts);
    if (searchController.text.isNotEmpty) {
      _contact.retainWhere((contact) {
        String searchTerm = searchController.text.toLowerCase();
        String searchTermFlatten = flattenPhoneNumber(searchTerm);
        String contactName = contact.displayName.toString().toLowerCase() ?? '';
        bool nameMatches = contactName.contains(searchTerm);
        if (nameMatches == true) {
          return true;
        }

        if (searchTermFlatten.isEmpty) {
          return false;
        }

        var phone = contact.phones.firstWhere((phn) {
          String phnFlattened = flattenPhoneNumber(phn.value);
          return phnFlattened.contains(searchTermFlatten);
        }, orElse: () => null);

        return phone != null;
      });
    }
    setState(() {
      contactsFiltered = _contact;
    });
  }

  @override
  Widget build(BuildContext context) {
    bool isSearching = searchController.text.isNotEmpty;

    return Scaffold(
        appBar: AppBar(
          title: (Text('Contacts')),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                child: TextField(
                  controller: searchController,
                  decoration: InputDecoration(
                      labelText: 'Search',
                      border: new OutlineInputBorder(
                          borderSide: new BorderSide(
                              color: Theme.of(context).primaryColor)),
                      prefixIcon: Icon(Icons.search,
                          color: Theme.of(context).primaryColor)),
                ),
              ),
              Expanded(
                child: _contacts != null
                    ? ListView.builder(
                        shrinkWrap: true,
                        itemCount: isSearching == true
                            ? contactsFiltered.length
                            : _contacts?.length ?? 0,
                        itemBuilder: (BuildContext context, int index) {
                          Contact contact = isSearching == true
                              ? contactsFiltered[index]
                              : _contacts?.elementAt(index);

                          String mNo = '', mNo1 = '';

                          if (contact.phones.length == 1) {
                            mNo = contact.phones.elementAt(0).value;
                          } else if (contact.phones.length == 2) {
                            mNo = contact.phones.elementAt(0).value;
                            mNo = contact.phones.elementAt(1).value;
                          } else if (contact.phones.length == 3) {
                            mNo = contact.phones.elementAt(2).value;
                          }

                          print('==================>>>');
                          print(mNo);

                          String get_Mo = flattenPhoneNumber(mNo);

                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Material(
                              elevation: 2.0,
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5.0))),
                              child: GroovinExpansionTile(
                                leading: (contact.avatar != null &&
                                        contact.avatar.isNotEmpty)
                                    ? CircleAvatar(
                                        backgroundImage:
                                            MemoryImage(contact.avatar),
                                      )
                                    : CircleAvatar(
                                        child: Text(contact.initials()),
                                        backgroundColor:
                                            Theme.of(context).primaryColor,
                                      ),
                                title: Text(
                                  contact.displayName ?? '',
                                  style: TextStyle(
                                    color: Theme.of(context).brightness ==
                                            Brightness.light
                                        ? Colors.black
                                        : Colors.white,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                subtitle: Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Text(get_Mo ?? ''),
                                ),
                                onExpansionChanged: (value) {
                                  setState(() {
                                    isExpanded = value;
                                  });
                                },
                                inkwellRadius: !isExpanded
                                    ? BorderRadius.all(Radius.circular(5.0))
                                    : BorderRadius.only(
                                        topRight: Radius.circular(5.0),
                                        topLeft: Radius.circular(5.0),
                                      ),
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      IconButton(
                                        icon: Icon(Icons.phone,
                                            color: Colors.green),
                                        onPressed: () async {
                                          Future<uuid_model> _futureSetCall =
                                              setCallRequest(
                                                  userID, mNo, accessToken);
                                          _futureSetCall.then((value) {
                                            Scaffold.of(context)
                                                .showSnackBar(SnackBar(
                                              content: Text(value.msg),
                                            ));

                                            if (value.msg ==
                                                'Call originate succesfully.') {
                                              Timer timer = new Timer(
                                                  const Duration(seconds: 2),
                                                  () {
                                                Navigator
                                                    .pushNamedAndRemoveUntil(
                                                        context,
                                                        "/currentcall",
                                                        (Route<dynamic>
                                                                route) =>
                                                            true);
                                              });
                                            }
                                          });
                                        },
                                        tooltip: "Call",
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      )
                    : Center(child: const CircularProgressIndicator()),
              )
            ],
          ),
        ));
  }

  Future<uuid_model> setCallRequest(int id, String number, String token) async {
    final http.Response response = await http.post(
      Constant.POST_AGENT_OUTGOING_CALL,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'agentid': id.toString(),
        'mobile': number,
      },
    );
    Logger().d("=======>>> response " + response.body.toString());
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return uuid_model.fromJson(json);
    } else {
      throw Exception(
          '=====>> Failed to post uuid. ' + response.body.toString());
    }
  }

  @override
  void callStateChanged(Call call, CallState state) {
    // TODO: implement callStateChanged
    /*if (state.state == CallStateEnum.CALL_INITIATION) {
      Navigator.pushNamed(context, '/callscreen', arguments: call);
    }*/
  }

  @override
  void onNewMessage(SIPMessageRequest msg) {
    // TODO: implement onNewMessage
  }

  @override
  void registrationStateChanged(RegistrationState state) {
    // TODO: implement registrationStateChanged
  }

  @override
  void transportStateChanged(TransportState state) {
    // TODO: implement transportStateChanged
  }
}
