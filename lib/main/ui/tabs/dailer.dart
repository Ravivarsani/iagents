import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:TeleVoice/main/ui/create_contact.dart';
import 'package:TeleVoice/main/utils/life_cycle.dart';
import 'package:TeleVoice/main/utils/string_duration.dart';
import 'package:TeleVoice/src/sip_ua_helper.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/model/call_log_model.dart';
import 'package:TeleVoice/main/model/uuid_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:TeleVoice/main/widgets/feedback_radio.dart';
import 'package:TeleVoice/main/widgets/show_feedback.dart';
import 'package:flutter_text_drawable/flutter_text_drawable.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:groovin_widgets/groovin_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:logger/logger.dart';
import 'package:rounded_modal/rounded_modal.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Dialer extends StatefulWidget {
  final SIPUAHelper _helper;

  Dialer(this._helper, {Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DialerPageState();
  }
}

class _DialerPageState extends State<Dialer>
    with HandleLifeCycle, WidgetsBindingObserver
    implements SipUaHelperListener {
  SIPUAHelper get helper => widget._helper;

  SharedPreferences _preferences;
  int userID;
  int page_number = 0;
  String accessToken;
  bool isExpanded = false;
  bool isLoading = false;
  bool loading = true;
  List<CallLogModelData> dataList;
  bool isEnabled = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(new LifecycleEventHandler(this));
    _loadSettings();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(new LifecycleEventHandler(this));
    super.dispose();
  }

  void _loadSettings() async {
    _preferences = await SharedPreferences.getInstance();

    this.setState(() {
      userID = _preferences.getInt('user_id');
      print('=========>>>   $userID');
      accessToken = _preferences.getString('access_token');
      GetDialerData(userID, accessToken);
    });
  }

  Widget _buildPaginatedListView() {
    return Column(
      children: <Widget>[
        Expanded(
            child: NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollInfo) {
            if (!isLoading &&
                scrollInfo.metrics.pixels ==
                    scrollInfo.metrics.maxScrollExtent) {
              this.GetDialerData(userID, accessToken);
              // start loading data
              setState(() {
                isLoading = true;
              });
            }
          },
          child: _listData(dataList),
        )),
        Container(
          height: isLoading ? 70.0 : 0,
          child: Center(
            child: new CircularProgressIndicator(),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      body: loading
          ? Center(
              child: new CircularProgressIndicator(),
            )
          : Center(child: _buildPaginatedListView()),
    );
  }

  Widget _listData(data) {
    if (data.length == 0) {
      return Container(
          child: Text(
        'No logs available',
        style: new TextStyle(fontSize: 20.0),
      ));
    } else {
      return ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (BuildContext context, int index) {
          bool isFeedbackAvail = false;
          if (data.length > 0) {
            if (data[index].feedback.toString() == 'null') {
              isFeedbackAvail = false;
            } else {
              isFeedbackAvail = true;
            }
          }
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Material(
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0))),
              child: GroovinExpansionTile(
                leading: CircleAvatar(
                  child: Text((index + 1).toString()),
                  backgroundColor: Theme.of(context).primaryColor,
                ),
                title: Text(
                  data[index].callerName.toString(),
                  style: TextStyle(
                    color: Theme.of(context).brightness == Brightness.light
                        ? Colors.black
                        : Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                subtitle: Text(data[index].callerNumber),
                onExpansionChanged: (value) {
                  setState(() {
                    isExpanded = value;
                  });
                },
                inkwellRadius: !isExpanded
                    ? BorderRadius.all(Radius.circular(5.0))
                    : BorderRadius.only(
                        topRight: Radius.circular(5.0),
                        topLeft: Radius.circular(5.0),
                      ),
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 16.0, bottom: 4.0),
                        child: Text(
                          DateFormat('dd/MM/yyyy HH:mm:ss').format(
                              DateTime.parse(data[index].callStartTime)
                                  .toLocal()),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 4.0, right: 16),
                        child:
                            Text(data[index].callTalkTime.toString() + ' Sec'),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.phone),
                          tooltip: "Call",
                          onPressed: isEnabled
                              ? () async {
                                  Future<uuid_model> _futureSetCall =
                                      setCallRequest(
                                          userID,
                                          data[index].callerNumber,
                                          accessToken);

                                  _futureSetCall.then((value) {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                      content: Text(value.msg),
                                      duration: Duration(seconds: 1),
                                    ));

                                    if (value.msg ==
                                        'Call originate succesfully.') {
                                      setState(() {
                                        isEnabled = true;
                                      });

                                      Timer timer = new Timer(
                                          const Duration(seconds: 1), () {
                                        Navigator.pushNamedAndRemoveUntil(
                                            context,
                                            "/currentcall",
                                            (Route<dynamic> route) => true);
                                      });
                                    } else {
                                      setState(() {
                                        isEnabled = true;
                                      });
                                    }
                                  });
                                }
                              : null),
                      IconButton(
                        icon: Icon(Icons.person_add),
                        onPressed: () async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => CreateContactPage(
                                    data[index].callerNumber,
                                    data[index].id
                                ),
                              ));
                        },
                        tooltip: "Add Contact",
                      ),
                      isFeedbackAvail
                          ? IconButton(
                              icon: Icon(
                                Icons.feedback,
                                color: Colors.blueAccent,
                              ),
                              onPressed: () async {
                                showRoundedModalBottomSheet(
                                  color: Theme.of(context).canvasColor,
                                  dismissOnTap: false,
                                  context: context,
                                  builder: (builder) {
                                    return Container(
                                      height: 300.0,
                                      color: Colors.transparent,
                                      child: Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: Scaffold(
                                          resizeToAvoidBottomPadding: false,
                                          body: SingleChildScrollView(
                                            child: Column(
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 8.0),
                                                  child: ModalDrawerHandle(),
                                                ),
                                                Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            16.0),
                                                    child: Text('Feedback',
                                                        style: new TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 20.0))),
                                                ShowFeedbackPage(
                                                    data[index].id,
                                                    data[index].feedback,
                                                    data[index].note,
                                                    data[index].scheduledate,
                                                    true),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              },
                              tooltip: "View Feedback",
                            )
                          : IconButton(
                              icon: Icon(
                                Icons.comment,
                                color: Color.fromRGBO(49, 87, 110, 1.0),
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => FeedbackRadio(
                                          data[index].id.toString(),
                                          false,
                                          '',
                                          '',
                                          ''),
                                    ));
                              },
                              tooltip: "Send Feedback",
                            ),
                      IconButton(
                        icon: Icon(GroovinMaterialIcons.play_circle),
                        onPressed: () async {
                          showRoundedModalBottomSheet(
                            color: Theme.of(context).canvasColor,
                            dismissOnTap: false,
                            context: context,
                            builder: (builder) {
                              return Container(
                                height: 220.0,
                                color: Colors.transparent,
                                child: Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Scaffold(
                                    body: Container(
                                      child: Column(
                                        children: <Widget>[
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 8.0),
                                            child: ModalDrawerHandle(),
                                          ),
                                          Padding(
                                              padding:
                                                  const EdgeInsets.all(16.0),
                                              child: Text('Recording',
                                                  style: new TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18.0))),
                                          MyPageWithAudio(
                                              Constant.RECORDING_PATH +
                                                  data[index].recPath),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        },
                        tooltip: "Play Audio",
                      ),
                    ],
                  ),
                ],
              ),
            ),
          );
        },
      );
    }
  }

  Future<uuid_model> setCallRequest(int id, String number, String token) async {
    setState(() {
      isEnabled = false;
    });

    final http.Response response = await http.post(
      Constant.POST_AGENT_OUTGOING_CALL,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'agentid': id.toString(),
        'mobile': number,
      },
    );
    Logger().d("=======>>> response " + response.body.toString());
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return uuid_model.fromJson(json);
    } else {
      setState(() {
        isEnabled = true;
      });
      throw Exception(
          '=====>> Failed to post uuid. ' + response.body.toString());
    }
  }

  Future<List<CallLogModelData>> GetDialerData(int id, String token) async {
    final http.Response response = await http.post(
      Constant.GET_AGENT_CALL_LOG,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
        'userid': id.toString(),
        'type': "1",
        'pageNumber': page_number.toString(),
        'pageSize': "10",
      },
    );

    setState(() {
      isLoading = false;
      loading = false;

      Logger().d("=======>>> Dialer " + response.body.toString());

      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        List<CallLogModelData> newItems = CallLogModel.fromJson(json).data;
        if (dataList == null) {
          dataList = newItems;
        } else {
          dataList.addAll(newItems);
        }
        page_number = page_number + 1;
        return CallLogModel.fromJson(json).data;
      } else {
        throw Exception('=====>>> Failed to fetch data');
      }
    });
  }

  @override
  void onPause() {
    // TODO: implement onPause
  }

  @override
  void onResume() {
    // TODO: implement onResume
    setState(() {
      isEnabled = true;
    });
  }

  @override
  void callStateChanged(Call call, CallState state) {
    // TODO: implement callStateChanged
  }

  @override
  void onNewMessage(SIPMessageRequest msg) {
    // TODO: implement onNewMessage
  }

  @override
  void registrationStateChanged(RegistrationState state) {
    // TODO: implement registrationStateChanged
  }

  @override
  void transportStateChanged(TransportState state) {
    // TODO: implement transportStateChanged
  }
}

class MyPageWithAudio extends StatefulWidget {
  String url;

  MyPageWithAudio(String s) {
    this.url = s;
  }

  @override
  _MyPageWithAudioState createState() => _MyPageWithAudioState(url);
}

class _MyPageWithAudioState extends State<MyPageWithAudio> {
  bool _play = true;
  String _currentPosition = "";
  String mUrl;

  _MyPageWithAudioState(String url) {
    this.mUrl = url;
  }

  @override
  Widget build(BuildContext context) {
    return AudioWidget.network(
      url: mUrl,
      play: _play,
      onReadyToPlay: (total) {
        setState(() {
          _currentPosition = "${Duration().mmSSFormat} / ${total.mmSSFormat}";
        });
      },
      onPositionChanged: (current, total) {
        setState(() {
          _currentPosition = "${current.mmSSFormat} / ${total.mmSSFormat}";
        });
      },
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: RaisedButton(
              shape: CircleBorder(),
              padding: EdgeInsets.all(14),
              color: Theme.of(context).primaryColor,
              child: Icon(
                _play ? Icons.pause : Icons.play_arrow,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  _play = !_play;
                });
              },
            ),
          ),
          Text(_currentPosition),
        ],
      ),
    );
  }
}
