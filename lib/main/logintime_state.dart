import 'package:flutter/material.dart';
import 'package:TeleVoice/main/ui/tabs_time/login_time.dart';
import 'package:TeleVoice/main/ui/tabs_time/logout_time.dart';

class LoginStateWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginStateWidgetState();
  }
}

class _LoginStateWidgetState extends State<LoginStateWidget> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
            title: Text('Activity Log'),
            bottom: TabBar(
              tabs: [
                Tab(
                  text: 'LoginTime',
                ),
                Tab(
                  text: 'LogoutTime',
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              LoginTimeWidget(),
              LogoutTimeWidget(),
            ],
          )),
    );
  }
}
