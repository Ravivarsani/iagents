import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'model/role_model.dart';

class UserVerification extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _UserVerificationState();
  }
}

class _UserVerificationState extends State<UserVerification> {
  bool isInternetConnection = true;
  bool isError = false;
  String mMobile, mUsername, mAccesstoken;
  int mUserID, voipstatus;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  static const platform =
      const MethodChannel('teleforce.flutter.dev/incoming_phone_number');
  String _number = '';

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  TextEditingController _usernameController = TextEditingController();
  Future<RoleModel> _futureRole;

  @override
  void initState() {
    _internetConnection();

    super.initState();
  }

  Future<void> _getIncomingNumber(String num) async {
    String number = '';
    try {
      final String result = await platform.invokeMethod(
          'getIncomingNumber', {'number': num, 'id': mUserID.toString()});
      number = result;
    } on PlatformException catch (e) {
      number = "Failed to get number: '${e.message}'.";
    }

    setState(() {
      _number = number;

      Navigator.pop(_keyLoader.currentContext);

      Fluttertoast.showToast(
          msg: _number,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
          textColor: Colors.white,
          fontSize: 16.0);

      if (_number == 'Verification Successful') {
        getRole(context, mUserID, mAccesstoken, mUsername);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;

    if (arguments != null) {
      Map m = arguments['mapdata'];

      mMobile = m['mobile'];
      mUserID = m['id'];
      mAccesstoken = m['accessToken'];
      mUsername = m['username'];
      voipstatus = m['voipstatus'];
    }

    _usernameController..text = mMobile;

    final mobileField = TextFormField(
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter Mobile';
        }
        return null;
      },
      obscureText: false,
      style: style,
      textInputAction: TextInputAction.next,
      controller: _usernameController,
      decoration: new InputDecoration(
        labelText: "Mobile Number",
        fillColor: Colors.white,
        border: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(25.0),
          borderSide: new BorderSide(),
        ),
        //fillColor: Colors.green
      ),
    );

    if (isInternetConnection != null && isInternetConnection) {
      return new Scaffold(
          appBar: AppBar(
            title: Text('Verification'),
          ),
          body: new SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(36.0),
                child: new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'We will call to your phone, when call will arrived we verify automatically it with our server and drop it',
                      style: TextStyle(fontSize: 17, color: Colors.black),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 50.0),
                    ),
                    mobileField,
                    Padding(
                      padding: EdgeInsets.only(top: 27.0),
                    ),
                    new MaterialButton(
                      color: Color.fromRGBO(49, 87, 110, 1.0),
                      textColor: Colors.white,
                      child: new Text('Verify'),
                      onPressed: () {
                        _verify();
                      },
                    )
                  ],
                ),
              ),
            ),
          ));
    } else if (isInternetConnection != null && !isInternetConnection) {
      return new Scaffold(
          body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(
                  Icons.signal_wifi_off,
                  color: Color.fromRGBO(49, 87, 110, 1.0),
                  size: 65.0,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                new Text('Oops!',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.title),
                Padding(
                  padding: EdgeInsets.only(top: 16.0),
                ),
                new Text('It seems like you are not connected to network.',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.body1),
                Padding(
                  padding: EdgeInsets.only(top: 27.0),
                ),
                new MaterialButton(
                  color: Colors.blue[700],
                  textColor: Colors.white,
                  child: new Text('RETRY'),
                  onPressed: () {
                    if (this.mounted) {
                      setState(() {
                        _internetConnection();
                      });
                    }
                  },
                )
              ],
            ),
          ),
        ],
      ));
    }
  }

  _internetConnection() {
    Constant.isInternetAvailable().then((value) {
      print("=====>> internet connection ==>> $value");
      if (value) {
        isInternetConnection = true;
        if (this.mounted) {
          setState(() {});
        }
      } else {
        isInternetConnection = false;
        if (this.mounted) {
          setState(() {});
        }
      }
    });
  }

  void _verify() {
    if (_usernameController.text.toString().isEmpty) {
      showDialog<Null>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Target is empty.'),
            content: Text('Please enter a number'),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    } else {
      showLoadingDialog(context, _keyLoader);
      _getIncomingNumber('0' + _usernameController.text.toString());
    }
  }

  void getRole(BuildContext cxt, int id, String token, String username) {
    _futureRole = fetchRole(cxt, id, token);
    _futureRole.then((value) => saveRole(value, id, token, username));
  }

  Future<RoleModel> fetchRole(BuildContext cnxt, int id, String token) async {
    final response = await http.get(
      Constant.API_URL + Constant.GET_ROLE + "/" + id.toString(),
      headers: {HttpHeaders.authorizationHeader: "Bearer " + token},
    );

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,

      return RoleModel.fromJson(json.decode(response.body));
    } else if (response.statusCode == 401) {
      Fluttertoast.showToast(
          msg: json.decode(response.body)['message'].toString(),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
          textColor: Colors.white,
          fontSize: 16.0);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception(
          '============>> Failed to load album ' + response.body.toString());
    }
  }

  void saveRole(RoleModel value, int id, String token, String username) async {
    if (value.data.isNotEmpty) {
      String type = value.data.elementAt(0).accountType.toString();

      if (type == '1' || type == '2') {
        //saveLoginSessionToSF(username, token, id);

        saveLoginSessionToSF(username, token, id, voipstatus,
            value.data.elementAt(0).accountType);

        SharedPreferences _preference = await SharedPreferences.getInstance();
        _preference.setInt('role', value.data.elementAt(0).accountType);

        Timer timer = new Timer(const Duration(seconds: 1), () {
          Navigator.pushNamedAndRemoveUntil(
              context, "/home", (Route<dynamic> route) => false);
        });
      } else if (type == '3' || type == '4') {
        Fluttertoast.showToast(
            msg: 'You can not login, This app is not designed for you!',
            toastLength: Toast.LENGTH_LONG,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Color.fromRGBO(49, 87, 110, 1.0),
            textColor: Colors.white,
            fontSize: 17.0);

        Timer timer = new Timer(const Duration(seconds: 1), () {
          Navigator.popAndPushNamed(context, '/login');
        });
      }
    }
  }

  saveLoginSessionToSF(String username, String accessToken, int id,
      int voipstatus, int role) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('islogin', true);
    prefs.setString('auth_user', username);
    prefs.setString('access_token', accessToken);
    prefs.setInt('user_id', id);

    prefs.setInt('role', role);

    if (voipstatus == 0) {
      prefs.setBool('voip_status', true);
    } else {
      prefs.setBool('voip_status', false);
    }

    Navigator.pushNamedAndRemoveUntil(
        context, "/home", (Route<dynamic> route) => false);
  }

  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new Padding(
            padding: EdgeInsets.all(20.0),
            child: WillPopScope(
                onWillPop: () async => false,
                child: SimpleDialog(
                    key: key,
                    backgroundColor: Colors.black38,
                    children: <Widget>[
                      Center(
                        child: Column(children: [
                          CircularProgressIndicator(),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            "Please wait, We trying to auto detect call for your verification ",
                            style: TextStyle(color: Colors.white),
                          )
                        ]),
                      )
                    ])),
          );
        });
  }
}
