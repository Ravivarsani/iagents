class AgentGroupModelData {
/*
{
  "GroupID": 24,
  "GroupName": "demo group",
  "GroupDescription": "demo",
  "AccountID": 122,
  "CallType": 0,
  "MusicOnHoldID": 58,
  "Sticky": 0,
  "QueueTimeOut": 10,
  "AgentTimeOut": 10,
  "WrapUpTime": 10,
  "CallRecording": 1,
  "OpenTime": "11:45:00",
  "CloseTime": "16:51:00",
  "Closed": 0,
  "Mon": 1,
  "Tue": 1,
  "Wed": 1,
  "Thu": 1,
  "Fri": 1,
  "Sat": 1,
  "Sun": 1,
  "CloseType": 0,
  "CloseAction": 0,
  "ApiHit": 0,
  "ApiID": "0",
  "transfer_extesion": ""
}
*/

  int GroupID;
  String GroupName;
  String GroupDescription;
  int AccountID;
  int CallType;
  int MusicOnHoldID;
  int Sticky;
  int QueueTimeOut;
  int AgentTimeOut;
  int WrapUpTime;
  int CallRecording;
  String OpenTime;
  String CloseTime;
  int Closed;
  int Mon;
  int Tue;
  int Wed;
  int Thu;
  int Fri;
  int Sat;
  int Sun;
  int CloseType;
  int CloseAction;
  int ApiHit;
  String ApiID;
  String transferExtesion;

  AgentGroupModelData({
    this.GroupID,
    this.GroupName,
    this.GroupDescription,
    this.AccountID,
    this.CallType,
    this.MusicOnHoldID,
    this.Sticky,
    this.QueueTimeOut,
    this.AgentTimeOut,
    this.WrapUpTime,
    this.CallRecording,
    this.OpenTime,
    this.CloseTime,
    this.Closed,
    this.Mon,
    this.Tue,
    this.Wed,
    this.Thu,
    this.Fri,
    this.Sat,
    this.Sun,
    this.CloseType,
    this.CloseAction,
    this.ApiHit,
    this.ApiID,
    this.transferExtesion,
  });
  AgentGroupModelData.fromJson(Map<String, dynamic> json) {
    GroupID = json["GroupID"]?.toInt();
    GroupName = json["GroupName"]?.toString();
    GroupDescription = json["GroupDescription"]?.toString();
    AccountID = json["AccountID"]?.toInt();
    CallType = json["CallType"]?.toInt();
    MusicOnHoldID = json["MusicOnHoldID"]?.toInt();
    Sticky = json["Sticky"]?.toInt();
    QueueTimeOut = json["QueueTimeOut"]?.toInt();
    AgentTimeOut = json["AgentTimeOut"]?.toInt();
    WrapUpTime = json["WrapUpTime"]?.toInt();
    CallRecording = json["CallRecording"]?.toInt();
    OpenTime = json["OpenTime"]?.toString();
    CloseTime = json["CloseTime"]?.toString();
    Closed = json["Closed"]?.toInt();
    Mon = json["Mon"]?.toInt();
    Tue = json["Tue"]?.toInt();
    Wed = json["Wed"]?.toInt();
    Thu = json["Thu"]?.toInt();
    Fri = json["Fri"]?.toInt();
    Sat = json["Sat"]?.toInt();
    Sun = json["Sun"]?.toInt();
    CloseType = json["CloseType"]?.toInt();
    CloseAction = json["CloseAction"]?.toInt();
    ApiHit = json["ApiHit"]?.toInt();
    ApiID = json["ApiID"]?.toString();
    transferExtesion = json["transfer_extesion"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["GroupID"] = GroupID;
    data["GroupName"] = GroupName;
    data["GroupDescription"] = GroupDescription;
    data["AccountID"] = AccountID;
    data["CallType"] = CallType;
    data["MusicOnHoldID"] = MusicOnHoldID;
    data["Sticky"] = Sticky;
    data["QueueTimeOut"] = QueueTimeOut;
    data["AgentTimeOut"] = AgentTimeOut;
    data["WrapUpTime"] = WrapUpTime;
    data["CallRecording"] = CallRecording;
    data["OpenTime"] = OpenTime;
    data["CloseTime"] = CloseTime;
    data["Closed"] = Closed;
    data["Mon"] = Mon;
    data["Tue"] = Tue;
    data["Wed"] = Wed;
    data["Thu"] = Thu;
    data["Fri"] = Fri;
    data["Sat"] = Sat;
    data["Sun"] = Sun;
    data["CloseType"] = CloseType;
    data["CloseAction"] = CloseAction;
    data["ApiHit"] = ApiHit;
    data["ApiID"] = ApiID;
    data["transfer_extesion"] = transferExtesion;
    return data;
  }
}

class AgentGroupModel {
  List<AgentGroupModelData> data;

  AgentGroupModel({
    this.data,
  });
  AgentGroupModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<AgentGroupModelData>();
      json['data'].forEach((v) {
        data.add(new AgentGroupModelData.fromJson(v));
      });
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
