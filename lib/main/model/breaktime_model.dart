class BreakTimeModelData {
/*
{
  "break_id": 1,
  "account_id": "188",
  "date": "2020-09-09T18:30:00.000Z",
  "start_time": "2020-09-10T04:13:00.000Z",
  "stop_time": null,
  "break_status": 0
}
*/

  int breakId;
  String accountId;
  String date;
  String startTime;
  String stopTime;
  int breakStatus;

  BreakTimeModelData({
    this.breakId,
    this.accountId,
    this.date,
    this.startTime,
    this.stopTime,
    this.breakStatus,
  });

  BreakTimeModelData.fromJson(Map<String, dynamic> json) {
    breakId = json["break_id"]?.toInt();
    accountId = json["account_id"]?.toString();
    date = json["date"]?.toString();
    startTime = json["start_time"]?.toString();
    stopTime = json["stop_time"]?.toString();
    breakStatus = json["break_status"]?.toInt();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["break_id"] = breakId;
    data["account_id"] = accountId;
    data["date"] = date;
    data["start_time"] = startTime;
    data["stop_time"] = stopTime;
    data["break_status"] = breakStatus;
    return data;
  }
}

class BreakTimeModel {
/*
{
  "data": {
    "break_id": 1,
    "account_id": "188",
    "date": "2020-09-09T18:30:00.000Z",
    "start_time": "2020-09-10T04:13:00.000Z",
    "stop_time": null,
    "break_status": 0
  }
}
*/

  BreakTimeModelData data;

  BreakTimeModel({
    this.data,
  });

  BreakTimeModel.fromJson(Map<String, dynamic> json) {
    data =
        json["data"] != null ? BreakTimeModelData.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (data != null) {
      data["data"] = this.data.toJson();
    }
    return data;
  }
}
