class ConferenceModelData {

  int confId;
  String confName;
  int confType;
  int confMembertype;
  String confMember;
  int confStart;
  String confDate;
  int confPin;
  int confAutoid;
  int accountId;
  int confMaxUser;
  String createDate;

  ConferenceModelData({
    this.confId,
    this.confName,
    this.confType,
    this.confMembertype,
    this.confMember,
    this.confStart,
    this.confDate,
    this.confPin,
    this.confAutoid,
    this.accountId,
    this.confMaxUser,
    this.createDate,
  });
  ConferenceModelData.fromJson(Map<String, dynamic> json) {
    confId = json["conf_id"]?.toInt();
    confName = json["conf_name"]?.toString();
    confType = json["conf_type"]?.toInt();
    confMembertype = json["conf_membertype"]?.toInt();
    confMember = json["conf_member"]?.toString();
    confStart = json["conf_start"]?.toInt();
    confDate = json["conf_date"]?.toString();
    confPin = json["conf_pin"]?.toInt();
    confAutoid = json["conf_autoid"]?.toInt();
    accountId = json["account_id"]?.toInt();
    confMaxUser = json["conf_max_user"]?.toInt();
    createDate = json["create_date"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["conf_id"] = confId;
    data["conf_name"] = confName;
    data["conf_type"] = confType;
    data["conf_membertype"] = confMembertype;
    data["conf_member"] = confMember;
    data["conf_start"] = confStart;
    data["conf_date"] = confDate;
    data["conf_pin"] = confPin;
    data["conf_autoid"] = confAutoid;
    data["account_id"] = accountId;
    data["conf_max_user"] = confMaxUser;
    data["create_date"] = createDate;
    return data;
  }
}

class ConferenceModel {

  List<ConferenceModelData> data;

  ConferenceModel({
    this.data,
  });
  ConferenceModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<ConferenceModelData>();
      json['data'].forEach((v) {
        data.add(new ConferenceModelData.fromJson(v));
      });
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
