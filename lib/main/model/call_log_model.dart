class CallLogModelData {
  int id;
  String uniqueid;
  int accountid;
  int dID;
  int callerType;
  int agentID;
  String agentName;
  String agentNumber;
  String callerNumber;
  String callerName;
  String cIRCLE;
  String oPERATOR;
  String hangupCause;
  int groupID;
  String groupName;
  int callTypeID;
  String callType;
  String callStartTime;
  String callAnswerTime;
  String callEndTime;
  int callDuration;
  int callTalkTime;
  String agentCallStartTime;
  String agentCallAnswerTime;
  String agentCallEndTime;
  int agentTalkTime;
  String callStatus;
  String agentStatus;
  String customerStatus;
  Null callDisposition;
  String disconnectedBy;
  String lastDestination;
  String recPath;
  int cdrStatus;
  String feedback;
  String note;
  String scheduledate;

  CallLogModelData(
      {this.id,
      this.uniqueid,
      this.accountid,
      this.dID,
      this.callerType,
      this.agentID,
      this.agentName,
      this.agentNumber,
      this.callerNumber,
      this.callerName,
      this.cIRCLE,
      this.oPERATOR,
      this.hangupCause,
      this.groupID,
      this.groupName,
      this.callTypeID,
      this.callType,
      this.callStartTime,
      this.callAnswerTime,
      this.callEndTime,
      this.callDuration,
      this.callTalkTime,
      this.agentCallStartTime,
      this.agentCallAnswerTime,
      this.agentCallEndTime,
      this.agentTalkTime,
      this.callStatus,
      this.agentStatus,
      this.customerStatus,
      this.callDisposition,
      this.disconnectedBy,
      this.lastDestination,
      this.recPath,
      this.cdrStatus,
      this.feedback,
      this.note,
      this.scheduledate});

  CallLogModelData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    uniqueid = json['uniqueid'];
    accountid = json['accountid'];
    dID = json['DID'];
    callerType = json['CallerType'];
    agentID = json['AgentID'];
    agentName = json['AgentName'];
    agentNumber = json['AgentNumber'];
    callerNumber = json['CallerNumber'];
    callerName = json['CallerName'];
    cIRCLE = json['CIRCLE'];
    oPERATOR = json['OPERATOR'];
    hangupCause = json['HangupCause'];
    groupID = json['GroupID'];
    groupName = json['GroupName'];
    callTypeID = json['CallTypeID'];
    callType = json['CallType'];
    callStartTime = json['CallStartTime'];
    callAnswerTime = json['CallAnswerTime'];
    callEndTime = json['CallEndTime'];
    callDuration = json['CallDuration'];
    callTalkTime = json['CallTalkTime'];
    agentCallStartTime = json['AgentCallStartTime'];
    agentCallAnswerTime = json['AgentCallAnswerTime'];
    agentCallEndTime = json['AgentCallEndTime'];
    agentTalkTime = json['AgentTalkTime'];
    callStatus = json['CallStatus'];
    agentStatus = json['AgentStatus'];
    customerStatus = json['CustomerStatus'];
    callDisposition = json['CallDisposition'];
    disconnectedBy = json['DisconnectedBy'];
    lastDestination = json['LastDestination'];
    recPath = json['RecPath'];
    cdrStatus = json['CdrStatus'];
    feedback = json['feedback'];
    note = json['note'];
    scheduledate = json['scheduledate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uniqueid'] = this.uniqueid;
    data['accountid'] = this.accountid;
    data['DID'] = this.dID;
    data['CallerType'] = this.callerType;
    data['AgentID'] = this.agentID;
    data['AgentName'] = this.agentName;
    data['AgentNumber'] = this.agentNumber;
    data['CallerNumber'] = this.callerNumber;
    data['CallerName'] = this.callerName;
    data['CIRCLE'] = this.cIRCLE;
    data['OPERATOR'] = this.oPERATOR;
    data['HangupCause'] = this.hangupCause;
    data['GroupID'] = this.groupID;
    data['GroupName'] = this.groupName;
    data['CallTypeID'] = this.callTypeID;
    data['CallType'] = this.callType;
    data['CallStartTime'] = this.callStartTime;
    data['CallAnswerTime'] = this.callAnswerTime;
    data['CallEndTime'] = this.callEndTime;
    data['CallDuration'] = this.callDuration;
    data['CallTalkTime'] = this.callTalkTime;
    data['AgentCallStartTime'] = this.agentCallStartTime;
    data['AgentCallAnswerTime'] = this.agentCallAnswerTime;
    data['AgentCallEndTime'] = this.agentCallEndTime;
    data['AgentTalkTime'] = this.agentTalkTime;
    data['CallStatus'] = this.callStatus;
    data['AgentStatus'] = this.agentStatus;
    data['CustomerStatus'] = this.customerStatus;
    data['CallDisposition'] = this.callDisposition;
    data['DisconnectedBy'] = this.disconnectedBy;
    data['LastDestination'] = this.lastDestination;
    data['RecPath'] = this.recPath;
    data['CdrStatus'] = this.cdrStatus;
    data['feedback'] = this.feedback;
    data['note'] = this.note;
    data['scheduledate'] = this.scheduledate;
    return data;
  }
}

class CallLogModel {
  int total;
  List<CallLogModelData> data;

  CallLogModel({
    this.total,
    this.data,
  });

  CallLogModel.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    if (json['data'] != null) {
      data = new List<CallLogModelData>();
      json['data'].forEach((v) {
        data.add(new CallLogModelData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['total'] = this.total;

    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
