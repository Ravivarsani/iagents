class AgentDetail {
  DataAgent data;

  AgentDetail({this.data});

  AgentDetail.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new DataAgent.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class DataAgent {
  int accountType;
  int accountId;
  String didAlloted;
  String accountName;
  String accountPassword;
  String email;
  String mobile;
  String alternateMobile;
  String isMobileVerify;
  Null companyName;
  int showNoId;
  Null pulse;
  int credits;
  int balance;
  int channels;
  int incomingChannel;
  int outgoingChannel;
  Null startDate;
  String createDate;
  Null expiryDate;
  String lastModifyDate;
  int currentStatus;
  int voipStatus;
  String startTime;
  String endTime;
  String createdBy;
  String lastModifyBy;
  String sourceFilePath;
  Null services;
  String timeCondition;
  int kycStatus;
  int callTrunkType;
  String callTrunkName;
  Null tollFreeNo;
  Null loginTime;
  Null accessKey;
  String uuid;
  Null userLimit;
  String isLoggedin;
  Null packageId;
  String zohoEnable;
  Null totalRenew;
  Null companyAddress;
  String authorisedPersonName;
  String loginIp;
  String webtoken;

  DataAgent(
      {this.accountType,
      this.accountId,
      this.didAlloted,
      this.accountName,
      this.accountPassword,
      this.email,
      this.mobile,
      this.alternateMobile,
      this.isMobileVerify,
      this.companyName,
      this.showNoId,
      this.pulse,
      this.credits,
      this.balance,
      this.channels,
      this.incomingChannel,
      this.outgoingChannel,
      this.startDate,
      this.createDate,
      this.expiryDate,
      this.lastModifyDate,
      this.currentStatus,
      this.voipStatus,
      this.startTime,
      this.endTime,
      this.createdBy,
      this.lastModifyBy,
      this.sourceFilePath,
      this.services,
      this.timeCondition,
      this.kycStatus,
      this.callTrunkType,
      this.callTrunkName,
      this.tollFreeNo,
      this.loginTime,
      this.accessKey,
      this.uuid,
      this.userLimit,
      this.isLoggedin,
      this.packageId,
      this.zohoEnable,
      this.totalRenew,
      this.companyAddress,
      this.authorisedPersonName,
      this.loginIp,
      this.webtoken});

  DataAgent.fromJson(Map<String, dynamic> json) {
    accountType = json['account_type'];
    accountId = json['account_id'];
    didAlloted = json['did_alloted'];
    accountName = json['account_name'];
    accountPassword = json['account_password'];
    email = json['email'];
    mobile = json['mobile'];
    alternateMobile = json['alternate_mobile'];
    isMobileVerify = json['is_mobile_verify'];
    companyName = json['company_name'];
    showNoId = json['show_no_id'];
    pulse = json['pulse'];
    credits = json['credits'];
    balance = json['balance'];
    channels = json['channels'];
    incomingChannel = json['incoming_channel'];
    outgoingChannel = json['outgoing_channel'];
    startDate = json['start_date'];
    createDate = json['create_date'];
    expiryDate = json['expiry_date'];
    lastModifyDate = json['last_modify_date'];
    currentStatus = json['current_status'];
    voipStatus = json['voip_status'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    createdBy = json['created_by'];
    lastModifyBy = json['last_modify_by'];
    sourceFilePath = json['source_file_path'];
    services = json['services'];
    timeCondition = json['TimeCondition'];
    kycStatus = json['kyc_status'];
    callTrunkType = json['CallTrunkType'];
    callTrunkName = json['CallTrunkName'];
    tollFreeNo = json['TollFreeNo'];
    loginTime = json['login_time'];
    accessKey = json['AccessKey'];
    uuid = json['uuid'];
    userLimit = json['user_limit'];
    isLoggedin = json['is_loggedin'];
    packageId = json['package_id'];
    zohoEnable = json['zoho_enable'];
    totalRenew = json['total_renew'];
    companyAddress = json['company_address'];
    authorisedPersonName = json['authorised_person_name'];
    loginIp = json['login_ip'];
    webtoken = json['webtoken'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['account_type'] = this.accountType;
    data['account_id'] = this.accountId;
    data['did_alloted'] = this.didAlloted;
    data['account_name'] = this.accountName;
    data['account_password'] = this.accountPassword;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['alternate_mobile'] = this.alternateMobile;
    data['is_mobile_verify'] = this.isMobileVerify;
    data['company_name'] = this.companyName;
    data['show_no_id'] = this.showNoId;
    data['pulse'] = this.pulse;
    data['credits'] = this.credits;
    data['balance'] = this.balance;
    data['channels'] = this.channels;
    data['incoming_channel'] = this.incomingChannel;
    data['outgoing_channel'] = this.outgoingChannel;
    data['start_date'] = this.startDate;
    data['create_date'] = this.createDate;
    data['expiry_date'] = this.expiryDate;
    data['last_modify_date'] = this.lastModifyDate;
    data['current_status'] = this.currentStatus;
    data['voip_status'] = this.voipStatus;
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    data['created_by'] = this.createdBy;
    data['last_modify_by'] = this.lastModifyBy;
    data['source_file_path'] = this.sourceFilePath;
    data['services'] = this.services;
    data['TimeCondition'] = this.timeCondition;
    data['kyc_status'] = this.kycStatus;
    data['CallTrunkType'] = this.callTrunkType;
    data['CallTrunkName'] = this.callTrunkName;
    data['TollFreeNo'] = this.tollFreeNo;
    data['login_time'] = this.loginTime;
    data['AccessKey'] = this.accessKey;
    data['uuid'] = this.uuid;
    data['user_limit'] = this.userLimit;
    data['is_loggedin'] = this.isLoggedin;
    data['package_id'] = this.packageId;
    data['zoho_enable'] = this.zohoEnable;
    data['total_renew'] = this.totalRenew;
    data['company_address'] = this.companyAddress;
    data['authorised_person_name'] = this.authorisedPersonName;
    data['login_ip'] = this.loginIp;
    data['webtoken'] = this.webtoken;
    return data;
  }
}
