class CallerNumber {
  List<DataCallerNumber> data;

  CallerNumber({this.data});

  CallerNumber.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<DataCallerNumber>();
      json['data'].forEach((v) {
        data.add(new DataCallerNumber.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class DataCallerNumber {
  String contId;
  int customerId;
  String name;
  String email;
  String mobile;
  String address;
  String city;
  String createdDate;
  String state;
  String category;
  String isMobileDnd;
  Null leadSource;
  Null leadStatus;
  Null createdBy;

  DataCallerNumber(
      {this.contId,
      this.customerId,
      this.name,
      this.email,
      this.mobile,
      this.address,
      this.city,
      this.createdDate,
      this.state,
      this.category,
      this.isMobileDnd,
      this.leadSource,
      this.leadStatus,
      this.createdBy});

  DataCallerNumber.fromJson(Map<String, dynamic> json) {
    contId = json['cont_id'];
    customerId = json['customer_id'];
    name = json['name'];
    email = json['email'];
    mobile = json['mobile'];
    address = json['address'];
    city = json['city'];
    createdDate = json['created_date'];
    state = json['state'];
    category = json['category'];
    isMobileDnd = json['is_mobile_dnd'];
    leadSource = json['lead_source'];
    leadStatus = json['lead_status'];
    createdBy = json['created_by'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cont_id'] = this.contId;
    data['customer_id'] = this.customerId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['address'] = this.address;
    data['city'] = this.city;
    data['created_date'] = this.createdDate;
    data['state'] = this.state;
    data['category'] = this.category;
    data['is_mobile_dnd'] = this.isMobileDnd;
    data['lead_source'] = this.leadSource;
    data['lead_status'] = this.leadStatus;
    data['created_by'] = this.createdBy;
    return data;
  }
}
