class DidConfigModel {
  String msg;

  DidConfigModel({this.msg});

  DidConfigModel.fromJson(Map<String, dynamic> json) {
    msg = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.msg;
    return data;
  }
}