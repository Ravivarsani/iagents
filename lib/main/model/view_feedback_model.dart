class ViewFeedbackModelData {
  int id;
  String note;
  String scheduledate;
  String feedback;

  ViewFeedbackModelData({
    this.id,
    this.note,
    this.scheduledate,
    this.feedback,
  });

  ViewFeedbackModelData.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    note = json["note"];
    scheduledate = json["scheduledate"];
    feedback = json["feedback"];
  }
}

class ViewFeedbackModel {
  List<ViewFeedbackModelData> data;

  ViewFeedbackModel({
    this.data,
  });

  ViewFeedbackModel.fromJson(Map<String, dynamic> json) {
    if (json["data"] != null) {
      var v = json["data"];
      var arr0 = List<ViewFeedbackModelData>();
      v.forEach((v) {
        arr0.add(ViewFeedbackModelData.fromJson(v));
      });
      data = arr0;
    }
  }

  List<ViewFeedbackModelData> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    if (data != null) {
      List<dynamic> dynamicList = data['data'];
      List<ViewFeedbackModelData> role = new List<ViewFeedbackModelData>();

      dynamicList.forEach((f) {
        ViewFeedbackModelData modelData = ViewFeedbackModelData.fromJson(f);
        role.add(modelData);
      });

      return role;
    }
  }
}
