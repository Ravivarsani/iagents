class AssignedleadModelData {
  String name;
  String mobile;
  int numid;
  int cam_id;

  AssignedleadModelData({this.name, this.mobile, this.numid, this.cam_id});

  AssignedleadModelData.fromJson(Map<String, dynamic> json) {
    name = json["name"]?.toString();
    mobile = json["mobile"]?.toString();
    numid = json["numid"]?.toInt();
    cam_id = json["cam_id"]?.toInt();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["name"] = name;
    data["mobile"] = mobile;
    data["numid"] = numid;
    data["cam_id"] = cam_id;
    return data;
  }
}

class AssignedleadModel {
  List<AssignedleadModelData> data;

  AssignedleadModel({
    this.data,
  });

  AssignedleadModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<AssignedleadModelData>();
      json['data'].forEach((v) {
        data.add(new AssignedleadModelData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
