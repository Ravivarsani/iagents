class feedback_model {
  String msg;

  feedback_model({this.msg});

  feedback_model.fromJson(Map<String, dynamic> json) {
    msg = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.msg;
    return data;
  }
}