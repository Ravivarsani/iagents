class IVRListModelData {

  int BuilderID;
  int UserID;
  String IvrName;
  String IvrDesc;
  int AnnouncementID;
  int AnnouncementType;
  int TimeOut;
  int InvalidAnnounceMentID;
  int InvalidRetryCount;
  int InvalidRetryAnnouncementID;
  int InvalidDestinationType;
  int InvalidDestinationAction;
  int MergeInvalidOnAnnounceMent;
  int NoInputAnnouncementID;
  int NoInputRetryCount;
  int NoInputRetryAnnouncementID;
  int NoInputDestinationType;
  int NoInputDestinationAction;
  int DefaultDestinationType;
  int DefaultDestinationAction;
  String CreateDate;
  int ApiHit;
  String ApiID;

  IVRListModelData({
    this.BuilderID,
    this.UserID,
    this.IvrName,
    this.IvrDesc,
    this.AnnouncementID,
    this.AnnouncementType,
    this.TimeOut,
    this.InvalidAnnounceMentID,
    this.InvalidRetryCount,
    this.InvalidRetryAnnouncementID,
    this.InvalidDestinationType,
    this.InvalidDestinationAction,
    this.MergeInvalidOnAnnounceMent,
    this.NoInputAnnouncementID,
    this.NoInputRetryCount,
    this.NoInputRetryAnnouncementID,
    this.NoInputDestinationType,
    this.NoInputDestinationAction,
    this.DefaultDestinationType,
    this.DefaultDestinationAction,
    this.CreateDate,
    this.ApiHit,
    this.ApiID,
  });
  IVRListModelData.fromJson(Map<String, dynamic> json) {
    BuilderID = json["BuilderID"]?.toInt();
    UserID = json["UserID"]?.toInt();
    IvrName = json["IvrName"]?.toString();
    IvrDesc = json["IvrDesc"]?.toString();
    AnnouncementID = json["AnnouncementID"]?.toInt();
    AnnouncementType = json["AnnouncementType"]?.toInt();
    TimeOut = json["TimeOut"]?.toInt();
    InvalidAnnounceMentID = json["InvalidAnnounceMentID"]?.toInt();
    InvalidRetryCount = json["InvalidRetryCount"]?.toInt();
    InvalidRetryAnnouncementID = json["InvalidRetryAnnouncementID"]?.toInt();
    InvalidDestinationType = json["InvalidDestinationType"]?.toInt();
    InvalidDestinationAction = json["InvalidDestinationAction"]?.toInt();
    MergeInvalidOnAnnounceMent = json["MergeInvalidOnAnnounceMent"]?.toInt();
    NoInputAnnouncementID = json["NoInputAnnouncementID"]?.toInt();
    NoInputRetryCount = json["NoInputRetryCount"]?.toInt();
    NoInputRetryAnnouncementID = json["NoInputRetryAnnouncementID"]?.toInt();
    NoInputDestinationType = json["NoInputDestinationType"]?.toInt();
    NoInputDestinationAction = json["NoInputDestinationAction"]?.toInt();
    DefaultDestinationType = json["DefaultDestinationType"]?.toInt();
    DefaultDestinationAction = json["DefaultDestinationAction"]?.toInt();
    CreateDate = json["CreateDate"]?.toString();
    ApiHit = json["ApiHit"]?.toInt();
    ApiID = json["ApiID"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["BuilderID"] = BuilderID;
    data["UserID"] = UserID;
    data["IvrName"] = IvrName;
    data["IvrDesc"] = IvrDesc;
    data["AnnouncementID"] = AnnouncementID;
    data["AnnouncementType"] = AnnouncementType;
    data["TimeOut"] = TimeOut;
    data["InvalidAnnounceMentID"] = InvalidAnnounceMentID;
    data["InvalidRetryCount"] = InvalidRetryCount;
    data["InvalidRetryAnnouncementID"] = InvalidRetryAnnouncementID;
    data["InvalidDestinationType"] = InvalidDestinationType;
    data["InvalidDestinationAction"] = InvalidDestinationAction;
    data["MergeInvalidOnAnnounceMent"] = MergeInvalidOnAnnounceMent;
    data["NoInputAnnouncementID"] = NoInputAnnouncementID;
    data["NoInputRetryCount"] = NoInputRetryCount;
    data["NoInputRetryAnnouncementID"] = NoInputRetryAnnouncementID;
    data["NoInputDestinationType"] = NoInputDestinationType;
    data["NoInputDestinationAction"] = NoInputDestinationAction;
    data["DefaultDestinationType"] = DefaultDestinationType;
    data["DefaultDestinationAction"] = DefaultDestinationAction;
    data["CreateDate"] = CreateDate;
    data["ApiHit"] = ApiHit;
    data["ApiID"] = ApiID;
    return data;
  }
}

class IVRListModel {

  List<IVRListModelData> data;

  IVRListModel({
    this.data,
  });
  IVRListModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<IVRListModelData>();
      json['data'].forEach((v) {
        data.add(new IVRListModelData.fromJson(v));
      });
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
