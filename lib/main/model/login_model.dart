class login_model {
  bool islogin;
  User user;
  String accessToken;
  int expiresIn;
  String msg;

  login_model({this.islogin, this.msg, this.user, this.accessToken, this.expiresIn});

  login_model.fromJson(Map<String, dynamic> json) {
    islogin = json['islogin'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    accessToken = json['access_token'];
    expiresIn = json['expires_in'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['islogin'] = this.islogin;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    if (!this.islogin) {
      data['msg'] = this.msg;
    }
    data['access_token'] = this.accessToken;
    data['expires_in'] = this.expiresIn;
    return data;
  }
}

class User {
  int id;
  String username;
  String mobile;
  String mobileverify;
  int voip_status;

  User({this.id, this.username});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['name'];
    mobile = json['mobile'];
    mobileverify = json['mobileverify'];
    voip_status = json['voip_status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.username;
    data['mobile'] = this.mobile;
    data['mobileverify'] = this.mobileverify;
    data['voip_status'] = this.voip_status;
    return data;
  }
}
