class contact_segmnet {
  List<Data> data;

  contact_segmnet({this.data});

  contact_segmnet.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String contId;
  String customerId;
  String name;
  String createdDate;

  Data({this.contId, this.customerId, this.name, this.createdDate});

  Data.fromJson(Map<String, dynamic> json) {
    contId = json['cont_id'];
    customerId = json['customer_id'];
    name = json['name'];
    createdDate = json['created_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['cont_id'] = this.contId;
    data['customer_id'] = this.customerId;
    data['name'] = this.name;
    data['created_date'] = this.createdDate;
    return data;
  }
}
