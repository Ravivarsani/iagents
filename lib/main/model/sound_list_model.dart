class SoundListModelData {

  int id;
  int accountId;
  String fileid;
  String filename;
  String originalfilename;
  String filesize;
  double duration;
  int active;
  int approved;
  int sorter;
  int status;
  String note;
  String approvedDate;
  String addDate;
  String updateDate;

  SoundListModelData({
    this.id,
    this.accountId,
    this.fileid,
    this.filename,
    this.originalfilename,
    this.filesize,
    this.duration,
    this.active,
    this.approved,
    this.sorter,
    this.status,
    this.note,
    this.approvedDate,
    this.addDate,
    this.updateDate,
  });
  SoundListModelData.fromJson(Map<String, dynamic> json) {
    id = json["id"]?.toInt();
    accountId = json["account_id"]?.toInt();
    fileid = json["fileid"]?.toString();
    filename = json["filename"]?.toString();
    originalfilename = json["originalfilename"]?.toString();
    filesize = json["filesize"]?.toString();
    duration = json["duration"]?.toDouble();
    active = json["active"]?.toInt();
    approved = json["approved"]?.toInt();
    sorter = json["sorter"]?.toInt();
    status = json["status"]?.toInt();
    note = json["note"]?.toString();
    approvedDate = json["approved_date"]?.toString();
    addDate = json["addDate"]?.toString();
    updateDate = json["updateDate"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["id"] = id;
    data["account_id"] = accountId;
    data["fileid"] = fileid;
    data["filename"] = filename;
    data["originalfilename"] = originalfilename;
    data["filesize"] = filesize;
    data["duration"] = duration;
    data["active"] = active;
    data["approved"] = approved;
    data["sorter"] = sorter;
    data["status"] = status;
    data["note"] = note;
    data["approved_date"] = approvedDate;
    data["addDate"] = addDate;
    data["updateDate"] = updateDate;
    return data;
  }
}

class SoundListModel {

  List<SoundListModelData> data;

  SoundListModel({
    this.data,
  });
  SoundListModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<SoundListModelData>();
      json['data'].forEach((v) {
        data.add(new SoundListModelData.fromJson(v));
      });
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
