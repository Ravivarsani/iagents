class AgentModelData {
  int accountType;
  int accountId;
  String didAlloted;
  String accountName;
  String accountPassword;
  String email;
  String mobile;
  String companyName;
  int showNoId;
  String pulse;
  int credits;
  int balance;
  int channels;
  String startDate;
  String createDate;
  String expiryDate;
  String lastModifyDate;
  int currentStatus;
  String startTime;
  String endTime;
  String createdBy;
  String lastModifyBy;
  String sourceFilePath;
  String services;
  String TimeCondition;
  int kycStatus;
  int CallTrunkType;
  String CallTrunkName;
  String TollFreeNo;
  String loginTime;
  String AccessKey;
  String uuid;
  int userLimit;
  String isLoggedin;
  String packageId;

  AgentModelData({
    this.accountType,
    this.accountId,
    this.didAlloted,
    this.accountName,
    this.accountPassword,
    this.email,
    this.mobile,
    this.companyName,
    this.showNoId,
    this.pulse,
    this.credits,
    this.balance,
    this.channels,
    this.startDate,
    this.createDate,
    this.expiryDate,
    this.lastModifyDate,
    this.currentStatus,
    this.startTime,
    this.endTime,
    this.createdBy,
    this.lastModifyBy,
    this.sourceFilePath,
    this.services,
    this.TimeCondition,
    this.kycStatus,
    this.CallTrunkType,
    this.CallTrunkName,
    this.TollFreeNo,
    this.loginTime,
    this.AccessKey,
    this.uuid,
    this.userLimit,
    this.isLoggedin,
    this.packageId,
  });
  AgentModelData.fromJson(Map<String, dynamic> json) {
    accountType = json["account_type"]?.toInt();
    accountId = json["account_id"]?.toInt();
    didAlloted = json["did_alloted"]?.toString();
    accountName = json["account_name"]?.toString();
    accountPassword = json["account_password"]?.toString();
    email = json["email"]?.toString();
    mobile = json["mobile"]?.toString();
    companyName = json["company_name"]?.toString();
    showNoId = json["show_no_id"]?.toInt();
    pulse = json["pulse"]?.toString();
    credits = json["credits"]?.toInt();
    balance = json["balance"]?.toInt();
    channels = json["channels"]?.toInt();
    startDate = json["start_date"]?.toString();
    createDate = json["create_date"]?.toString();
    expiryDate = json["expiry_date"]?.toString();
    lastModifyDate = json["last_modify_date"]?.toString();
    currentStatus = json["current_status"]?.toInt();
    startTime = json["start_time"]?.toString();
    endTime = json["end_time"]?.toString();
    createdBy = json["created_by"]?.toString();
    lastModifyBy = json["last_modify_by"]?.toString();
    sourceFilePath = json["source_file_path"]?.toString();
    services = json["services"]?.toString();
    TimeCondition = json["TimeCondition"]?.toString();
    kycStatus = json["kyc_status"]?.toInt();
    CallTrunkType = json["CallTrunkType"]?.toInt();
    CallTrunkName = json["CallTrunkName"]?.toString();
    TollFreeNo = json["TollFreeNo"]?.toString();
    loginTime = json["login_time"]?.toString();
    AccessKey = json["AccessKey"]?.toString();
    uuid = json["uuid"]?.toString();
    userLimit = json["user_limit"]?.toInt();
    isLoggedin = json["is_loggedin"]?.toString();
    packageId = json["package_id"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["account_type"] = accountType;
    data["account_id"] = accountId;
    data["did_alloted"] = didAlloted;
    data["account_name"] = accountName;
    data["account_password"] = accountPassword;
    data["email"] = email;
    data["mobile"] = mobile;
    data["company_name"] = companyName;
    data["show_no_id"] = showNoId;
    data["pulse"] = pulse;
    data["credits"] = credits;
    data["balance"] = balance;
    data["channels"] = channels;
    data["start_date"] = startDate;
    data["create_date"] = createDate;
    data["expiry_date"] = expiryDate;
    data["last_modify_date"] = lastModifyDate;
    data["current_status"] = currentStatus;
    data["start_time"] = startTime;
    data["end_time"] = endTime;
    data["created_by"] = createdBy;
    data["last_modify_by"] = lastModifyBy;
    data["source_file_path"] = sourceFilePath;
    data["services"] = services;
    data["TimeCondition"] = TimeCondition;
    data["kyc_status"] = kycStatus;
    data["CallTrunkType"] = CallTrunkType;
    data["CallTrunkName"] = CallTrunkName;
    data["TollFreeNo"] = TollFreeNo;
    data["login_time"] = loginTime;
    data["AccessKey"] = AccessKey;
    data["uuid"] = uuid;
    data["user_limit"] = userLimit;
    data["is_loggedin"] = isLoggedin;
    data["package_id"] = packageId;
    return data;
  }

}

class AgentModel {

  List<AgentModelData> data;

  AgentModel({
    this.data,
  });
  AgentModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<AgentModelData>();
      json['data'].forEach((v) {
        data.add(new AgentModelData.fromJson(v));
      });
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
