class ManagerLeadModelData {

  int id;
  String uniqueid;
  int accountid;
  int DID;
  String channel;
  int serviceid;
  int servicevalue;
  int CallerType;
  int AgentID;
  String AgentName;
  String AgentNumber;
  String CallerNumber;
  String CallerName;
  String CIRCLE;
  String OPERATOR;
  String HangupCause;
  int GroupID;
  String GroupName;
  int CallTypeID;
  String CallType;
  String CallStartTime;
  String CallAnswerTime;
  String CallEndTime;
  int CallDuration;
  int CallTalkTime;
  String AgentCallStartTime;
  String AgentCallAnswerTime;
  String AgentCallEndTime;
  int AgentTalkTime;
  String CallStatus;
  String AgentStatus;
  String CustomerStatus;
  String CallDisposition;
  String DisconnectedBy;
  String LastDestination;
  String RecPath;
  int CdrStatus;
  String feedback;
  String note;
  String scheduledate;
  int autoNumid;

  ManagerLeadModelData({
    this.id,
    this.uniqueid,
    this.accountid,
    this.DID,
    this.channel,
    this.serviceid,
    this.servicevalue,
    this.CallerType,
    this.AgentID,
    this.AgentName,
    this.AgentNumber,
    this.CallerNumber,
    this.CallerName,
    this.CIRCLE,
    this.OPERATOR,
    this.HangupCause,
    this.GroupID,
    this.GroupName,
    this.CallTypeID,
    this.CallType,
    this.CallStartTime,
    this.CallAnswerTime,
    this.CallEndTime,
    this.CallDuration,
    this.CallTalkTime,
    this.AgentCallStartTime,
    this.AgentCallAnswerTime,
    this.AgentCallEndTime,
    this.AgentTalkTime,
    this.CallStatus,
    this.AgentStatus,
    this.CustomerStatus,
    this.CallDisposition,
    this.DisconnectedBy,
    this.LastDestination,
    this.RecPath,
    this.CdrStatus,
    this.feedback,
    this.note,
    this.scheduledate,
    this.autoNumid,
  });
  ManagerLeadModelData.fromJson(Map<String, dynamic> json) {
    id = json["id"]?.toInt();
    uniqueid = json["uniqueid"]?.toString();
    accountid = json["accountid"]?.toInt();
    DID = json["DID"]?.toInt();
    channel = json["channel"]?.toString();
    serviceid = json["serviceid"]?.toInt();
    servicevalue = json["servicevalue"]?.toInt();
    CallerType = json["CallerType"]?.toInt();
    AgentID = json["AgentID"]?.toInt();
    AgentName = json["AgentName"]?.toString();
    AgentNumber = json["AgentNumber"]?.toString();
    CallerNumber = json["CallerNumber"]?.toString();
    CallerName = json["CallerName"]?.toString();
    CIRCLE = json["CIRCLE"]?.toString();
    OPERATOR = json["OPERATOR"]?.toString();
    HangupCause = json["HangupCause"]?.toString();
    GroupID = json["GroupID"]?.toInt();
    GroupName = json["GroupName"]?.toString();
    CallTypeID = json["CallTypeID"]?.toInt();
    CallType = json["CallType"]?.toString();
    CallStartTime = json["CallStartTime"]?.toString();
    CallAnswerTime = json["CallAnswerTime"]?.toString();
    CallEndTime = json["CallEndTime"]?.toString();
    CallDuration = json["CallDuration"]?.toInt();
    CallTalkTime = json["CallTalkTime"]?.toInt();
    AgentCallStartTime = json["AgentCallStartTime"]?.toString();
    AgentCallAnswerTime = json["AgentCallAnswerTime"]?.toString();
    AgentCallEndTime = json["AgentCallEndTime"]?.toString();
    AgentTalkTime = json["AgentTalkTime"]?.toInt();
    CallStatus = json["CallStatus"]?.toString();
    AgentStatus = json["AgentStatus"]?.toString();
    CustomerStatus = json["CustomerStatus"]?.toString();
    CallDisposition = json["CallDisposition"]?.toString();
    DisconnectedBy = json["DisconnectedBy"]?.toString();
    LastDestination = json["LastDestination"]?.toString();
    RecPath = json["RecPath"]?.toString();
    CdrStatus = json["CdrStatus"]?.toInt();
    feedback = json["feedback"]?.toString();
    note = json["note"]?.toString();
    scheduledate = json["scheduledate"]?.toString();
    autoNumid = json["auto_numid"]?.toInt();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["id"] = id;
    data["uniqueid"] = uniqueid;
    data["accountid"] = accountid;
    data["DID"] = DID;
    data["channel"] = channel;
    data["serviceid"] = serviceid;
    data["servicevalue"] = servicevalue;
    data["CallerType"] = CallerType;
    data["AgentID"] = AgentID;
    data["AgentName"] = AgentName;
    data["AgentNumber"] = AgentNumber;
    data["CallerNumber"] = CallerNumber;
    data["CallerName"] = CallerName;
    data["CIRCLE"] = CIRCLE;
    data["OPERATOR"] = OPERATOR;
    data["HangupCause"] = HangupCause;
    data["GroupID"] = GroupID;
    data["GroupName"] = GroupName;
    data["CallTypeID"] = CallTypeID;
    data["CallType"] = CallType;
    data["CallStartTime"] = CallStartTime;
    data["CallAnswerTime"] = CallAnswerTime;
    data["CallEndTime"] = CallEndTime;
    data["CallDuration"] = CallDuration;
    data["CallTalkTime"] = CallTalkTime;
    data["AgentCallStartTime"] = AgentCallStartTime;
    data["AgentCallAnswerTime"] = AgentCallAnswerTime;
    data["AgentCallEndTime"] = AgentCallEndTime;
    data["AgentTalkTime"] = AgentTalkTime;
    data["CallStatus"] = CallStatus;
    data["AgentStatus"] = AgentStatus;
    data["CustomerStatus"] = CustomerStatus;
    data["CallDisposition"] = CallDisposition;
    data["DisconnectedBy"] = DisconnectedBy;
    data["LastDestination"] = LastDestination;
    data["RecPath"] = RecPath;
    data["CdrStatus"] = CdrStatus;
    data["feedback"] = feedback;
    data["note"] = note;
    data["scheduledate"] = scheduledate;
    data["auto_numid"] = autoNumid;
    return data;
  }
}

class ManagerLeadModel {

  int total;
  List<ManagerLeadModelData> data;

  ManagerLeadModel({
    this.total,
    this.data,
  });
  ManagerLeadModel.fromJson(Map<String, dynamic> json) {
    total = json["total"]?.toInt();
    if (json['data'] != null) {
      data = new List<ManagerLeadModelData>();
      json['data'].forEach((v) {
        data.add(new ManagerLeadModelData.fromJson(v));
      });
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["total"] = total;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
