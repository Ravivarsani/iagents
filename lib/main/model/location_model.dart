class Location_model {
  String msg;

  Location_model({this.msg});

  Location_model.fromJson(Map<String, dynamic> json) {
    msg = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.msg;
    return data;
  }
}