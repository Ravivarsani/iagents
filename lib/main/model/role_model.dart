
class RoleModelData {
  int accountType;

  RoleModelData({
    this.accountType,
  });

  RoleModelData.fromJson(Map<String, dynamic> json) {
    accountType = json["account_type"];
  }

}

class RoleModel {
  List<RoleModelData> data;

  RoleModel({
    this.data,
  });

  RoleModel.fromJson(Map<String, dynamic> json) {
    if (json["data"] != null) {
      var v = json["data"];
      var arr0 = List<RoleModelData>();
      v.forEach((v) {
        arr0.add(RoleModelData.fromJson(v));
      });
      data = arr0;
    }
  }

  List<RoleModelData> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    if (data != null) {
      List<dynamic> dynamicList = data['data'];
      List<RoleModelData> role = new List<RoleModelData>();

      dynamicList.forEach((f) {
        RoleModelData modelData = RoleModelData.fromJson(f);
        role.add(modelData);
      });

      return role;
    }
  }
}
