class ActivityModelData {
  int id;
  int uid;
  String date;
  String action;
  String ip;

  ActivityModelData({
    this.id,
    this.uid,
    this.date,
    this.action,
    this.ip,
  });

  ActivityModelData.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    uid = json["uid"];
    date = json["date"];
    action = json["action"];
    ip = json["ip"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['uid'] = this.uid;
    data['date'] = this.date;
    data['action'] = this.action;
    data['ip'] = this.ip;
    return data;
  }
}

class ActivityModel {
  List<ActivityModelData> data;

  ActivityModel({
    this.data,
  });

  ActivityModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<ActivityModelData>();
      json['data'].forEach((v) {
        data.add(new ActivityModelData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
