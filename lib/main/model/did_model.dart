class DidModelData {
  String didNumber;
  int id;
  String vmn;
  String did;
  int Type;
  int Action;
  int assign;
  int active;
  String assginDate;
  String unassignDate;
  int accountId;
  String Description;

  DidModelData({
    this.didNumber,
    this.id,
    this.vmn,
    this.did,
    this.Type,
    this.Action,
    this.assign,
    this.active,
    this.assginDate,
    this.unassignDate,
    this.accountId,
    this.Description,
  });

  DidModelData.fromJson(Map<String, dynamic> json) {
    didNumber = json["did_number"]?.toString();
    id = json["id"]?.toInt();
    vmn = json["vmn"]?.toString();
    did = json["did"]?.toString();
    Type = json["Type"]?.toInt();
    Action = json["Action"]?.toInt();
    assign = json["assign"]?.toInt();
    active = json["active"]?.toInt();
    assginDate = json["assginDate"]?.toString();
    unassignDate = json["unassignDate"]?.toString();
    accountId = json["account_id"]?.toInt();
    Description = json["Description"]?.toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["did_number"] = didNumber;
    data["id"] = id;
    data["vmn"] = vmn;
    data["did"] = did;
    data["Type"] = Type;
    data["Action"] = Action;
    data["assign"] = assign;
    data["active"] = active;
    data["assginDate"] = assginDate;
    data["unassignDate"] = unassignDate;
    data["account_id"] = accountId;
    data["Description"] = Description;
    return data;
  }
}

class DidModel {
  List<DidModelData> data;

  DidModel({
    this.data,
  });

  DidModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<DidModelData>();
      json['data'].forEach((v) {
        data.add(new DidModelData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
