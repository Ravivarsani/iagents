
class TotalDataModelData {
  int total;

  TotalDataModelData({
    this.total,
  });

  TotalDataModelData.fromJson(Map<String, dynamic> json) {
    total = json['total'];
  }

}

class TotalModel {
  List<TotalDataModelData> data;

  TotalModel({
    this.data,
  });

  TotalModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      var v = json['data'];
      var arr0 = List<TotalDataModelData>();
      v.forEach((v) {
        arr0.add(TotalDataModelData.fromJson(v));
      });
      data = arr0;
    }
  }

  List<TotalDataModelData> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    if (data != null) {
      List<dynamic> dynamicList = data['data'];
      List<TotalDataModelData> role = new List<TotalDataModelData>();

      dynamicList.forEach((f) {
        TotalDataModelData modelData = TotalDataModelData.fromJson(f);
        role.add(modelData);
      });

      return role;
    }
  }
}
