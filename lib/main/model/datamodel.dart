class Data_model {
  String msg;

  Data_model({this.msg});

  Data_model.fromJson(Map<String, dynamic> json) {
    msg = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.msg;
    return data;
  }
}
