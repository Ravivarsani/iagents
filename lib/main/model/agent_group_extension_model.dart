class AgentTransferModelData {
  String groupname;
  String transferext;

  AgentTransferModelData({
    this.groupname,
    this.transferext,
  });

  AgentTransferModelData.fromJson(Map<String, dynamic> json) {
    groupname = json["groupname"]?.toString();
    transferext = json["transferext"]?.toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["groupname"] = groupname;
    data["transferext"] = transferext;
    return data;
  }
}

class AgentTransferModel {
  List<AgentTransferModelData> data;

  AgentTransferModel({
    this.data,
  });

  AgentTransferModel.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<AgentTransferModelData>();
      json['data'].forEach((v) {
        data.add(new AgentTransferModelData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
