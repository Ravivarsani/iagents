import 'package:TeleVoice/src/sip_ua_helper.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/dialpad.dart';
import 'package:TeleVoice/main/ui/call_logs/call_logs.dart';
import 'package:TeleVoice/main/ui/contacts/contacts.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:logger/logger.dart';
import 'package:ringtone/ringtone.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:TeleVoice/main/model/call_log_model.dart';
import 'package:TeleVoice/main/utils/constant.dart';
import 'package:http/http.dart' as http;

import 'model/call_log_model.dart';

class PhoneDialerWidget extends StatefulWidget {
  final SIPUAHelper _helper;

  PhoneDialerWidget(this._helper, {Key key}) : super(key: key);

  @override
  _MyPhoneDialerWidget createState() => _MyPhoneDialerWidget();
}

class _MyPhoneDialerWidget extends State<PhoneDialerWidget>
    implements SipUaHelperListener {
  SIPUAHelper get helper => widget._helper;
  int _currentIndex = 0;

  List<Widget> _children;

  bool isCall = false;

  SharedPreferences _preferences;
  int userID;
  String accessToken;
  Future<List<CallLogModelData>> _futureCurrentCall;
  Timer timer;
  bool isInternetConnection = true;

  @override
  void initState() {
    // TODO: implement initState
    _loadSettings();
    super.initState();
  }

  @override
  deactivate() {
    super.deactivate();
    helper.removeSipUaHelperListener(this);
  }

  void _loadSettings() async {
    helper.addSipUaHelperListener(this);
    _preferences = await SharedPreferences.getInstance();
    _children = [DialPadWidget(), ContactsPage(helper), CallLogs(helper)];
    this.setState(() {
      userID = _preferences.getInt('user_id');
      accessToken = _preferences.getString('access_token');
    });

    timer = Timer.periodic(Duration(seconds: 5), (_) {
      loadDetails();
    });
  }

  loadDetails() async {
    _futureCurrentCall = GetCurrentCallData(userID, accessToken);
    _futureCurrentCall.then((value) async {
      setState(() {
        if (value.length > 0) {
          isCall = true;
        } else {
          isCall = false;
        }
      });

      return value;
    });
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    if (isInternetConnection != null && !isInternetConnection) {
      return new Scaffold(
          body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(
                  Icons.signal_wifi_off,
                  color: Color.fromRGBO(49, 87, 110, 1.0),
                  size: 65.0,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                new Text('Oops!',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.title),
                Padding(
                  padding: EdgeInsets.only(top: 16.0),
                ),
                new Text('It seems like you are not connected to network.',
                    softWrap: true,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.body1),
                Padding(
                  padding: EdgeInsets.only(top: 27.0),
                ),
                new MaterialButton(
                  color: Colors.blue[700],
                  textColor: Colors.white,
                  child: new Text('RETRY'),
                  onPressed: () {
                    if (this.mounted) {
                      setState(() {
                        _internetConnection();
                      });
                    }
                  },
                )
              ],
            ),
          ),
        ],
      ));
    } else {
      return Scaffold(
        body: _children[_currentIndex], // new
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped, // new
          currentIndex: _currentIndex, // n
          items: [
            BottomNavigationBarItem(
              icon: new Icon(Icons.dialpad),
              title: new Text('Dailer'),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.contacts),
              title: new Text('Contacts'),
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.history),
              title: new Text('History'),
            ),
          ],
        ),
        floatingActionButton: isCall
            ? new FloatingActionButton.extended(
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, "/currentcall", (Route<dynamic> route) => true);
                },
                icon: Icon(Icons.call, color: Colors.green),
                label: Text("Current Call"),
              )
            : Container(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      );
    }
  }

  _internetConnection() {
    Constant.isInternetAvailable().then((value) {
      print("=====>> internet connection ==>> $value");
      if (value) {
        isInternetConnection = true;
        if (this.mounted) {
          setState(() {});
        }
      } else {
        isInternetConnection = false;
        if (this.mounted) {
          setState(() {});
        }
      }
    });
  }

  Future<List<CallLogModelData>> GetCurrentCallData(
      int id, String token) async {
    final http.Response response = await http.post(
      Constant.GET_AGENT_CURRENT_CALL,
      headers: <String, String>{
        HttpHeaders.authorizationHeader: "Bearer " + token,
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {'id': id.toString()},
    );

    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      return CallLogModel.fromJson(json).data;
    } else {
      throw Exception('=====>>> Failed to fetch data');
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    timer.cancel();
    super.dispose();
  }

  @override
  void callStateChanged(Call call, CallState state) {
    // TODO: implement callStateChanged
    if (state.state == CallStateEnum.CALL_INITIATION) {
      if (call.direction == 'INCOMING') {
        Ringtone.play();
      }

      Navigator.pushNamed(context, '/callscreen', arguments: call);
    }
  }

  @override
  void onNewMessage(SIPMessageRequest msg) {
    // TODO: implement onNewMessage
  }

  @override
  void registrationStateChanged(RegistrationState state) {
    // TODO: implement registrationStateChanged
  }

  @override
  void transportStateChanged(TransportState state) {
    // TODO: implement transportStateChanged
  }
}
