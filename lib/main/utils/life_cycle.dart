import 'package:flutter/cupertino.dart';

abstract class HandleLifeCycle {
  void onResume();
  void onPause();
}

class LifecycleEventHandler extends WidgetsBindingObserver {
  HandleLifeCycle listener;

  LifecycleEventHandler(this.listener);

  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        print("===========>>> inactive");
        break;
      case AppLifecycleState.paused:
        print("===========>>> paused");
        listener.onPause();
        break;
      case AppLifecycleState.resumed:
        print("===========>>> resumed");
        listener.onResume();
        break;
      case AppLifecycleState.detached:
        // TODO: Handle this case.
        break;
    }
  }
}