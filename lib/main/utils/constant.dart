import 'dart:io';

class Constant {
  static final API_URL = 'https://app.teleforce.in/api/';

  static final SIGNIN = 'signin';

  static final GET_ROLE = SIGNIN + '/getrole';

  static final POST_UUID = SIGNIN + '/updateuuid';

  static final POST_LOGOUT = API_URL + SIGNIN + '/logout';

  static final GET_AGENT_CALL_LOG = API_URL + 'call/getAgentCallLogData';

  static final GET_AGENT_CURRENT_CALL = API_URL + 'call/getagentcurrentcall1';

  static final GET_AGENT_FEEDBACK = API_URL + 'call/getAgentCallFeedback';

  static final POST_AGENT_OUTGOING_CALL = API_URL + 'call/agentoutgoingcall';

  static final POST_FEEDBACK = API_URL + 'call/updateAgentCallFeedback';

  static final RECORDING_PATH = API_URL + 'call/audio/';

  static final ACTIVITY_LOG = API_URL + 'default/getlog/';

  static final ACTIVITY_LOGOUT = API_URL + 'default/getlogout/';

  static final GET_AGENT_LIST = API_URL + 'default/getagents/';

  static final GET_DID = API_URL + 'default/getdidrouting/';

  static final GET_DID_DETAIL = API_URL + 'default/getDIDdetail/';

  static final UPDATE_DID = API_URL +
      'default/updateDIDconfig/'; // action: 102   did_id: 129    type: 9

  static final GET_IVR_LIST = API_URL + 'default/getivrlist/';

  static final GET_AGENT_GROUP = API_URL + 'default/getAgentGroupwithPackage/';

  static final GET_HUNT_AGENT_GROUP = API_URL + 'default/gethuntgroup/';

  static final DELETE_AGENT = API_URL + 'default/deleteagent/';

  static final DELETE_HUNT_GROUP = API_URL + 'default/deleteHuntgroup/';

  static final GET_LIVE_CALL = API_URL + 'call/searchLiveCall';

  static final UPDATE_CALL_LOG = API_URL + 'call/updateCallogData';

  static final POST_LIVE_CALL_MONITOR = API_URL + 'call/callmonitor';

  static final GET_TOTAL_LIVE_CALL = API_URL + 'call/totalLiveCall';

  static final GET_TOTAL_IVR_CALL = API_URL + 'call/totalIVRCall';

  static final GET_TOTAL_DID_NUMBER_CALL = API_URL + 'call/totalDidNumbers';

  static final GET_TOTAL_MISSED_CALL = API_URL + 'call/totalMissCall';

  static final GET_TOTAL_TODAY_CALL = API_URL + 'call/totalTodayCall';

  static final GET_TOTAL_TODAY_INCOMING_CALL =
      API_URL + 'call/totalTodayIncomingCall';

  static final GET_TOTAL_TODAY_OUTGOING_CALL =
      API_URL + 'call/totalTodayOutgoingCall';

  static final GET_TOTAL_TODAY_MISSED_CALL =
      API_URL + 'call/totalTodayMissedCall';

  static final GET_MANAGER_CALL_LOGS = API_URL + 'call/getcalllog/';

  static final GET_MANAGER_SEARCH_CALL_LOGS = API_URL + 'call/searchCallLog';

  static final GET_MANAGER_LEAD = API_URL + 'call/getmanagerlead';

  static final GET_AGENT_ASSIGN_LEAD = API_URL + 'call/getagentassignleaddata';

  static final GET_AGENT_LEAD_CALL = API_URL + 'call/agentleadcall';

  static final GET_CONFERENCE_CALL = API_URL + 'call/getConferenceCall/';

  static final GET_SOUND_LIST = API_URL + 'setting/getsoundlist/';

  static final START_AGENT_BREAK_TIME =
      API_URL + 'setting/startAgentBreakTime/';

  static final STOP_AGENT_BREAK_TIME = API_URL + 'setting/stopAgentBreakTime';

  static final GET_AGENT_BREAK_TIME_STATUS =
      API_URL + 'setting/getAgentStartBreakTime/';

  static final POST_AGENT_LIVE_LOCATION =
      API_URL + 'location/saveAgentLocation';

  static final CHECK_PHONE_NUMBER = API_URL + 'contacts/checkCallerNumber';

  static final GET_CATEGORY_LIST = API_URL + 'contacts/getCategorylist/';

  static final CREATE_CONTACT = API_URL + 'contacts/createcontact';

  static final GET_AGNET_DETAIL = API_URL + 'default/getagentdetail/';

  static Future<bool> isInternetAvailable() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      print("onError Socket Exception ====>> ");
      return false;
    }
  }
}

class RegularExpressionsPatterns {
  static const String EMAIL_VALIDATION =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

  static const String PHONE_VALIDATION = r'^[0-9]+$';

  static const String LATITUDE_PATTERN =
      r'^(\+|-)?((\d((\.)|\.\d{1,10})?)|(0*?[0-8]\d((\.)|\.\d{1,10})?)|(0*?90((\.)|\.0{1,10})?))$';

  static const String LONGITUDE_PATTERN =
      r'^(\+|-)?((\d((\.)|\.\d{1,10})?)|(0*?\d\d((\.)|\.\d{1,10})?)|(0*?1[0-7]\d((\.)|\.\d{1,10})?)|(0*?180((\.)|\.0{1,10})?))$';
}
