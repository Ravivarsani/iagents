import 'dart:io';
import 'package:adhara_socket_io/adhara_socket_io.dart';

class SocketUtils {
  //
  static String _connectUrl = 'https://auth.teleforce.in/';

  // Events
  static const String ON_AGENT_CALL_RECEIVED = 'agentcallanswer';

  String _fromUser;

  SocketIO _socket;
  SocketIOManager _manager;

  initSocket(String fromUser) async {
    print('=================>> user: ${fromUser}');
    this._fromUser = fromUser;
    await _init();
  }

  _init() async {
    _manager = SocketIOManager();
    _socket = await _manager.createInstance(_socketOptions());
  }

  _socketOptions() {
    final Map<String, String> userMap = {
      'token': _fromUser,
    };
    return SocketOptions(
      _connectUrl,
      enableLogging: true,
      transports: [Transports.WEB_SOCKET],
      query: userMap,
    );
  }

  connectToSocket() {
    if (null == _socket) {
      print("====================>>> Socket is Null");
      return;
    }
    print("======================>>> Connecting to socket...");
    _socket.connect();
  }

  setConnectListener(Function onConnect) {
    _socket.onConnect((data) {
      onConnect(data);
    });
  }

  setOnConnectionErrorListener(Function onConnectError) {
    _socket.onConnectError((data) {
      onConnectError(data);
    });
  }

  setOnConnectionErrorTimeOutListener(Function onConnectTimeout) {
    _socket.onConnectTimeout((data) {
      onConnectTimeout(data);
    });
  }

  setOnErrorListener(Function onError) {
    _socket.onError((error) {
      onError(error);
    });
  }

  setOnDisconnectListener(Function onDisconnect) {
    _socket.onDisconnect((data) {
      print("onDisconnect $data");
      onDisconnect(data);
    });
  }

  setOnAgentCallReceivedListener(Function onDataReceived) {
    _socket.on(ON_AGENT_CALL_RECEIVED, (data) {
      print("======================>>> Received $data");
      onDataReceived(data);
    });
  }

  closeConnection() {
    if (null != _socket) {
      print("Close Connection");
      _manager.clearInstance(_socket);
    }
  }
}
