import 'package:flutter/material.dart';
import 'package:TeleVoice/main/utils/SizeConfig.dart';
import 'package:TeleVoice/main/widgets/action_button.dart';

class ResponsiveApp extends StatefulWidget {
  @override
  _LoginExampleState createState() => _LoginExampleState();
}

class _LoginExampleState extends State<ResponsiveApp> {
  MediaQueryData queryData;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    queryData = MediaQuery.of(context);
    print('===============>>> ' + queryData.size.width.toString());
    print('===============>>> ' + queryData.size.height.toString());
    return Material(
      child: SafeArea(
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 6.0),
              child: Align(
                alignment: Alignment.topCenter,
                child: Text(
                  'Calling...',
                  style: new TextStyle(
                      fontSize: 20.0, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Align(
              alignment: Alignment(0.0, -0.72),
              child: Text(
                '8685785858',
                style: new TextStyle(fontSize: 22.0, color: Colors.black),
              ),
            ),
            Align(
              alignment: Alignment(0.0, -0.6),
              child: Text(
                '8758395325',
                style: new TextStyle(fontSize: 18.0, color: Colors.black54),
              ),
            ),
            Align(
              alignment: Alignment(0.0, -0.5),
              child: Text('uyfyfyfuy',
                  style: TextStyle(fontSize: 14, color: Colors.black54)),
            ),
            Align(
              alignment: Alignment.center,
              child: Image.asset(
                "images/calling1.gif",
                height: 220.0,
                width: 240.0,
              ),
            ),
            Align(
              alignment: Alignment(0.0, 0.75),
              child: ActionButton(
                title: "transfer",
                icon: Icons.phone_forwarded,
                onPressed: () async {},
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: RaisedButton(
                child: Text(
                  "Give Feedback",
                  style: TextStyle(fontSize: 18),
                ),
                onPressed: () => null,
                color: Color.fromRGBO(49, 87, 110, 1.0),
                textColor: Colors.white,
                splashColor: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
