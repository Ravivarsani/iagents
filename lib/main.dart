import 'dart:collection';

import 'package:TeleVoice/main/sip_callscreen.dart';
import 'package:TeleVoice/main/sip_dialpad.dart';
import 'package:TeleVoice/main/support_client.dart';
import 'package:TeleVoice/main/ui/create_contact.dart';
import 'package:TeleVoice/src/sip_ua_helper.dart';
import 'package:flutter/material.dart';
import 'package:TeleVoice/main/home.dart';
import 'package:TeleVoice/main/logintime_state.dart';
import 'package:TeleVoice/main/ongoing_call.dart';
import 'package:TeleVoice/main/phone_dialer.dart';
import 'package:TeleVoice/main/ui/call_logs/call_logs.dart';
import 'package:TeleVoice/main/ui/dashboard/dashboard.dart';
import 'package:TeleVoice/main/ui/manager/manager_call_logs.dart';
import 'package:TeleVoice/main/ui/manager/manager_did.dart';
import 'package:TeleVoice/main/user_verification.dart';
import 'package:splashscreen/splashscreen.dart';
import 'main/responsive_ui.dart';
import 'main/services/service_locator.dart';
import 'main/ui/assigned_lead.dart';
import 'main/ui/contacts/contacts.dart';
import 'main/ui/manager/leads/manager_leads.dart';
import 'main/ui/manager/manager_agents_groupt.dart';
import 'main/ui/manager/manager_agents_list.dart';
import 'main/ui/manager/manager_live_call.dart';
import 'package:TeleVoice/main/about.dart';
import 'package:TeleVoice/main/login.dart';

void main() {
  setupLocator();
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 5,
        navigateAfterSeconds: new AfterSplash(),
        image: Image.asset('images/splash.png'),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        loadingText: Text(
          'From Garuda Advertising Pvt. Ltd.',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
        ),
        onClick: () => print("Teleforce"),
        loaderColor: Color.fromRGBO(49, 87, 110, 1.0));
  }
}

typedef PageContentBuilder = Widget Function(
    [SIPUAHelper helper, Object arguments]);

class AfterSplash extends StatelessWidget {
  final SIPUAHelper _helper = SIPUAHelper();

  Map<String, PageContentBuilder> routes = {
    '/login': ([SIPUAHelper helper, Object arguments]) => MyLogin(helper),
    '/sipdailer': ([SIPUAHelper helper, Object arguments]) =>
        SipDialPadWidget(helper),
    '/callscreen': ([SIPUAHelper helper, Object arguments]) =>
        SipCallScreenWidget(helper, arguments as Call),
    '/about': ([SIPUAHelper helper, Object arguments]) => AboutWidget(),
    '/home': ([SIPUAHelper helper, Object arguments]) => Home(helper),
    '/support': ([SIPUAHelper helper, Object arguments]) => SupportClientPage(),
    '/loginstate': ([SIPUAHelper helper, Object arguments]) =>
        LoginStateWidget(),
    '/about': ([SIPUAHelper helper, Object arguments]) => AboutWidget(),
    '/dashboard': ([SIPUAHelper helper, Object arguments]) => Dashboard(),
    '/calllogs': ([SIPUAHelper helper, Object arguments]) => CallLogs(helper),
    '/contacts': ([SIPUAHelper helper, Object arguments]) =>
        ContactsPage(helper),
    '/currentcall': ([SIPUAHelper helper, Object arguments]) =>
        OngoingCallWidget(),
    '/phonedialer': ([SIPUAHelper helper, Object arguments]) =>
        PhoneDialerWidget(helper),
    '/livecall': ([SIPUAHelper helper, Object arguments]) => LiveCall(),
    '/managercalllogs': ([SIPUAHelper helper, Object arguments]) =>
        ManagerCallLogs(),
    '/assignedlead': ([SIPUAHelper helper, Object arguments]) =>
        AssignedLead(helper),
    '/managerdid': ([SIPUAHelper helper, Object arguments]) => ManagerDid(),
    '/managerleads': ([SIPUAHelper helper, Object arguments]) => ManagerLeads(),
    '/manageragentslist': ([SIPUAHelper helper, Object arguments]) =>
        ManagerAgentListPage(),
    '/manageragentsgroup': ([SIPUAHelper helper, Object arguments]) =>
        ManagerAgentGroupPage(),
    '/responsiveui': ([SIPUAHelper helper, Object arguments]) =>
        ResponsiveApp(),
    '/verification': ([SIPUAHelper helper, Object arguments]) =>
        UserVerification(),
  };

  Route<dynamic> _onGenerateRoute(RouteSettings settings) {
    final String name = settings.name;
    final PageContentBuilder pageContentBuilder = routes[name];
    if (pageContentBuilder != null) {
      if (settings.arguments != null) {
        final Route route = MaterialPageRoute<Widget>(
            builder: (context) =>
                pageContentBuilder(_helper, settings.arguments));
        return route;
      } else {
        final Route route = MaterialPageRoute<Widget>(
            builder: (context) => pageContentBuilder(_helper));
        return route;
      }
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Tele Dialer',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Color.fromRGBO(49, 87, 110, 1.0),
        accentColor: Colors.blue[700],
        textSelectionHandleColor: Colors.blue[600],
        fontFamily: 'SourceSansPro-Bold',
      ),
      initialRoute: '/login',
      onGenerateRoute: _onGenerateRoute,
      debugShowCheckedModeBanner: false,
    );
  }
}
