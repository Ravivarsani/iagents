package com.example.flutter_iagents.network;

/** Created by jayesh on 8/8/2018. */
public interface NetworkListener {

    void onError(String APIKey, Exception e);

    void onResponse(String APIKey, String response, String tag);
}
