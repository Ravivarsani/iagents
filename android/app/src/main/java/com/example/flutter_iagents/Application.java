package com.example.flutter_iagents;

import android.util.Log;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import io.flutter.app.FlutterApplication;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService;
import okhttp3.OkHttpClient;

public class Application extends FlutterApplication implements PluginRegistrantCallback {

    public static OkHttpClient httpClient;

    @Override
    public void onCreate() {
        super.onCreate();
        FlutterFirebaseMessagingService.setPluginRegistrant(this);
        initializeOkHttpClient();
    }

    @Override
    public void registerWith(PluginRegistry registry) {
        FirebaseCloudMessagingPluginRegistrant.registerWith(registry);
    }

    private void initializeOkHttpClient() {
        try {
            httpClient = getUserOkHttp();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private OkHttpClient getUserOkHttp() {
        try {
            // Create a trust manager that dose not validate chains
            final TrustManager[] trustManagers =
                    new TrustManager[]{
                            new X509TrustManager() {
                                @Override
                                public void checkServerTrusted(X509Certificate[] chain, String authType)
                                        throws CertificateException {
                                }

                                @Override
                                public void checkClientTrusted(X509Certificate[] chain, String authType)
                                        throws CertificateException {
                                }

                                @Override
                                public X509Certificate[] getAcceptedIssuers() {
                                    return new X509Certificate[]{};
                                }
                            }
                    };
            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustManagers, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustManagers[0]);
            builder.hostnameVerifier(
                    new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    });

            OkHttpClient client =
                    builder.connectTimeout(60, TimeUnit.SECONDS)
                            .writeTimeout(60, TimeUnit.SECONDS)
                            .readTimeout(60, TimeUnit.SECONDS)
                            .retryOnConnectionFailure(true)
                            .build();

            return client;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}