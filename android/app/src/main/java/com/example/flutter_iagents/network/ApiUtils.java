package com.example.flutter_iagents.network;

import okhttp3.Request;
import okhttp3.RequestBody;

public class ApiUtils {
    public static final String domain = "https://app.teleforce.in/api/tfapi/";

    public static final String CALL_VERIFY = domain + "verifymobile";
    public static final String CHECK_VERIFY = domain + "checkverifymobile";

    public static String POST_CALL_VERIFY() {
        return CALL_VERIFY;
    }

    public static String POST_CHECK_VERIFY() {
        return CHECK_VERIFY;
    }

    public static Request getRequest(String url, RequestBody reqBody) {
        Request requestBody =
                new Request.Builder()
                        .url(url)
                        .post(reqBody)
                        .header("Content-type", "application/x-www-form-urlencoded")
                        .build();
        return requestBody;
    }

    public static Request getRequestWithHeaderRequestBody(String requestUrl, RequestBody reqBody) {
        Request requestBody =
                new Request.Builder()
                        .url(requestUrl)
                        .post(reqBody)
                        .header("Content-type", "application/x-www-form-urlencoded")
                        .build();
        return requestBody;
    }

    public static final class ApiTag {
        public static final String CallTag = "calltag";
        public static final String VerifyTag = "verifytag";
    }
}
