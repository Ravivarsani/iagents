package com.example.flutter_iagents;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;

import com.example.flutter_iagents.network.ApiUtils;
import com.example.flutter_iagents.network.NetworkCall;
import com.example.flutter_iagents.network.NetworkListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.flutter.Log;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import okhttp3.FormBody;

public class MainActivity extends FlutterActivity implements NetworkListener {

    private static final String CHANNEL = "teleforce.flutter.dev/incoming_phone_number";
    String mNumber, mUserID;
    String resultS = "We could not got call to verify.";
    NetworkCall networkCall;
    CallReceiver mCallReceiver;
    MethodChannel.Result mResultMethod = null;

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String incoming_number = intent.getStringExtra("incoming_number");

                makeCallVerifyAPI(mNumber, incoming_number);
            }

        }
    };

    public static boolean isJSONValid(String jsonString) {
        try {
            new JSONObject(jsonString);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(jsonString);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        networkCall = new NetworkCall(this);
        mCallReceiver = new CallReceiver();
        registerReceiver(broadcastReceiver, new IntentFilter("incoming_number"));
    }

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);

        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL).setMethodCallHandler((call, result) -> {
            mResultMethod = result;
            if (call.method.equals("getIncomingNumber")) {
                mNumber = call.argument("number");
                mUserID = call.argument("id");
                makeCallAPI(mNumber, mUserID);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        result.success(resultS);
                    }
                }, 7000);
            } else {
                result.notImplemented();
            }
        });

    }

    private void makeCallAPI(String mobile, String mUserID) {

        FormBody body = new FormBody.Builder().add("mobile", mobile).add("user_id", mUserID).build();

        networkCall.makeAPICall(ApiUtils.POST_CALL_VERIFY(), body, this, ApiUtils.ApiTag.CallTag);
    }

    private void makeCallVerifyAPI(String mobile, String incomingnumber) {

        unregisterReceiver(broadcastReceiver);

        FormBody body = new FormBody.Builder().add("mobile", mobile).add("did", incomingnumber).add("user_id", mUserID).build();

        networkCall.makeAPICall(ApiUtils.POST_CHECK_VERIFY(), body, this, ApiUtils.ApiTag.VerifyTag);
    }

    @Override
    public void onError(String APIKey, Exception e) {

    }

    @Override
    public void onResponse(String APIKey, String response, String tag) {
        try {
            if (tag == ApiUtils.ApiTag.CallTag) {
                if (response != null && isJSONValid(response)) {
                    JSONObject jsonObject = new JSONObject(response);
                    String msg = jsonObject.getString("msg");
                    if (msg.equals("Call originate succesfully.")) {

                        runOnUiThread(
                                () -> {

                                    new Handler().postDelayed(() -> {

                                        String s = "Verification Failed";

                                    }, 25000);

                                    IntentFilter filter = new IntentFilter();
                                    filter.addAction("android.intent.action.PHONE_STATE");
                                    registerReceiver(mCallReceiver, filter);
                                    //Toast.makeText(this, "Register receiver", Toast.LENGTH_SHORT).show();
                                });
                    } else {

                    }
                } else {

                }
            } else if (tag == ApiUtils.ApiTag.VerifyTag) {
                JSONObject jsonObject = new JSONObject(response);
                String msg = jsonObject.getString("msg");
                resultS = msg.equals("success") ? "Verification Successful" : "Verification Failed";
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mResultMethod != null) mResultMethod.success(resultS);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(mCallReceiver);
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

}

