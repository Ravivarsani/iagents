package com.example.flutter_iagents;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class CallReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle bundle = intent.getExtras();
        String phone_number = bundle.getString("incoming_number");

        if (phone_number == null || "".equals(phone_number)) {
            return;
        }
        context.sendBroadcast(new Intent("incoming_number").putExtra("incoming_number", phone_number));
    }
}