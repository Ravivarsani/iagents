package com.example.flutter_iagents.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.example.flutter_iagents.Application;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by admin on 10/8/2018.
 */
public class NetworkCall {

    public static final String TAG = "=======>>NetworkCall";

    Context activity;
    ProgressDialog pd;

    public NetworkCall(Context activity) {
        this.activity = activity;
    }

    public static ProgressDialog getProgressDialog(Context activity) {
        ProgressDialog pd = new ProgressDialog(activity);
        pd.setTitle("Hold on! We are calling you for auto verification");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        return pd;
    }

    public void makeAPICall(
            final String requestUrl,
            FormBody body,
            final NetworkListener listener,
            final String tag) {

//        showWaiting();
        Request request = ApiUtils.getRequest(requestUrl, body);

        Application.httpClient
                .newCall(request)
                .enqueue(
                        new Callback() {

                            @Override
                            public void onFailure(Call call, IOException e) {
                               // closeWaiting();
                                listener.onError(tag, e);
                            }

                            @Override
                            public void onResponse(Call call, Response response)
                                    throws IOException {
                                try {

                                    String recResponse = response.body().string();

                                    Log.i(TAG, "recResponse : " + recResponse);
                                   // closeWaiting();
                                    listener.onResponse(requestUrl, recResponse, tag);
                                } catch (Exception e) {
                                    Log.e(
                                            TAG,
                                            "Error in onResponse() method while from server : "
                                                    + tag);
                                    e.printStackTrace();
                                }
                            }
                        });
    }

    public void makeAPICallWithRequestBody(
            final String requestUrl,
            RequestBody body,
            final NetworkListener listener,
            final String tag) {
        showWaiting();
        Request request = ApiUtils.getRequestWithHeaderRequestBody(requestUrl, body);

        Application.httpClient
                .newCall(request)
                .enqueue(
                        new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                closeWaiting();
                                listener.onError(tag, e);
                            }

                            @Override
                            public void onResponse(Call call, Response response)
                                    throws IOException {
                                try {
                                    closeWaiting();

                                    String recResponse = response.body().string();
                                    Log.i(TAG, "recceAddResponse : " + recResponse);
                                    listener.onResponse(requestUrl, recResponse, tag);
                                } catch (Exception e) {
                                    Log.e(
                                            TAG,
                                            "Error in onResponse() method while from server : "
                                                    + tag);
                                    e.printStackTrace();
                                }
                            }
                        });
    }

    private void showWaiting() {
        closeWaiting();
        pd = getProgressDialog(activity);
        pd.show();
//        pd.setContentView(R.layout.dialog_progress);
    }

    private void closeWaiting() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
            pd.cancel();
        }
    }

}
